-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 09, 2021 at 11:27 PM
-- Server version: 8.0.15
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_agroregistry`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `url`, `method`, `ip`, `agent`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:24', '2021-08-30 05:30:24'),
(2, 'https://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:24', '2021-08-30 05:30:24'),
(3, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:25', '2021-08-30 05:30:25'),
(4, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:26', '2021-08-30 05:30:26'),
(5, 'https://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:27', '2021-08-30 05:30:27'),
(6, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:27', '2021-08-30 05:30:27'),
(7, 'https://agroreestr.teampro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:29', '2021-08-30 05:30:29'),
(8, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:29', '2021-08-30 05:30:29'),
(9, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:30', '2021-08-30 05:30:30'),
(10, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:30', '2021-08-30 05:30:30'),
(11, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=js%2Fapp.js', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:30', '2021-08-30 05:30:30'),
(12, 'https://agroreestr.teampro.uz/admin/bread', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:44', '2021-08-30 05:30:44'),
(13, 'https://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:47', '2021-08-30 05:30:47'),
(14, 'https://agroreestr.teampro.uz/admin/bread/activities/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:30:49', '2021-08-30 05:30:49'),
(15, 'https://agroreestr.teampro.uz', 'GET', '127.0.0.1', 'Symfony', NULL, '2021-08-30 05:31:11', '2021-08-30 05:31:11'),
(16, 'https://agroreestr.teampro.uz', 'GET', '127.0.0.1', 'Symfony', NULL, '2021-08-30 05:31:16', '2021-08-30 05:31:16'),
(17, 'https://agroreestr.teampro.uz/admin/bread', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:31:22', '2021-08-30 05:31:22'),
(18, 'https://agroreestr.teampro.uz/admin/bread', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:31:23', '2021-08-30 05:31:23'),
(19, 'https://agroreestr.teampro.uz/admin/activities', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:31:28', '2021-08-30 05:31:28'),
(20, 'https://agroreestr.teampro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:32:55', '2021-08-30 05:32:55'),
(21, 'https://agroreestr.teampro.uz/admin/menus/1/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:32:57', '2021-08-30 05:32:57'),
(22, 'https://agroreestr.teampro.uz/admin/menus/1/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:33:08', '2021-08-30 05:33:08'),
(23, 'https://agroreestr.teampro.uz/admin/menus/1/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:33:12', '2021-08-30 05:33:12'),
(24, 'https://agroreestr.teampro.uz/admin/menus/1/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:33:16', '2021-08-30 05:33:16'),
(25, 'https://agroreestr.teampro.uz/admin/compass', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:33:35', '2021-08-30 05:33:35'),
(26, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=images%2Fbg.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:33:35', '2021-08-30 05:33:35'),
(27, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=images%2Fcompass%2Fdocumentation.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:33:35', '2021-08-30 05:33:35'),
(28, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=images%2Fcompass%2Fvoyager-home.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:33:36', '2021-08-30 05:33:36'),
(29, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=images%2Fcompass%2Fhooks.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:33:36', '2021-08-30 05:33:36'),
(30, 'https://agroreestr.teampro.uz/admin/roles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:33:44', '2021-08-30 05:33:44'),
(31, 'https://agroreestr.teampro.uz/admin/users', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:33:45', '2021-08-30 05:33:45'),
(32, 'https://agroreestr.teampro.uz/admin/menus/1/item', 'PUT', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:34:24', '2021-08-30 05:34:24'),
(33, 'https://agroreestr.teampro.uz/admin/menus/1/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:34:24', '2021-08-30 05:34:24'),
(34, 'https://agroreestr.teampro.uz/admin/activities', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:34:31', '2021-08-30 05:34:31'),
(35, 'http://agroreestr.teampro.uz/admin/logout', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:35:03', '2021-08-30 05:35:03'),
(36, 'http://agroreestr.teampro.uz/admin/logout', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:35:14', '2021-08-30 05:35:14'),
(37, 'http://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:35:15', '2021-08-30 05:35:15'),
(38, 'http://reestr.agro.uz/admin/logout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', NULL, '2021-08-30 05:36:54', '2021-08-30 05:36:54'),
(39, 'http://reestr.agro.uz/admin/logout', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', NULL, '2021-08-30 05:37:59', '2021-08-30 05:37:59'),
(40, 'http://reestr.agro.uz/admin/login', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0', NULL, '2021-08-30 05:37:59', '2021-08-30 05:37:59'),
(41, 'http://agroreestr.teampro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:47:56', '2021-08-30 05:47:56'),
(42, 'http://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:47:56', '2021-08-30 05:47:56'),
(43, 'http://agroreestr.teampro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:48:03', '2021-08-30 05:48:03'),
(44, 'http://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:48:04', '2021-08-30 05:48:04'),
(45, 'http://agroreestr.teampro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:48:09', '2021-08-30 05:48:09'),
(46, 'http://agroreestr.teampro.uz/admin/settings', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:48:10', '2021-08-30 05:48:10'),
(47, 'http://agroreestr.teampro.uz/admin/products', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:48:26', '2021-08-30 05:48:26'),
(48, 'http://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:48:31', '2021-08-30 05:48:31'),
(49, 'http://agroreestr.teampro.uz/admin/logout', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:48:38', '2021-08-30 05:48:38'),
(50, 'http://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:48:38', '2021-08-30 05:48:38'),
(51, 'http://agroreestr.teampro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:48:57', '2021-08-30 05:48:57'),
(52, 'http://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:48:58', '2021-08-30 05:48:58'),
(53, 'http://agroreestr.teampro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:50:14', '2021-08-30 05:50:14'),
(54, 'http://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 05:50:14', '2021-08-30 05:50:14'),
(55, 'http://agroreestr.teampro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 06:45:34', '2021-08-30 06:45:34'),
(56, 'http://agroreestr.teampro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 07:37:38', '2021-08-30 07:37:38'),
(57, 'http://agroreestr.teampro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-30 07:37:42', '2021-08-30 07:37:42'),
(58, 'http://reestr.agro.uz', 'GET', '213.230.117.231', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', NULL, '2021-08-30 12:16:41', '2021-08-30 12:16:41'),
(59, 'http://reestr.agro.uz/admin', 'GET', '213.230.117.231', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', NULL, '2021-08-30 12:17:44', '2021-08-30 12:17:44'),
(60, 'http://reestr.agro.uz/admin/login', 'GET', '213.230.117.231', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', NULL, '2021-08-30 12:17:44', '2021-08-30 12:17:44'),
(61, 'http://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.117.231', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', NULL, '2021-08-30 12:17:44', '2021-08-30 12:17:44'),
(62, 'http://reestr.agro.uz', 'GET', '185.139.137.108', 'Mozilla/5.0 (Linux; Android 10; Redmi Note 8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.105 Mobile Safari/537.36', NULL, '2021-08-30 12:19:34', '2021-08-30 12:19:34'),
(63, 'http://reestr.agro.uz', 'GET', '213.230.117.231', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', NULL, '2021-08-30 12:43:16', '2021-08-30 12:43:16'),
(64, 'http://reestr.agro.uz', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-08-31 00:28:11', '2021-08-31 00:28:11'),
(65, 'http://reestr.agro.uz', 'GET', '195.158.3.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 00:33:34', '2021-08-31 00:33:34'),
(66, 'http://reestr.agro.uz', 'GET', '34.209.167.186', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', NULL, '2021-08-31 00:33:36', '2021-08-31 00:33:36'),
(67, 'http://reestr.agro.uz/admin', 'GET', '195.158.3.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 00:34:02', '2021-08-31 00:34:02'),
(68, 'http://reestr.agro.uz/admin/login', 'GET', '195.158.3.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 00:34:03', '2021-08-31 00:34:03'),
(69, 'http://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '195.158.3.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 00:34:03', '2021-08-31 00:34:03'),
(70, 'http://agroreestr.teampro.uz', 'GET', '34.219.8.2', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', NULL, '2021-08-31 00:34:05', '2021-08-31 00:34:05'),
(71, 'http://reestr.agro.uz', 'GET', '195.158.3.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 00:34:48', '2021-08-31 00:34:48'),
(72, 'http://reestr.agro.uz', 'GET', '195.158.3.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 00:46:16', '2021-08-31 00:46:16'),
(73, 'http://reestr.agro.uz', 'GET', '195.158.3.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 00:46:35', '2021-08-31 00:46:35'),
(74, 'http://reestr.agro.uz/admin', 'GET', '195.158.3.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 00:46:41', '2021-08-31 00:46:41'),
(75, 'http://reestr.agro.uz/admin/login', 'GET', '195.158.3.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 00:46:41', '2021-08-31 00:46:41'),
(76, 'http://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '195.158.3.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 00:46:41', '2021-08-31 00:46:41'),
(77, 'http://reestr.agro.uz', 'GET', '91.212.89.52', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 00:50:35', '2021-08-31 00:50:35'),
(78, 'http://agroreestr.teampro.uz', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko', NULL, '2021-08-31 02:22:15', '2021-08-31 02:22:15'),
(79, 'http://reestr.agro.uz', 'GET', '195.158.3.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-08-31 04:49:15', '2021-08-31 04:49:15'),
(80, 'http://reestr.agro.uz', 'GET', '114.119.130.151', 'Mozilla/5.0 (compatible;PetalBot;+https://webmaster.petalsearch.com/site/petalbot)', NULL, '2021-08-31 13:25:41', '2021-08-31 13:25:41'),
(81, 'https://agroreestr.teampro.uz', 'GET', '92.38.107.2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-01 03:25:14', '2021-09-01 03:25:14'),
(82, 'https://agroreestr.teampro.uz/admin', 'GET', '92.38.107.2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-01 03:25:28', '2021-09-01 03:25:28'),
(83, 'https://agroreestr.teampro.uz/admin/login', 'GET', '92.38.107.2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-01 03:25:29', '2021-09-01 03:25:29'),
(84, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '92.38.107.2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-01 03:25:29', '2021-09-01 03:25:29'),
(85, 'https://agroreestr.teampro.uz/admin/logout', 'GET', '92.38.107.2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-01 03:25:32', '2021-09-01 03:25:32'),
(86, 'https://agroreestr.teampro.uz/admin/logouthttps://labor.briks.uz', 'GET', '92.38.107.2', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-01 03:25:46', '2021-09-01 03:25:46'),
(87, 'http://reestr.agro.uz', 'GET', '185.139.137.108', 'Mozilla/5.0 (Linux; Android 10; Redmi Note 8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.105 Mobile Safari/537.36', NULL, '2021-09-01 04:31:55', '2021-09-01 04:31:55'),
(88, 'http://reestr.agro.uz', 'GET', '54.216.137.178', 'webprosbot/2.0 (+mailto:abuse-6337@webpros.com)', NULL, '2021-09-02 01:22:00', '2021-09-02 01:22:00'),
(89, 'https://reestr.agro.uz', 'GET', '54.216.137.178', 'webprosbot/2.0 (+mailto:abuse-6337@webpros.com)', NULL, '2021-09-02 01:22:16', '2021-09-02 01:22:16'),
(90, 'http://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:25:43', '2021-09-02 03:25:43'),
(91, 'http://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:25:43', '2021-09-02 03:25:43'),
(92, 'http://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:25:44', '2021-09-02 03:25:44'),
(93, 'http://agroreestr.teampro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:26:01', '2021-09-02 03:26:01'),
(94, 'http://agroreestr.teampro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:26:01', '2021-09-02 03:26:01'),
(95, 'http://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:26:02', '2021-09-02 03:26:02'),
(96, 'http://agroreestr.teampro.uz/admin/voyager-assets?path=js%2Fapp.js', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:26:03', '2021-09-02 03:26:03'),
(97, 'http://agroreestr.teampro.uz/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:26:03', '2021-09-02 03:26:03'),
(98, 'http://agroreestr.teampro.uz/admin/vehicles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:26:14', '2021-09-02 03:26:14'),
(99, 'http://agroreestr.teampro.uz/_ignition/health-check', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:26:15', '2021-09-02 03:26:15'),
(100, 'http://agroreestr.teampro.uz/admin/contracts', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:26:25', '2021-09-02 03:26:25'),
(101, 'http://agroreestr.teampro.uz/admin/products', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:26:33', '2021-09-02 03:26:33'),
(102, 'http://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:26:41', '2021-09-02 03:26:41'),
(103, 'http://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 03:26:59', '2021-09-02 03:26:59'),
(104, 'https://reestr.agro.uz', 'GET', '84.54.68.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 14:17:14', '2021-09-02 14:17:14'),
(105, 'https://reestr.agro.uz/admin', 'GET', '84.54.68.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 14:17:50', '2021-09-02 14:17:50'),
(106, 'https://reestr.agro.uz/admin/login', 'GET', '84.54.68.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 14:17:51', '2021-09-02 14:17:51'),
(107, 'https://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '84.54.68.47', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-02 14:17:51', '2021-09-02 14:17:51'),
(108, 'http://reestr.agro.uz', 'GET', '5.255.231.123', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', NULL, '2021-09-03 02:55:19', '2021-09-03 02:55:19'),
(109, 'http://reestr.agro.uz/accounts/faq', 'GET', '66.249.64.73', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.119 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', NULL, '2021-09-03 08:37:33', '2021-09-03 08:37:33'),
(110, 'https://reestr.agro.uz', 'GET', '92.118.160.5', 'NetSystemsResearch studies the availability of various services across the internet. Our website is netsystemsresearch.com', NULL, '2021-09-03 22:45:05', '2021-09-03 22:45:05'),
(111, 'http://reestr.agro.uz', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-04 04:13:02', '2021-09-04 04:13:02'),
(112, 'http://reestr.agro.uz/accounts/faq', 'GET', '66.249.64.75', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', NULL, '2021-09-04 12:12:00', '2021-09-04 12:12:00'),
(113, 'http://reestr.agro.uz/accounts/faq', 'GET', '66.249.64.75', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.119 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', NULL, '2021-09-05 01:41:11', '2021-09-05 01:41:11'),
(114, 'http://reestr.agro.uz', 'GET', '84.54.75.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-05 02:18:12', '2021-09-05 02:18:12'),
(115, 'http://reestr.agro.uz/admin', 'GET', '84.54.75.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-05 02:18:17', '2021-09-05 02:18:17'),
(116, 'http://reestr.agro.uz/admin/login', 'GET', '84.54.75.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-05 02:18:17', '2021-09-05 02:18:17'),
(117, 'http://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '84.54.75.153', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-05 02:18:17', '2021-09-05 02:18:17'),
(118, 'http://reestr.agro.uz', 'GET', '5.255.231.123', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.268', NULL, '2021-09-05 04:04:47', '2021-09-05 04:04:47'),
(119, 'http://reestr.agro.uz/admin', 'GET', '5.255.231.123', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', NULL, '2021-09-05 04:05:44', '2021-09-05 04:05:44'),
(120, 'http://reestr.agro.uz/admin/login', 'GET', '5.255.231.123', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', NULL, '2021-09-05 04:05:47', '2021-09-05 04:05:47'),
(121, 'https://agroreestr.teampro.uz', 'GET', '5.255.231.123', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.268', NULL, '2021-09-05 04:06:40', '2021-09-05 04:06:40'),
(122, 'http://agroreestr.teampro.uz', 'GET', '84.54.92.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-05 04:09:50', '2021-09-05 04:09:50'),
(123, 'http://agroreestr.teampro.uz', 'GET', '199.250.251.36', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', NULL, '2021-09-05 04:09:52', '2021-09-05 04:09:52'),
(124, 'http://agroreestr.teampro.uz/admin', 'GET', '84.54.92.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-05 04:10:46', '2021-09-05 04:10:46'),
(125, 'http://agroreestr.teampro.uz/admin/login', 'GET', '84.54.92.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-05 04:10:46', '2021-09-05 04:10:46'),
(126, 'http://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '84.54.92.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-05 04:10:47', '2021-09-05 04:10:47'),
(127, 'http://reestr.agro.uz/content/2', 'GET', '5.255.253.124', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', NULL, '2021-09-05 04:23:22', '2021-09-05 04:23:22'),
(128, 'http://agroreestr.teampro.uz', 'GET', '92.118.160.37', 'NetSystemsResearch studies the availability of various services across the internet. Our website is netsystemsresearch.com', NULL, '2021-09-05 05:04:09', '2021-09-05 05:04:09'),
(129, 'http://reestr.agro.uz', 'GET', '213.230.113.123', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-05 12:27:20', '2021-09-05 12:27:20'),
(130, 'http://reestr.agro.uz', 'GET', '82.215.102.238', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-05 12:27:24', '2021-09-05 12:27:24'),
(131, 'http://reestr.agro.uz', 'GET', '82.215.106.112', 'Mozilla/5.0 (Linux; Android 11; M2101K7AG) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.62 Mobile Safari/537.36', NULL, '2021-09-05 12:27:29', '2021-09-05 12:27:29'),
(132, 'http://reestr.agro.uz', 'GET', '82.215.102.238', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-05 14:38:07', '2021-09-05 14:38:07'),
(133, 'http://reestr.agro.uz', 'GET', '149.154.161.16', 'TelegramBot (like TwitterBot)', NULL, '2021-09-05 14:38:24', '2021-09-05 14:38:24'),
(134, 'http://reestr.agro.uz', 'GET', '37.110.211.208', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-05 14:38:28', '2021-09-05 14:38:28'),
(135, 'http://reestr.agro.uz', 'GET', '82.215.102.238', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-05 15:15:34', '2021-09-05 15:15:34'),
(136, 'https://agroreestr.teampro.uz', 'GET', '5.255.231.123', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.268', NULL, '2021-09-05 15:19:16', '2021-09-05 15:19:16'),
(137, 'https://agroreestr.teampro.uz', 'GET', '5.255.231.123', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.268', NULL, '2021-09-05 17:16:57', '2021-09-05 17:16:57'),
(138, 'https://agroreestr.teampro.uz', 'GET', '5.255.231.123', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.268', NULL, '2021-09-05 19:17:14', '2021-09-05 19:17:14'),
(139, 'http://reestr.agro.uz', 'GET', '94.232.25.182', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-05 22:36:48', '2021-09-05 22:36:48'),
(140, 'http://reestr.agro.uz', 'GET', '5.255.231.123', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.268', NULL, '2021-09-06 00:19:39', '2021-09-06 00:19:39'),
(141, 'http://agroreestr.teampro.uz', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:09:42', '2021-09-07 01:09:42'),
(142, 'http://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 01:29:34', '2021-09-07 01:29:34'),
(143, 'http://agroreestr.teampro.uz', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:31:49', '2021-09-07 01:31:49'),
(144, 'http://agroreestr.teampro.uz/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:31:52', '2021-09-07 01:31:52'),
(145, 'http://agroreestr.teampro.uz/admin/login', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:31:52', '2021-09-07 01:31:52'),
(146, 'http://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:31:53', '2021-09-07 01:31:53'),
(147, 'http://agroreestr.teampro.uz/admin/login', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:32:31', '2021-09-07 01:32:31'),
(148, 'http://agroreestr.teampro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:32:47', '2021-09-07 01:32:47'),
(149, 'http://agroreestr.teampro.uz/admin/login', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:32:47', '2021-09-07 01:32:47'),
(150, 'http://agroreestr.teampro.uz/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:32:47', '2021-09-07 01:32:47'),
(151, 'http://agroreestr.teampro.uz/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:32:48', '2021-09-07 01:32:48'),
(152, 'http://agroreestr.teampro.uz/admin/voyager-assets?path=js%2Fapp.js', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:32:48', '2021-09-07 01:32:48'),
(153, 'http://agroreestr.teampro.uz/admin/menus', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:33:31', '2021-09-07 01:33:31'),
(154, 'http://agroreestr.teampro.uz/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:33:36', '2021-09-07 01:33:36'),
(155, 'http://agroreestr.teampro.uz/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:34:01', '2021-09-07 01:34:01'),
(156, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 01:35:57', '2021-09-07 01:35:57'),
(157, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 01:35:58', '2021-09-07 01:35:58'),
(158, 'http://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 01:36:00', '2021-09-07 01:36:00'),
(159, 'http://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 01:36:00', '2021-09-07 01:36:00'),
(160, 'http://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 01:36:05', '2021-09-07 01:36:05'),
(161, 'http://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 01:36:49', '2021-09-07 01:36:49'),
(162, 'http://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 01:36:50', '2021-09-07 01:36:50'),
(163, 'http://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 01:42:58', '2021-09-07 01:42:58'),
(164, 'http://agroreestr.teampro.uz/admin/login', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:50:00', '2021-09-07 01:50:00'),
(165, 'http://agroreestr.teampro.uz/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:50:00', '2021-09-07 01:50:00'),
(166, 'http://agroreestr.teampro.uz/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:50:04', '2021-09-07 01:50:04'),
(167, 'http://agroreestr.teampro.uz/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 01:50:08', '2021-09-07 01:50:08'),
(168, 'http://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:07:15', '2021-09-07 02:07:15'),
(169, 'http://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:07:16', '2021-09-07 02:07:16'),
(170, 'http://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:07:18', '2021-09-07 02:07:18'),
(171, 'https://agroreestr.teampro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:07:24', '2021-09-07 02:07:24'),
(172, 'http://agroreestr.teampro.uz/admin/roles', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 02:15:09', '2021-09-07 02:15:09'),
(173, 'http://agroreestr.teampro.uz/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 02:16:55', '2021-09-07 02:16:55'),
(174, 'http://agroreestr.teampro.uz/admin/menus', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 02:17:24', '2021-09-07 02:17:24'),
(175, 'http://agroreestr.teampro.uz/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 02:17:28', '2021-09-07 02:17:28'),
(176, 'https://agroreestr.teampro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:18:41', '2021-09-07 02:18:41'),
(177, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:18:44', '2021-09-07 02:18:44'),
(178, 'https://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:18:44', '2021-09-07 02:18:44'),
(179, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:18:45', '2021-09-07 02:18:45'),
(180, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:21:22', '2021-09-07 02:21:22'),
(181, 'https://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:21:22', '2021-09-07 02:21:22'),
(182, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:21:23', '2021-09-07 02:21:23'),
(183, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:21:34', '2021-09-07 02:21:34'),
(184, 'https://agroreestr.teampro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:21:34', '2021-09-07 02:21:34'),
(185, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:21:35', '2021-09-07 02:21:35'),
(186, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=js%2Fapp.js', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:21:36', '2021-09-07 02:21:36'),
(187, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:21:36', '2021-09-07 02:21:36'),
(188, 'https://agroreestr.teampro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:14', '2021-09-07 02:22:14'),
(189, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:19', '2021-09-07 02:22:19'),
(190, 'https://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:19', '2021-09-07 02:22:19'),
(191, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:19', '2021-09-07 02:22:19'),
(192, 'https://agroreestr.teampro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:31', '2021-09-07 02:22:31'),
(193, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:31', '2021-09-07 02:22:31'),
(194, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:32', '2021-09-07 02:22:32'),
(195, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=js%2Fapp.js', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:32', '2021-09-07 02:22:32'),
(196, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:32', '2021-09-07 02:22:32'),
(197, 'https://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:40', '2021-09-07 02:22:40'),
(198, 'https://agroreestr.teampro.uz/admin/products', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:41', '2021-09-07 02:22:41'),
(199, 'https://agroreestr.teampro.uz/admin/contracts', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:47', '2021-09-07 02:22:47'),
(200, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:57', '2021-09-07 02:22:57'),
(201, 'https://agroreestr.teampro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:57', '2021-09-07 02:22:57'),
(202, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:57', '2021-09-07 02:22:57'),
(203, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=js%2Fapp.js', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:58', '2021-09-07 02:22:58'),
(204, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:22:58', '2021-09-07 02:22:58'),
(205, 'https://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:23:04', '2021-09-07 02:23:04'),
(206, 'https://agroreestr.teampro.uz/admin/vehicles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:25:08', '2021-09-07 02:25:08');
INSERT INTO `activities` (`id`, `url`, `method`, `ip`, `agent`, `user_id`, `created_at`, `updated_at`) VALUES
(207, 'https://agroreestr.teampro.uz/_ignition/health-check', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:25:12', '2021-09-07 02:25:12'),
(208, 'https://agroreestr.teampro.uz/admin/database/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:27:44', '2021-09-07 02:27:44'),
(209, 'https://agroreestr.teampro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:44:52', '2021-09-07 02:44:52'),
(210, 'https://agroreestr.teampro.uz', 'GET', '149.154.161.9', 'TelegramBot (like TwitterBot)', NULL, '2021-09-07 02:45:01', '2021-09-07 02:45:01'),
(211, 'https://agroreestr.teampro.uz', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 02:45:03', '2021-09-07 02:45:03'),
(212, 'https://agroreestr.teampro.uz', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:45:04', '2021-09-07 02:45:04'),
(213, 'https://reestr.agro.uz', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 02:45:18', '2021-09-07 02:45:18'),
(214, 'https://reestr.agro.uz', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:45:24', '2021-09-07 02:45:24'),
(215, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:45:25', '2021-09-07 02:45:25'),
(216, 'https://reestr.agro.uz', 'GET', '94.232.25.182', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:45:26', '2021-09-07 02:45:26'),
(217, 'https://reestr.agro.uz', 'GET', '94.232.25.182', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:45:26', '2021-09-07 02:45:26'),
(218, 'https://reestr.agro.uz', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 02:45:29', '2021-09-07 02:45:29'),
(219, 'https://reestr.agro.uz/admin', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:45:48', '2021-09-07 02:45:48'),
(220, 'https://reestr.agro.uz/admin/login', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:45:48', '2021-09-07 02:45:48'),
(221, 'https://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:45:49', '2021-09-07 02:45:49'),
(222, 'https://reestr.agro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:46:11', '2021-09-07 02:46:11'),
(223, 'https://reestr.agro.uz/admin/login', 'POST', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:46:11', '2021-09-07 02:46:11'),
(224, 'https://reestr.agro.uz/admin/login', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:46:12', '2021-09-07 02:46:12'),
(225, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:46:21', '2021-09-07 02:46:21'),
(226, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:46:29', '2021-09-07 02:46:29'),
(227, 'https://reestr.agro.uz', 'GET', '127.0.0.1', 'Symfony', NULL, '2021-09-07 02:47:49', '2021-09-07 02:47:49'),
(228, 'https://reestr.agro.uz/history', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:49:41', '2021-09-07 02:49:41'),
(229, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:50:02', '2021-09-07 02:50:02'),
(230, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:50:03', '2021-09-07 02:50:03'),
(231, 'https://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 02:50:03', '2021-09-07 02:50:03'),
(232, 'https://agroreestr.teampro.uz/admin/database', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:50:27', '2021-09-07 02:50:27'),
(233, 'https://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:50:28', '2021-09-07 02:50:28'),
(234, 'https://agroreestr.teampro.uz/admin/database/applications', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:50:37', '2021-09-07 02:50:37'),
(235, 'https://agroreestr.teampro.uz/admin/bread/applications/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:50:53', '2021-09-07 02:50:53'),
(236, 'https://agroreestr.teampro.uz/admin/bread', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:55:37', '2021-09-07 02:55:37'),
(237, 'https://agroreestr.teampro.uz/admin/bread', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:55:38', '2021-09-07 02:55:38'),
(238, 'https://agroreestr.teampro.uz/admin/applications', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:55:46', '2021-09-07 02:55:46'),
(239, 'https://agroreestr.teampro.uz/admin/applications/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:55:50', '2021-09-07 02:55:50'),
(240, 'https://agroreestr.teampro.uz/admin/bread', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:56:26', '2021-09-07 02:56:26'),
(241, 'https://agroreestr.teampro.uz/admin/bread/applications/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:56:31', '2021-09-07 02:56:31'),
(242, 'https://agroreestr.teampro.uz/admin/bread/20', 'PUT', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:56:47', '2021-09-07 02:56:47'),
(243, 'https://agroreestr.teampro.uz/admin/bread', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:56:47', '2021-09-07 02:56:47'),
(244, 'https://agroreestr.teampro.uz/admin/applications/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 02:56:50', '2021-09-07 02:56:50'),
(245, 'https://agroreestr.teampro.uz/admin/bread', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:33:15', '2021-09-07 03:33:15'),
(246, 'https://agroreestr.teampro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:33:54', '2021-09-07 03:33:54'),
(247, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:34:57', '2021-09-07 03:34:57'),
(248, 'https://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:41:02', '2021-09-07 03:41:02'),
(249, 'https://agroreestr.teampro.uz/admin/database/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:41:05', '2021-09-07 03:41:05'),
(250, 'https://agroreestr.teampro.uz/admin/database', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:43:16', '2021-09-07 03:43:16'),
(251, 'https://agroreestr.teampro.uz/admin/database/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:43:17', '2021-09-07 03:43:17'),
(252, 'https://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:43:46', '2021-09-07 03:43:46'),
(253, 'https://agroreestr.teampro.uz/admin/database/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:43:50', '2021-09-07 03:43:50'),
(254, 'https://agroreestr.teampro.uz/admin/database/menus/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:44:13', '2021-09-07 03:44:13'),
(255, 'https://agroreestr.teampro.uz/admin/database/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:44:41', '2021-09-07 03:44:41'),
(256, 'https://agroreestr.teampro.uz/admin/bread/menus/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:44:44', '2021-09-07 03:44:44'),
(257, 'https://agroreestr.teampro.uz/admin/bread/menus/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:50:07', '2021-09-07 03:50:07'),
(258, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:50:14', '2021-09-07 03:50:14'),
(259, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:50:18', '2021-09-07 03:50:18'),
(260, 'https://agroreestr.teampro.uz/admin/applications', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:50:40', '2021-09-07 03:50:40'),
(261, 'https://agroreestr.teampro.uz/admin/applications/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:50:45', '2021-09-07 03:50:45'),
(262, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:52:20', '2021-09-07 03:52:20'),
(263, 'https://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:52:30', '2021-09-07 03:52:30'),
(264, 'https://reestr.agro.uz', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36', NULL, '2021-09-07 03:52:37', '2021-09-07 03:52:37'),
(265, 'https://agroreestr.teampro.uz/admin/bread/applications/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:52:38', '2021-09-07 03:52:38'),
(266, 'https://agroreestr.teampro.uz/admin/database/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:52:45', '2021-09-07 03:52:45'),
(267, 'https://agroreestr.teampro.uz/admin/bread/applications/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:53:01', '2021-09-07 03:53:01'),
(268, 'https://agroreestr.teampro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:55:53', '2021-09-07 03:55:53'),
(269, 'https://agroreestr.teampro.uz/admin/menus/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:55:59', '2021-09-07 03:55:59'),
(270, 'https://agroreestr.teampro.uz/admin/menus', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:56:04', '2021-09-07 03:56:04'),
(271, 'https://agroreestr.teampro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:56:05', '2021-09-07 03:56:05'),
(272, 'https://agroreestr.teampro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 03:56:15', '2021-09-07 03:56:15'),
(273, 'https://agroreestr.teampro.uz/admin/database/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:01:59', '2021-09-07 04:01:59'),
(274, 'https://agroreestr.teampro.uz/admin/database', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:03:11', '2021-09-07 04:03:11'),
(275, 'https://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:03:15', '2021-09-07 04:03:15'),
(276, 'https://agroreestr.teampro.uz/admin/bread/lang_menus/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:03:29', '2021-09-07 04:03:29'),
(277, 'http://reestr.agro.uz/accounts/faq', 'GET', '66.249.66.156', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.119 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', NULL, '2021-09-07 04:04:00', '2021-09-07 04:04:00'),
(278, 'https://agroreestr.teampro.uz/admin/bread', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:04:35', '2021-09-07 04:04:35'),
(279, 'https://agroreestr.teampro.uz/admin/bread', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:04:36', '2021-09-07 04:04:36'),
(280, 'https://agroreestr.teampro.uz/admin/lang-menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:04:41', '2021-09-07 04:04:41'),
(281, 'https://agroreestr.teampro.uz/admin/lang-menus/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:04:44', '2021-09-07 04:04:44'),
(282, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:05:56', '2021-09-07 04:05:56'),
(283, 'https://agroreestr.teampro.uz/admin/lang-menus/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:11:59', '2021-09-07 04:11:59'),
(284, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:14:55', '2021-09-07 04:14:55'),
(285, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:14:55', '2021-09-07 04:14:55'),
(286, 'https://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:14:56', '2021-09-07 04:14:56'),
(287, 'https://agroreestr.teampro.uz/admin/bread/lang_menus/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:17:49', '2021-09-07 04:17:49'),
(288, 'https://reestr.agro.uz/admin/login', 'GET', '149.154.161.16', 'TelegramBot (like TwitterBot)', NULL, '2021-09-07 04:17:56', '2021-09-07 04:17:56'),
(289, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:17:59', '2021-09-07 04:17:59'),
(290, 'https://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:18:00', '2021-09-07 04:18:00'),
(291, 'https://reestr.agro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:19:06', '2021-09-07 04:19:06'),
(292, 'https://reestr.agro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:19:07', '2021-09-07 04:19:07'),
(293, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:19:08', '2021-09-07 04:19:08'),
(294, 'https://reestr.agro.uz/admin/voyager-assets?path=js%2Fapp.js', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:19:08', '2021-09-07 04:19:08'),
(295, 'https://reestr.agro.uz/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:19:09', '2021-09-07 04:19:09'),
(296, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:19:16', '2021-09-07 04:19:16'),
(297, 'https://reestr.agro.uz/admin/roles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:19:18', '2021-09-07 04:19:18'),
(298, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:19:27', '2021-09-07 04:19:27'),
(299, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:19:54', '2021-09-07 04:19:54'),
(300, 'https://reestr.agro.uz/admin/products', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:21:59', '2021-09-07 04:21:59'),
(301, 'https://reestr.agro.uz/admin/vehicles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:22:03', '2021-09-07 04:22:03'),
(302, 'https://reestr.agro.uz/_ignition/health-check', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:22:07', '2021-09-07 04:22:07'),
(303, 'https://agroreestr.teampro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:22:12', '2021-09-07 04:22:12'),
(304, 'https://reestr.agro.uz/admin/lang-menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:22:13', '2021-09-07 04:22:13'),
(305, 'https://reestr.agro.uz/admin/contracts', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:23:45', '2021-09-07 04:23:45'),
(306, 'https://reestr.agro.uz/admin/products', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:23:51', '2021-09-07 04:23:51'),
(307, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:27:05', '2021-09-07 04:27:05'),
(308, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:31:42', '2021-09-07 04:31:42'),
(309, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:32:12', '2021-09-07 04:32:12'),
(310, 'https://agroreestr.teampro.uz/admin/bread/21', 'DELETE', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:39:20', '2021-09-07 04:39:20'),
(311, 'https://agroreestr.teampro.uz/admin/bread', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:39:21', '2021-09-07 04:39:21'),
(312, 'https://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:39:27', '2021-09-07 04:39:27'),
(313, 'https://agroreestr.teampro.uz/admin/database/lang_menus', 'DELETE', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:39:34', '2021-09-07 04:39:34'),
(314, 'https://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:39:35', '2021-09-07 04:39:35'),
(315, 'https://agroreestr.teampro.uz/admin/database/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:39:39', '2021-09-07 04:39:39'),
(316, 'https://agroreestr.teampro.uz/admin/database', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:43:40', '2021-09-07 04:43:40'),
(317, 'https://agroreestr.teampro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 04:43:40', '2021-09-07 04:43:40'),
(318, 'https://agroreestr.teampro.uz', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 04:45:28', '2021-09-07 04:45:28'),
(319, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:08:48', '2021-09-07 05:08:48'),
(320, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:08:54', '2021-09-07 05:08:54'),
(321, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:09:00', '2021-09-07 05:09:00'),
(322, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:09:04', '2021-09-07 05:09:04'),
(323, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:09:06', '2021-09-07 05:09:06'),
(324, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:09:06', '2021-09-07 05:09:06'),
(325, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:09:12', '2021-09-07 05:09:12'),
(326, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:10:01', '2021-09-07 05:10:01'),
(327, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:10:16', '2021-09-07 05:10:16'),
(328, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:11:22', '2021-09-07 05:11:22'),
(329, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:15:09', '2021-09-07 05:15:09'),
(330, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:15:09', '2021-09-07 05:15:09'),
(331, 'https://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:15:10', '2021-09-07 05:15:10'),
(332, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:18:27', '2021-09-07 05:18:27'),
(333, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:18:28', '2021-09-07 05:18:28'),
(334, 'https://agroreestr.teampro.uz/admin/bread/navbar_menus/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:19:35', '2021-09-07 05:19:35'),
(335, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:20:36', '2021-09-07 05:20:36'),
(336, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:20:36', '2021-09-07 05:20:36'),
(337, 'https://reestr.agro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:20:50', '2021-09-07 05:20:50'),
(338, 'https://reestr.agro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:20:51', '2021-09-07 05:20:51'),
(339, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:20:51', '2021-09-07 05:20:51'),
(340, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:20:54', '2021-09-07 05:20:54'),
(341, 'https://reestr.agro.uz/admin/users', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:20:56', '2021-09-07 05:20:56'),
(342, 'https://reestr.agro.uz/admin/media', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:20:58', '2021-09-07 05:20:58'),
(343, 'https://reestr.agro.uz/admin/media/files', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:20:59', '2021-09-07 05:20:59'),
(344, 'https://reestr.agro.uz/admin/posts', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:01', '2021-09-07 05:21:01'),
(345, 'https://reestr.agro.uz/admin/settings', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:04', '2021-09-07 05:21:04'),
(346, 'https://reestr.agro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:21:06', '2021-09-07 05:21:06'),
(347, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:21:07', '2021-09-07 05:21:07'),
(348, 'https://reestr.agro.uz/admin/contracts', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:07', '2021-09-07 05:21:07'),
(349, 'https://reestr.agro.uz/admin/applications', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:09', '2021-09-07 05:21:09'),
(350, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:25', '2021-09-07 05:21:25'),
(351, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:27', '2021-09-07 05:21:27'),
(352, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:27', '2021-09-07 05:21:27'),
(353, 'https://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:28', '2021-09-07 05:21:28'),
(354, 'https://reestr.agro.uz/admin/roles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:35', '2021-09-07 05:21:35'),
(355, 'https://reestr.agro.uz/admin/settings', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:38', '2021-09-07 05:21:38'),
(356, 'https://reestr.agro.uz/admin/products', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:40', '2021-09-07 05:21:40'),
(357, 'https://reestr.agro.uz/admin/contracts', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:43', '2021-09-07 05:21:43'),
(358, 'https://reestr.agro.uz/admin/vehicles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:46', '2021-09-07 05:21:46'),
(359, 'https://reestr.agro.uz/_ignition/health-check', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:46', '2021-09-07 05:21:46'),
(360, 'https://reestr.agro.uz/admin/applications', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:21:52', '2021-09-07 05:21:52'),
(361, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:11', '2021-09-07 05:23:11'),
(362, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:12', '2021-09-07 05:23:12'),
(363, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:23:16', '2021-09-07 05:23:16'),
(364, 'https://reestr.agro.uz/admin/users', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:23:19', '2021-09-07 05:23:19'),
(365, 'https://reestr.agro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:23:20', '2021-09-07 05:23:20'),
(366, 'https://reestr.agro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:23:20', '2021-09-07 05:23:20'),
(367, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:23:20', '2021-09-07 05:23:20'),
(368, 'https://reestr.agro.uz/admin/voyager-assets?path=js%2Fapp.js', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:23:21', '2021-09-07 05:23:21'),
(369, 'https://reestr.agro.uz/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:23:21', '2021-09-07 05:23:21'),
(370, 'https://reestr.agro.uz/admin/media', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:23:27', '2021-09-07 05:23:27'),
(371, 'https://reestr.agro.uz/admin/media/files', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:23:27', '2021-09-07 05:23:27'),
(372, 'https://reestr.agro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:27', '2021-09-07 05:23:27'),
(373, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:28', '2021-09-07 05:23:28'),
(374, 'https://reestr.agro.uz/admin/voyager-assets?path=js%2Fapp.js', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:28', '2021-09-07 05:23:28'),
(375, 'https://reestr.agro.uz/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:28', '2021-09-07 05:23:28'),
(376, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:23:40', '2021-09-07 05:23:40'),
(377, 'https://reestr.agro.uz/admin/roles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:40', '2021-09-07 05:23:40'),
(378, 'https://reestr.agro.uz/admin/roles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:23:41', '2021-09-07 05:23:41'),
(379, 'https://reestr.agro.uz/admin/media', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:43', '2021-09-07 05:23:43'),
(380, 'https://reestr.agro.uz/admin/media/files', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:43', '2021-09-07 05:23:43'),
(381, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:45', '2021-09-07 05:23:45'),
(382, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:49', '2021-09-07 05:23:49'),
(383, 'https://reestr.agro.uz/admin/roles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:51', '2021-09-07 05:23:51'),
(384, 'https://reestr.agro.uz/admin/media', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:52', '2021-09-07 05:23:52'),
(385, 'https://reestr.agro.uz/admin/media/files', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:52', '2021-09-07 05:23:52'),
(386, 'https://reestr.agro.uz/admin/pages', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:53', '2021-09-07 05:23:53'),
(387, 'https://reestr.agro.uz/admin/activities', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:23:55', '2021-09-07 05:23:55'),
(388, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:24:00', '2021-09-07 05:24:00'),
(389, 'https://reestr.agro.uz/admin/users', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:24:20', '2021-09-07 05:24:20'),
(390, 'https://reestr.agro.uz/admin/roles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:24:51', '2021-09-07 05:24:51'),
(391, 'https://reestr.agro.uz/admin/users', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:24:57', '2021-09-07 05:24:57'),
(392, 'https://reestr.agro.uz/admin/pages', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:25:09', '2021-09-07 05:25:09'),
(393, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:25:10', '2021-09-07 05:25:10'),
(394, 'https://reestr.agro.uz/admin/users', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:25:12', '2021-09-07 05:25:12'),
(395, 'https://reestr.agro.uz/admin/media', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:25:14', '2021-09-07 05:25:14'),
(396, 'https://reestr.agro.uz/admin/media/files', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:25:15', '2021-09-07 05:25:15'),
(397, 'https://reestr.agro.uz/admin/posts', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:25:15', '2021-09-07 05:25:15'),
(398, 'https://reestr.agro.uz/admin/pages', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:25:16', '2021-09-07 05:25:16'),
(399, 'https://reestr.agro.uz/admin/activities', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:25:24', '2021-09-07 05:25:24'),
(400, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:25:54', '2021-09-07 05:25:54'),
(401, 'https://reestr.agro.uz/admin/roles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:26:04', '2021-09-07 05:26:04'),
(402, 'https://reestr.agro.uz/admin/roles/3', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:26:10', '2021-09-07 05:26:10'),
(403, 'https://reestr.agro.uz/admin/voyager-assets?path=fonts%2Fbootstrap%2Fglyphicons-halflings-regular.woff2', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:26:11', '2021-09-07 05:26:11'),
(404, 'https://agroreestr.teampro.uz/admin/bread', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:36:21', '2021-09-07 05:36:21'),
(405, 'https://agroreestr.teampro.uz/admin/bread', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:36:21', '2021-09-07 05:36:21'),
(406, 'https://agroreestr.teampro.uz/admin/database/navbar_menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:36:31', '2021-09-07 05:36:31'),
(407, 'https://agroreestr.teampro.uz/admin/bread/navbar_menus/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:36:41', '2021-09-07 05:36:41'),
(408, 'https://agroreestr.teampro.uz/admin/navbar-menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:37:32', '2021-09-07 05:37:32'),
(409, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:37:37', '2021-09-07 05:37:37'),
(410, 'https://agroreestr.teampro.uz/admin/applications', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:38:01', '2021-09-07 05:38:01'),
(411, 'https://agroreestr.teampro.uz/admin/navbar-menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:38:07', '2021-09-07 05:38:07');
INSERT INTO `activities` (`id`, `url`, `method`, `ip`, `agent`, `user_id`, `created_at`, `updated_at`) VALUES
(412, 'https://agroreestr.teampro.uz/admin/navbar-menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:45:58', '2021-09-07 05:45:58'),
(413, 'https://agroreestr.teampro.uz/admin/bread', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:46:06', '2021-09-07 05:46:06'),
(414, 'https://agroreestr.teampro.uz/admin/database/navbar_menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:46:11', '2021-09-07 05:46:11'),
(415, 'https://agroreestr.teampro.uz/admin/database/navbar_menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:48:24', '2021-09-07 05:48:24'),
(416, 'https://reestr.agro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:57:55', '2021-09-07 05:57:55'),
(417, 'https://reestr.agro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:57:55', '2021-09-07 05:57:55'),
(418, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:57:55', '2021-09-07 05:57:55'),
(419, 'https://reestr.agro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:58:09', '2021-09-07 05:58:09'),
(420, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:58:10', '2021-09-07 05:58:10'),
(421, 'https://reestr.agro.uz/admin/voyager-assets?path=js%2Fapp.js', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:58:10', '2021-09-07 05:58:10'),
(422, 'https://reestr.agro.uz/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:58:10', '2021-09-07 05:58:10'),
(423, 'https://reestr.agro.uz/admin/navbar-menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:58:21', '2021-09-07 05:58:21'),
(424, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:58:34', '2021-09-07 05:58:34'),
(425, 'https://reestr.agro.uz/admin/navbar-menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:58:39', '2021-09-07 05:58:39'),
(426, 'https://reestr.agro.uz/admin/settings', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:58:50', '2021-09-07 05:58:50'),
(427, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:58:54', '2021-09-07 05:58:54'),
(428, 'https://reestr.agro.uz/admin/vehicles', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:58:54', '2021-09-07 05:58:54'),
(429, 'https://reestr.agro.uz/_ignition/health-check', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:58:55', '2021-09-07 05:58:55'),
(430, 'https://reestr.agro.uz/admin/navbar-menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 05:59:33', '2021-09-07 05:59:33'),
(431, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:59:43', '2021-09-07 05:59:43'),
(432, 'https://reestr.agro.uz/admin/menus/2/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:59:52', '2021-09-07 05:59:52'),
(433, 'https://reestr.agro.uz/admin/menus/2', 'PUT', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:59:59', '2021-09-07 05:59:59'),
(434, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 05:59:59', '2021-09-07 05:59:59'),
(435, 'https://reestr.agro.uz/admin/navbar-menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:00:02', '2021-09-07 06:00:02'),
(436, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:00:03', '2021-09-07 06:00:03'),
(437, 'https://reestr.agro.uz/admin/applications', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:00:07', '2021-09-07 06:00:07'),
(438, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:00:14', '2021-09-07 06:00:14'),
(439, 'https://reestr.agro.uz/admin/menus/2/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:00:28', '2021-09-07 06:00:28'),
(440, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:00:28', '2021-09-07 06:00:28'),
(441, 'https://reestr.agro.uz/admin/menus/2/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:00:43', '2021-09-07 06:00:43'),
(442, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:00:43', '2021-09-07 06:00:43'),
(443, 'https://reestr.agro.uz/admin/menus/2/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:00:48', '2021-09-07 06:00:48'),
(444, 'https://reestr.agro.uz/admin/menus/2/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:01:07', '2021-09-07 06:01:07'),
(445, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:01:08', '2021-09-07 06:01:08'),
(446, 'https://reestr.agro.uz/admin/menus/2/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:01:22', '2021-09-07 06:01:22'),
(447, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:01:22', '2021-09-07 06:01:22'),
(448, 'https://reestr.agro.uz/admin/menus/2/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:01:33', '2021-09-07 06:01:33'),
(449, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:01:33', '2021-09-07 06:01:33'),
(450, 'https://reestr.agro.uz/admin/menus/2/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:01:44', '2021-09-07 06:01:44'),
(451, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:01:44', '2021-09-07 06:01:44'),
(452, 'https://reestr.agro.uz/admin/menus/2/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:02:05', '2021-09-07 06:02:05'),
(453, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:02:06', '2021-09-07 06:02:06'),
(454, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:02:08', '2021-09-07 06:02:08'),
(455, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:02:15', '2021-09-07 06:02:15'),
(456, 'https://reestr.agro.uz/admin/menus/2/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:02:15', '2021-09-07 06:02:15'),
(457, 'https://reestr.agro.uz/admin/menus/2/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:02:17', '2021-09-07 06:02:17'),
(458, 'https://reestr.agro.uz/admin/menus/2/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:02:53', '2021-09-07 06:02:53'),
(459, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:02:58', '2021-09-07 06:02:58'),
(460, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:21', '2021-09-07 06:03:21'),
(461, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:21', '2021-09-07 06:03:21'),
(462, 'https://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:21', '2021-09-07 06:03:21'),
(463, 'https://reestr.agro.uz/admin/menus/2/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:28', '2021-09-07 06:03:28'),
(464, 'https://reestr.agro.uz/admin/menus/2/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:30', '2021-09-07 06:03:30'),
(465, 'https://reestr.agro.uz/admin/menus/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:03:32', '2021-09-07 06:03:32'),
(466, 'https://reestr.agro.uz/admin/menus/2/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:34', '2021-09-07 06:03:34'),
(467, 'https://reestr.agro.uz/admin/menus/2/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:37', '2021-09-07 06:03:37'),
(468, 'https://reestr.agro.uz/admin/menus/2/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:37', '2021-09-07 06:03:37'),
(469, 'https://reestr.agro.uz/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:38', '2021-09-07 06:03:38'),
(470, 'https://reestr.agro.uz/admin/login', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:38', '2021-09-07 06:03:38'),
(471, 'https://reestr.agro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:38', '2021-09-07 06:03:38'),
(472, 'https://reestr.agro.uz/admin/menus', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:03:39', '2021-09-07 06:03:39'),
(473, 'https://reestr.agro.uz/admin/voyager-assets?path=js%2Fapp.js', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:39', '2021-09-07 06:03:39'),
(474, 'https://reestr.agro.uz/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:39', '2021-09-07 06:03:39'),
(475, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:03:40', '2021-09-07 06:03:40'),
(476, 'https://reestr.agro.uz/admin/menus/3/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:03:44', '2021-09-07 06:03:44'),
(477, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:45', '2021-09-07 06:03:45'),
(478, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:03:48', '2021-09-07 06:03:48'),
(479, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:03:57', '2021-09-07 06:03:57'),
(480, 'https://reestr.agro.uz/admin/menus/3/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:04:03', '2021-09-07 06:04:03'),
(481, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:04:05', '2021-09-07 06:04:05'),
(482, 'https://reestr.agro.uz/admin/menus/2/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:04:10', '2021-09-07 06:04:10'),
(483, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:04:12', '2021-09-07 06:04:12'),
(484, 'https://reestr.agro.uz/admin/menus/2/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:04:13', '2021-09-07 06:04:13'),
(485, 'https://reestr.agro.uz/admin/menus/2/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:04:22', '2021-09-07 06:04:22'),
(486, 'https://reestr.agro.uz/admin/menus/2/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:04:23', '2021-09-07 06:04:23'),
(487, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:04:29', '2021-09-07 06:04:29'),
(488, 'http://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:05:12', '2021-09-07 06:05:12'),
(489, 'http://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:05:12', '2021-09-07 06:05:12'),
(490, 'http://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:05:13', '2021-09-07 06:05:13'),
(491, 'https://agroreestr.teampro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:05:18', '2021-09-07 06:05:18'),
(492, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:05:39', '2021-09-07 06:05:39'),
(493, 'https://reestr.agro.uz/admin/menus/3/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:05:46', '2021-09-07 06:05:46'),
(494, 'https://reestr.agro.uz/admin/menus/3/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:06:04', '2021-09-07 06:06:04'),
(495, 'https://reestr.agro.uz/admin/menus/3/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:06:05', '2021-09-07 06:06:05'),
(496, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:06:09', '2021-09-07 06:06:09'),
(497, 'https://reestr.agro.uz/admin/menus/create', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:06:13', '2021-09-07 06:06:13'),
(498, 'https://reestr.agro.uz/admin/menus', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:06:19', '2021-09-07 06:06:19'),
(499, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:06:20', '2021-09-07 06:06:20'),
(500, 'https://reestr.agro.uz/admin/menus/3/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:06:26', '2021-09-07 06:06:26'),
(501, 'https://reestr.agro.uz/admin/menus/3/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:06:26', '2021-09-07 06:06:26'),
(502, 'https://reestr.agro.uz/admin/menus/3/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:06:55', '2021-09-07 06:06:55'),
(503, 'https://reestr.agro.uz/admin/menus/3/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:06:55', '2021-09-07 06:06:55'),
(504, 'https://reestr.agro.uz/admin/menus/3/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:07:09', '2021-09-07 06:07:09'),
(505, 'https://reestr.agro.uz/admin/menus/3/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:07:09', '2021-09-07 06:07:09'),
(506, 'https://reestr.agro.uz/admin/menus/3/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:07:27', '2021-09-07 06:07:27'),
(507, 'https://reestr.agro.uz/admin/menus/3/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:07:27', '2021-09-07 06:07:27'),
(508, 'https://reestr.agro.uz/admin/menus/3/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:07:41', '2021-09-07 06:07:41'),
(509, 'https://reestr.agro.uz/admin/menus/3/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:07:41', '2021-09-07 06:07:41'),
(510, 'https://reestr.agro.uz/admin/menus/3/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:07:52', '2021-09-07 06:07:52'),
(511, 'https://reestr.agro.uz/admin/menus/3/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:07:52', '2021-09-07 06:07:52'),
(512, 'https://reestr.agro.uz/admin/menus/4/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:08:02', '2021-09-07 06:08:02'),
(513, 'https://reestr.agro.uz/admin/menus/4/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:08:05', '2021-09-07 06:08:05'),
(514, 'https://reestr.agro.uz/admin/menus/4/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:08:19', '2021-09-07 06:08:19'),
(515, 'https://reestr.agro.uz/admin/menus/4/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:08:19', '2021-09-07 06:08:19'),
(516, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:08:22', '2021-09-07 06:08:22'),
(517, 'https://reestr.agro.uz/admin/menus/4/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:08:31', '2021-09-07 06:08:31'),
(518, 'https://reestr.agro.uz/admin/menus/4/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:08:53', '2021-09-07 06:08:53'),
(519, 'https://reestr.agro.uz/admin/menus/4/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:08:53', '2021-09-07 06:08:53'),
(520, 'https://agroreestr.teampro.uz', 'GET', '5.255.231.123', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.268', NULL, '2021-09-07 06:09:01', '2021-09-07 06:09:01'),
(521, 'https://reestr.agro.uz/admin/menus/4/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:09:15', '2021-09-07 06:09:15'),
(522, 'https://reestr.agro.uz/admin/menus/4/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:09:15', '2021-09-07 06:09:15'),
(523, 'https://reestr.agro.uz/admin/menus/4/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:09:37', '2021-09-07 06:09:37'),
(524, 'https://reestr.agro.uz/admin/menus/4/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:09:38', '2021-09-07 06:09:38'),
(525, 'https://reestr.agro.uz/admin/menus/4/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:09:59', '2021-09-07 06:09:59'),
(526, 'https://reestr.agro.uz/admin/menus/4/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:10:00', '2021-09-07 06:10:00'),
(527, 'https://reestr.agro.uz/admin/menus/4/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:10:24', '2021-09-07 06:10:24'),
(528, 'https://reestr.agro.uz/admin/menus/4/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:10:25', '2021-09-07 06:10:25'),
(529, 'https://reestr.agro.uz/admin/menus/4/item', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:10:43', '2021-09-07 06:10:43'),
(530, 'https://reestr.agro.uz/admin/menus/4/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:10:44', '2021-09-07 06:10:44'),
(531, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:10:54', '2021-09-07 06:10:54'),
(532, 'https://reestr.agro.uz/admin/menus/4/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:10:56', '2021-09-07 06:10:56'),
(533, 'https://reestr.agro.uz/admin/menus/4/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:11:01', '2021-09-07 06:11:01'),
(534, 'https://reestr.agro.uz/admin/menus/4/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:11:01', '2021-09-07 06:11:01'),
(535, 'https://reestr.agro.uz/admin/menus/4/order', 'POST', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:11:04', '2021-09-07 06:11:04'),
(536, 'https://reestr.agro.uz/admin/database', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:17:27', '2021-09-07 06:17:27'),
(537, 'https://reestr.agro.uz/admin/database/posts', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:17:38', '2021-09-07 06:17:38'),
(538, 'https://reestr.agro.uz/admin/posts', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:17:43', '2021-09-07 06:17:43'),
(539, 'https://reestr.agro.uz/admin/database/posts/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:17:56', '2021-09-07 06:17:56'),
(540, 'https://reestr.agro.uz/admin/database/posts/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:18:21', '2021-09-07 06:18:21'),
(541, 'https://reestr.agro.uz/admin/bread', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:18:34', '2021-09-07 06:18:34'),
(542, 'https://reestr.agro.uz/admin/bread/users/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:18:51', '2021-09-07 06:18:51'),
(543, 'https://reestr.agro.uz/admin/database/activities', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:19:05', '2021-09-07 06:19:05'),
(544, 'https://reestr.agro.uz/admin/bread/posts/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:19:51', '2021-09-07 06:19:51'),
(545, 'https://reestr.agro.uz/admin/database/activities', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:19:58', '2021-09-07 06:19:58'),
(546, 'https://reestr.agro.uz/admin/database/users', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:20:15', '2021-09-07 06:20:15'),
(547, 'https://reestr.agro.uz/admin/menus/3/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:29:14', '2021-09-07 06:29:14'),
(548, 'https://reestr.agro.uz/admin/menus/3', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:29:20', '2021-09-07 06:29:20'),
(549, 'https://reestr.agro.uz/admin/voyager-assets?path=fonts%2Fbootstrap%2Fglyphicons-halflings-regular.woff2', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:29:21', '2021-09-07 06:29:21'),
(550, 'https://reestr.agro.uz/admin/menus', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:29:23', '2021-09-07 06:29:23'),
(551, 'https://reestr.agro.uz/admin/menus/4/edit', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:29:28', '2021-09-07 06:29:28'),
(552, 'https://reestr.agro.uz/admin/menus/4/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:29:32', '2021-09-07 06:29:32'),
(553, 'https://reestr.agro.uz/admin/menus/3/builder', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-07 06:29:39', '2021-09-07 06:29:39'),
(554, 'http://reestr.agro.uz', 'GET', '195.158.16.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 06:48:32', '2021-09-07 06:48:32'),
(555, 'http://reestr.agro.uz', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-07 10:44:48', '2021-09-07 10:44:48'),
(556, 'https://reestr.agro.uz', 'GET', '161.35.136.233', 'Mozilla/5.0 (compatible; NetcraftSurveyAgent/1.0; +info@netcraft.com)', NULL, '2021-09-07 12:52:51', '2021-09-07 12:52:51'),
(557, 'http://reestr.agro.uz', 'GET', '66.249.66.157', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.119 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', NULL, '2021-09-07 13:44:45', '2021-09-07 13:44:45'),
(558, 'https://agroreestr.teampro.uz', 'GET', '167.71.81.32', 'Mozilla/5.0 (compatible; NetcraftSurveyAgent/1.0; +info@netcraft.com)', NULL, '2021-09-07 14:07:09', '2021-09-07 14:07:09'),
(559, 'http://reestr.agro.uz/content/2', 'GET', '5.255.253.157', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', NULL, '2021-09-07 16:18:36', '2021-09-07 16:18:36'),
(560, 'http://reestr.agro.uz/application', 'GET', '66.249.66.156', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.119 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', NULL, '2021-09-07 23:38:12', '2021-09-07 23:38:12'),
(561, 'https://reestr.agro.uz/admin/categories', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-08 00:12:07', '2021-09-08 00:12:07'),
(562, 'https://reestr.agro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-08 00:12:08', '2021-09-08 00:12:08'),
(563, 'http://reestr.agro.uz', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-08 04:23:27', '2021-09-08 04:23:27'),
(564, 'https://reestr.agro.uz', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-08 04:23:45', '2021-09-08 04:23:45'),
(565, 'http://reestr.agro.uz', 'GET', '195.158.30.115', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-08 04:30:30', '2021-09-08 04:30:30'),
(566, 'https://reestr.agro.uz/admin/login', 'GET', '84.54.77.73', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-08 06:23:01', '2021-09-08 06:23:01'),
(567, 'https://reestr.agro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '84.54.77.73', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-08 06:23:02', '2021-09-08 06:23:02'),
(568, 'https://agroreestr.teampro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 01:55:22', '2021-09-09 01:55:22'),
(569, 'https://agroreestr.teampro.uz/admin', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 01:55:30', '2021-09-09 01:55:30'),
(570, 'https://agroreestr.teampro.uz/admin/login', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 01:55:30', '2021-09-09 01:55:30'),
(571, 'https://agroreestr.teampro.uz/admin/voyager-assets?path=css%2Fapp.css', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 01:55:31', '2021-09-09 01:55:31'),
(572, 'https://reestr.agro.uz', 'GET', '213.230.68.218', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 02:17:24', '2021-09-09 02:17:24'),
(573, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Symfony', NULL, '2021-09-09 02:45:14', '2021-09-09 02:45:14'),
(574, 'http://localhost/agro.test', 'GET', '127.0.0.1', 'Symfony', NULL, '2021-09-09 03:01:54', '2021-09-09 03:01:54'),
(575, 'http://agro.test', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:01:58', '2021-09-09 03:01:58'),
(576, 'http://agro.test/assets/icons/address.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:01:58', '2021-09-09 03:01:58'),
(577, 'http://agro.test/img/icons/search.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:01:58', '2021-09-09 03:01:58'),
(578, 'http://agro.test/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:26', '2021-09-09 03:08:26'),
(579, 'http://agro.test/admin/login', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:26', '2021-09-09 03:08:26'),
(580, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:28', '2021-09-09 03:08:28'),
(581, 'http://agro.test/css/custom.css', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:28', '2021-09-09 03:08:28'),
(582, 'http://agro.test/admin/voyager-assets?path=css%2Fapp.css', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:28', '2021-09-09 03:08:28'),
(583, 'http://agro.test/js/auth.js', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:28', '2021-09-09 03:08:28'),
(584, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:28', '2021-09-09 03:08:28'),
(585, 'http://agro.test/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:37', '2021-09-09 03:08:37'),
(586, 'http://agro.test/admin/login', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:37', '2021-09-09 03:08:37'),
(587, 'http://agro.test/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:38', '2021-09-09 03:08:38'),
(588, 'http://agro.test/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:39', '2021-09-09 03:08:39'),
(589, 'http://agro.test/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:39', '2021-09-09 03:08:39'),
(590, 'http://agro.test/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:39', '2021-09-09 03:08:39'),
(591, 'http://agro.test/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:39', '2021-09-09 03:08:39'),
(592, 'http://agro.test/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:39', '2021-09-09 03:08:39'),
(593, 'http://agro.test/admin/voyager-assets?path=js%2Fapp.js', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:39', '2021-09-09 03:08:39'),
(594, 'http://agro.test/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:40', '2021-09-09 03:08:40'),
(595, 'http://agro.test/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:49', '2021-09-09 03:08:49'),
(596, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:50', '2021-09-09 03:08:50'),
(597, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:50', '2021-09-09 03:08:50'),
(598, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:50', '2021-09-09 03:08:50'),
(599, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:50', '2021-09-09 03:08:50'),
(600, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 03:08:50', '2021-09-09 03:08:50'),
(601, 'http://localhost', 'GET', '127.0.0.1', 'Symfony', NULL, '2021-09-09 05:13:12', '2021-09-09 05:13:12'),
(602, 'http://agro.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:15:42', '2021-09-09 05:15:42'),
(603, 'http://agro.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:15:56', '2021-09-09 05:15:56'),
(604, 'http://agro.loc/admin/login', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:15:57', '2021-09-09 05:15:57'),
(605, 'http://agro.loc/admin/voyager-assets?path=css%2Fapp.css', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:15:57', '2021-09-09 05:15:57'),
(606, 'http://agro.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:15:58', '2021-09-09 05:15:58'),
(607, 'http://agro.loc/admin/login', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:15:58', '2021-09-09 05:15:58'),
(608, 'http://agro.loc/admin/login', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:16:18', '2021-09-09 05:16:18'),
(609, 'http://agro.loc/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:16:18', '2021-09-09 05:16:18'),
(610, 'http://agro.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:16:18', '2021-09-09 05:16:18'),
(611, 'http://agro.loc/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:16:19', '2021-09-09 05:16:19'),
(612, 'http://agro.loc/admin/voyager-assets?path=js%2Fapp.js', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:16:19', '2021-09-09 05:16:19'),
(613, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:16:29', '2021-09-09 05:16:29');
INSERT INTO `activities` (`id`, `url`, `method`, `ip`, `agent`, `user_id`, `created_at`, `updated_at`) VALUES
(614, 'http://agro.loc/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:16:33', '2021-09-09 05:16:33'),
(615, 'http://agro.loc/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:22:05', '2021-09-09 05:22:05'),
(616, 'http://agro.loc/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:22:11', '2021-09-09 05:22:11'),
(617, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:32:23', '2021-09-09 03:32:23'),
(618, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:32:30', '2021-09-09 03:32:30'),
(619, 'http://agroregistry.loc/admin/login', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:32:31', '2021-09-09 03:32:31'),
(620, 'http://agroregistry.loc/css/custom.css', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:32:32', '2021-09-09 03:32:32'),
(621, 'http://agroregistry.loc/admin/voyager-assets?path=css%2Fapp.css', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:32:33', '2021-09-09 03:32:33'),
(622, 'http://agroregistry.loc/js/auth.js', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:32:33', '2021-09-09 03:32:33'),
(623, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:32:33', '2021-09-09 03:32:33'),
(624, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:32:34', '2021-09-09 03:32:34'),
(625, 'http://agroregistry.loc/js/auth.js', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:32:35', '2021-09-09 03:32:35'),
(626, 'http://agroregistry.loc/admin/login', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:34:38', '2021-09-09 03:34:38'),
(627, 'http://agroregistry.loc/admin/voyager-assets?path=fonts%2Fvoyager.woff', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:34:38', '2021-09-09 03:34:38'),
(628, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:34:40', '2021-09-09 03:34:40'),
(629, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:34:44', '2021-09-09 03:34:44'),
(630, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:34:44', '2021-09-09 03:34:44'),
(631, 'http://agroregistry.loc/admin/voyager-assets?path=images%2Fwidget-backgrounds%2F02.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:34:45', '2021-09-09 03:34:45'),
(632, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:34:45', '2021-09-09 03:34:45'),
(633, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:34:45', '2021-09-09 03:34:45'),
(634, 'http://agroregistry.loc/admin/voyager-assets?path=js%2Fapp.js', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:34:45', '2021-09-09 03:34:45'),
(635, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:34:48', '2021-09-09 03:34:48'),
(636, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:36:04', '2021-09-09 03:36:04'),
(637, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:36:06', '2021-09-09 03:36:06'),
(638, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:36:07', '2021-09-09 03:36:07'),
(639, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:36:07', '2021-09-09 03:36:07'),
(640, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:36:08', '2021-09-09 03:36:08'),
(641, 'http://agroregistry.loc/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:36:10', '2021-09-09 03:36:10'),
(642, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:36:12', '2021-09-09 03:36:12'),
(643, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:36:13', '2021-09-09 03:36:13'),
(644, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:36:13', '2021-09-09 03:36:13'),
(645, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:36:13', '2021-09-09 03:36:13'),
(646, 'http://agro.loc/admin/database', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:45:03', '2021-09-09 05:45:03'),
(647, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:45:04', '2021-09-09 05:45:04'),
(648, 'http://agroregistry.loc/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:43:56', '2021-09-09 03:43:56'),
(649, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:43:59', '2021-09-09 03:43:59'),
(650, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:43:59', '2021-09-09 03:43:59'),
(651, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:44:00', '2021-09-09 03:44:00'),
(652, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 03:44:00', '2021-09-09 03:44:00'),
(653, 'http://agro.loc/admin/database/applying_for_testings/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:46:06', '2021-09-09 05:46:06'),
(654, 'http://agro.loc/admin/vehicles', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:47:46', '2021-09-09 05:47:46'),
(655, 'http://agro.loc/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:47:47', '2021-09-09 05:47:47'),
(656, 'http://agro.loc/admin/contracts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:48:03', '2021-09-09 05:48:03'),
(657, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:48:31', '2021-09-09 05:48:31'),
(658, 'http://agro.loc/admin/database/applying_for_testings', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:48:36', '2021-09-09 05:48:36'),
(659, 'http://agro.loc/admin/database/vehicles', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:49:24', '2021-09-09 05:49:24'),
(660, 'http://agro.loc/admin/database/vehicles', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:49:38', '2021-09-09 05:49:38'),
(661, 'http://agro.loc/admin/database/vehicles/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 05:49:42', '2021-09-09 05:49:42'),
(662, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 06:35:37', '2021-09-09 06:35:37'),
(663, 'http://agroregistry.loc/admin/database', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:09', '2021-09-09 04:37:09'),
(664, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:11', '2021-09-09 04:37:11'),
(665, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:13', '2021-09-09 04:37:13'),
(666, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:13', '2021-09-09 04:37:13'),
(667, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:13', '2021-09-09 04:37:13'),
(668, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:14', '2021-09-09 04:37:14'),
(669, 'http://agroregistry.loc/admin/bread/parametres/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:29', '2021-09-09 04:37:29'),
(670, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:30', '2021-09-09 04:37:30'),
(671, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:31', '2021-09-09 04:37:31'),
(672, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:31', '2021-09-09 04:37:31'),
(673, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:31', '2021-09-09 04:37:31'),
(674, 'http://agroregistry.loc/admin/database/parametres/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:47', '2021-09-09 04:37:47'),
(675, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:48', '2021-09-09 04:37:48'),
(676, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:49', '2021-09-09 04:37:49'),
(677, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:49', '2021-09-09 04:37:49'),
(678, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:37:49', '2021-09-09 04:37:49'),
(679, 'http://agro.test/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:38:01', '2021-09-09 04:38:01'),
(680, 'http://agro.test/admin/database/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:38:02', '2021-09-09 04:38:02'),
(681, 'http://agro.test/admin/database/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:38:02', '2021-09-09 04:38:02'),
(682, 'http://agro.test/admin/database/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:38:02', '2021-09-09 04:38:02'),
(683, 'http://agro.test/admin/database/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:38:02', '2021-09-09 04:38:02'),
(684, 'http://agroregistry.loc/admin/database/parametres', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:38:08', '2021-09-09 04:38:08'),
(685, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:38:09', '2021-09-09 04:38:09'),
(686, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:38:10', '2021-09-09 04:38:10'),
(687, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:38:11', '2021-09-09 04:38:11'),
(688, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:38:11', '2021-09-09 04:38:11'),
(689, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:38:12', '2021-09-09 04:38:12'),
(690, 'http://agroregistry.loc/admin/bread/parameters/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:38:50', '2021-09-09 04:38:50'),
(691, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:38:52', '2021-09-09 04:38:52'),
(692, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:38:52', '2021-09-09 04:38:52'),
(693, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:38:52', '2021-09-09 04:38:52'),
(694, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:38:53', '2021-09-09 04:38:53'),
(695, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 06:41:36', '2021-09-09 06:41:36'),
(696, 'http://agro.loc/admin/users', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 06:41:52', '2021-09-09 06:41:52'),
(697, 'http://agro.loc/admin/bread/vehicles/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 06:42:02', '2021-09-09 06:42:02'),
(698, 'http://agro.loc/admin/database/vehicles', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 06:42:57', '2021-09-09 06:42:57'),
(699, 'http://agro.loc/admin/database/applying_for_testings/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 06:43:15', '2021-09-09 06:43:15'),
(700, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:43:54', '2021-09-09 04:43:54'),
(701, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:47:29', '2021-09-09 04:47:29'),
(702, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:47:30', '2021-09-09 04:47:30'),
(703, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:47:30', '2021-09-09 04:47:30'),
(704, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:47:30', '2021-09-09 04:47:30'),
(705, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 04:47:32', '2021-09-09 04:47:32'),
(706, 'http://agro.test/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:54:55', '2021-09-09 04:54:55'),
(707, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:54:56', '2021-09-09 04:54:56'),
(708, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:54:56', '2021-09-09 04:54:56'),
(709, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:54:56', '2021-09-09 04:54:56'),
(710, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:54:56', '2021-09-09 04:54:56'),
(711, 'http://agro.test/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:56:31', '2021-09-09 04:56:31'),
(712, 'http://agro.test/admin/database/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:56:32', '2021-09-09 04:56:32'),
(713, 'http://agro.test/admin/database/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:56:32', '2021-09-09 04:56:32'),
(714, 'http://agro.test/admin/database/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:56:32', '2021-09-09 04:56:32'),
(715, 'http://agro.test/admin/database/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:56:32', '2021-09-09 04:56:32'),
(716, 'http://agro.test/admin/database', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:57:26', '2021-09-09 04:57:26'),
(717, 'http://agro.test/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:57:26', '2021-09-09 04:57:26'),
(718, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:57:27', '2021-09-09 04:57:27'),
(719, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:57:27', '2021-09-09 04:57:27'),
(720, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:57:27', '2021-09-09 04:57:27'),
(721, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:57:27', '2021-09-09 04:57:27'),
(722, 'http://agro.test/admin/database/tnved_datas', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:57:32', '2021-09-09 04:57:32'),
(723, 'http://agro.loc/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 06:59:24', '2021-09-09 06:59:24'),
(724, 'http://agro.test/admin/bread/tnved_datas/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:14', '2021-09-09 04:58:14'),
(725, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:15', '2021-09-09 04:58:15'),
(726, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:15', '2021-09-09 04:58:15'),
(727, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:15', '2021-09-09 04:58:15'),
(728, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:15', '2021-09-09 04:58:15'),
(729, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:16', '2021-09-09 04:58:16'),
(730, 'http://agro.test/admin/bread', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:29', '2021-09-09 04:58:29'),
(731, 'http://agro.test/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:30', '2021-09-09 04:58:30'),
(732, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:31', '2021-09-09 04:58:31'),
(733, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:31', '2021-09-09 04:58:31'),
(734, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:31', '2021-09-09 04:58:31'),
(735, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:31', '2021-09-09 04:58:31'),
(736, 'http://agro.test/admin/tnved-datas', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:36', '2021-09-09 04:58:36'),
(737, 'http://agro.test/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:37', '2021-09-09 04:58:37'),
(738, 'http://agro.test/_ignition/execute-solution', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:49', '2021-09-09 04:58:49'),
(739, 'http://agro.test/admin/tnved-datas', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:55', '2021-09-09 04:58:55'),
(740, 'http://agro.test/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:58:55', '2021-09-09 04:58:55'),
(741, 'http://agro.loc/admin/database', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:00:33', '2021-09-09 07:00:33'),
(742, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:00:33', '2021-09-09 07:00:33'),
(743, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:59:18', '2021-09-09 04:59:18'),
(744, 'http://agro.loc/admin/database/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:00:52', '2021-09-09 07:00:52'),
(745, 'http://agro.loc/admin/bread/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:01:02', '2021-09-09 07:01:02'),
(746, 'http://agro.test/admin/tnved-datas', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:59:37', '2021-09-09 04:59:37'),
(747, 'http://agro.test/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 04:59:38', '2021-09-09 04:59:38'),
(748, 'http://agro.loc/admin/bread', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:01:21', '2021-09-09 07:01:21'),
(749, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:01:22', '2021-09-09 07:01:22'),
(750, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:01:26', '2021-09-09 07:01:26'),
(751, 'http://agro.loc/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:01:27', '2021-09-09 07:01:27'),
(752, 'http://agro.test/admin/bread/tnved_datas/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:00:17', '2021-09-09 05:00:17'),
(753, 'http://agro.loc/admin/bread/gosts/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:01:49', '2021-09-09 07:01:49'),
(754, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:00:18', '2021-09-09 05:00:18'),
(755, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:00:18', '2021-09-09 05:00:18'),
(756, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:00:18', '2021-09-09 05:00:18'),
(757, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:00:18', '2021-09-09 05:00:18'),
(758, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:01:58', '2021-09-09 07:01:58'),
(759, 'http://agro.loc/admin/database/gosts/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:02:02', '2021-09-09 07:02:02'),
(760, 'http://agro.loc/admin/database/gosts', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:02:05', '2021-09-09 07:02:05'),
(761, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:02:05', '2021-09-09 07:02:05'),
(762, 'http://agro.loc/admin/bread/gosts/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:02:12', '2021-09-09 07:02:12'),
(763, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:02:25', '2021-09-09 07:02:25'),
(764, 'http://agro.test/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:00:54', '2021-09-09 05:00:54'),
(765, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:00:55', '2021-09-09 05:00:55'),
(766, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:00:55', '2021-09-09 05:00:55'),
(767, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:00:55', '2021-09-09 05:00:55'),
(768, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:00:55', '2021-09-09 05:00:55'),
(769, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:02:31', '2021-09-09 07:02:31'),
(770, 'http://agro.test/admin/bread/tnved_datas/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:05', '2021-09-09 05:01:05'),
(771, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:06', '2021-09-09 05:01:06'),
(772, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:06', '2021-09-09 05:01:06'),
(773, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:06', '2021-09-09 05:01:06'),
(774, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:06', '2021-09-09 05:01:06'),
(775, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:06', '2021-09-09 05:01:06'),
(776, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:06', '2021-09-09 05:01:06'),
(777, 'http://agro.test/admin/database/tnved_datas/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:08', '2021-09-09 05:01:08'),
(778, 'http://agro.test/admin/database/tnved_datas/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:09', '2021-09-09 05:01:09'),
(779, 'http://agro.test/admin/database/tnved_datas/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:09', '2021-09-09 05:01:09'),
(780, 'http://agro.test/admin/database/tnved_datas/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:09', '2021-09-09 05:01:09'),
(781, 'http://agro.test/admin/database/tnved_datas/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:09', '2021-09-09 05:01:09'),
(782, 'http://agro.test/admin/database/tnved_datas/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:10', '2021-09-09 05:01:10'),
(783, 'http://agro.test/admin/database/tnved_datas', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:15', '2021-09-09 05:01:15'),
(784, 'http://agro.test/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:16', '2021-09-09 05:01:16'),
(785, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:16', '2021-09-09 05:01:16'),
(786, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:16', '2021-09-09 05:01:16'),
(787, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:16', '2021-09-09 05:01:16'),
(788, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:16', '2021-09-09 05:01:16'),
(789, 'http://agro.test/admin/bread/tnved_data/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:34', '2021-09-09 05:01:34'),
(790, 'http://agro.test/admin/bread/tnved_data/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:35', '2021-09-09 05:01:35'),
(791, 'http://agro.test/admin/bread/tnved_data/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:35', '2021-09-09 05:01:35'),
(792, 'http://agro.test/admin/bread/tnved_data/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:35', '2021-09-09 05:01:35'),
(793, 'http://agro.test/admin/bread/tnved_data/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:35', '2021-09-09 05:01:35'),
(794, 'http://agro.test/admin/bread', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:01:40', '2021-09-09 05:01:40'),
(795, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:03:33', '2021-09-09 07:03:33'),
(796, 'http://agro.loc/admin/bread/gosts/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:03:37', '2021-09-09 07:03:37'),
(797, 'http://agro.test/admin/bread/tnved_data/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:06', '2021-09-09 05:02:06'),
(798, 'http://agro.test/admin/bread/tnved_data/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:09', '2021-09-09 05:02:09'),
(799, 'http://agro.test/admin/bread/tnved_data/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:09', '2021-09-09 05:02:09'),
(800, 'http://agro.test/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:14', '2021-09-09 05:02:14');
INSERT INTO `activities` (`id`, `url`, `method`, `ip`, `agent`, `user_id`, `created_at`, `updated_at`) VALUES
(801, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:14', '2021-09-09 05:02:14'),
(802, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:14', '2021-09-09 05:02:14'),
(803, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:14', '2021-09-09 05:02:14'),
(804, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:14', '2021-09-09 05:02:14'),
(805, 'http://agro.test/admin/database/tnved_data', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:21', '2021-09-09 05:02:21'),
(806, 'http://agro.test/admin/database/tnved_data/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:24', '2021-09-09 05:02:24'),
(807, 'http://agro.test/admin/database/tnved_data/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:24', '2021-09-09 05:02:24'),
(808, 'http://agro.test/admin/database/tnved_data/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:24', '2021-09-09 05:02:24'),
(809, 'http://agro.test/admin/database/tnved_data/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:24', '2021-09-09 05:02:24'),
(810, 'http://agro.test/admin/database/tnved_data/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:25', '2021-09-09 05:02:25'),
(811, 'http://agro.test/admin/database/tnved_data/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:25', '2021-09-09 05:02:25'),
(812, 'http://agro.test/admin/database/tnved_data', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:28', '2021-09-09 05:02:28'),
(813, 'http://agro.test/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:29', '2021-09-09 05:02:29'),
(814, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:29', '2021-09-09 05:02:29'),
(815, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:29', '2021-09-09 05:02:29'),
(816, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:29', '2021-09-09 05:02:29'),
(817, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:29', '2021-09-09 05:02:29'),
(818, 'http://agro.loc/admin/bread/24', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:10', '2021-09-09 07:04:10'),
(819, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:10', '2021-09-09 07:04:10'),
(820, 'http://agro.test/admin/bread/tnved_datas/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:42', '2021-09-09 05:02:42'),
(821, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:15', '2021-09-09 07:04:15'),
(822, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:43', '2021-09-09 05:02:43'),
(823, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:43', '2021-09-09 05:02:43'),
(824, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:43', '2021-09-09 05:02:43'),
(825, 'http://agro.test/admin/bread/tnved_datas/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:02:43', '2021-09-09 05:02:43'),
(826, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:17', '2021-09-09 07:04:17'),
(827, 'http://agro.loc/admin/voyager-assets?path=js%2Fskins%2Fvoyager%2Fskin.min.css', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:17', '2021-09-09 07:04:17'),
(828, 'http://agro.loc/admin/voyager-assets?path=js%2Fskins%2Fvoyager%2Fcontent.min.css', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:17', '2021-09-09 07:04:17'),
(829, 'http://agro.loc/admin/voyager-assets?path=js%2Fskins%2Fvoyager%2Ffonts%2Ftinymce.woff', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:17', '2021-09-09 07:04:17'),
(830, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:26', '2021-09-09 07:04:26'),
(831, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:27', '2021-09-09 07:04:27'),
(832, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:30', '2021-09-09 07:04:30'),
(833, 'http://agro.test/admin/bread/23', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:04', '2021-09-09 05:03:04'),
(834, 'http://agro.test/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:04', '2021-09-09 05:03:04'),
(835, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:05', '2021-09-09 05:03:05'),
(836, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:05', '2021-09-09 05:03:05'),
(837, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:05', '2021-09-09 05:03:05'),
(838, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:05', '2021-09-09 05:03:05'),
(839, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:38', '2021-09-09 07:04:38'),
(840, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:38', '2021-09-09 07:04:38'),
(841, 'http://agro.test/admin/tnved-datas', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:07', '2021-09-09 05:03:07'),
(842, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:40', '2021-09-09 07:04:40'),
(843, 'http://agro.test/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:08', '2021-09-09 05:03:08'),
(844, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:51', '2021-09-09 07:04:51'),
(845, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:51', '2021-09-09 07:04:51'),
(846, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:04:54', '2021-09-09 07:04:54'),
(847, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:02', '2021-09-09 07:05:02'),
(848, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:02', '2021-09-09 07:05:02'),
(849, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:05', '2021-09-09 07:05:05'),
(850, 'http://agro.test/admin/database/tnved_datas', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:33', '2021-09-09 05:03:33'),
(851, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:12', '2021-09-09 07:05:12'),
(852, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:12', '2021-09-09 07:05:12'),
(853, 'http://agro.test/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:41', '2021-09-09 05:03:41'),
(854, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:42', '2021-09-09 05:03:42'),
(855, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:42', '2021-09-09 05:03:42'),
(856, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:42', '2021-09-09 05:03:42'),
(857, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:42', '2021-09-09 05:03:42'),
(858, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:15', '2021-09-09 07:05:15'),
(859, 'http://agro.test/admin/database/tnved_datas/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:48', '2021-09-09 05:03:48'),
(860, 'http://agro.test/admin/database/tnved_datas/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:49', '2021-09-09 05:03:49'),
(861, 'http://agro.test/admin/database/tnved_datas/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:49', '2021-09-09 05:03:49'),
(862, 'http://agro.test/admin/database/tnved_datas/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:49', '2021-09-09 05:03:49'),
(863, 'http://agro.test/admin/database/tnved_datas/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:03:49', '2021-09-09 05:03:49'),
(864, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:23', '2021-09-09 07:05:23'),
(865, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:23', '2021-09-09 07:05:23'),
(866, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:25', '2021-09-09 07:05:25'),
(867, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:33', '2021-09-09 07:05:33'),
(868, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:33', '2021-09-09 07:05:33'),
(869, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:35', '2021-09-09 07:05:35'),
(870, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:43', '2021-09-09 07:05:43'),
(871, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:43', '2021-09-09 07:05:43'),
(872, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:44', '2021-09-09 07:05:44'),
(873, 'http://agro.test/admin/database/tnved_datas', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:14', '2021-09-09 05:04:14'),
(874, 'http://agro.test/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:14', '2021-09-09 05:04:14'),
(875, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:15', '2021-09-09 05:04:15'),
(876, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:15', '2021-09-09 05:04:15'),
(877, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:15', '2021-09-09 05:04:15'),
(878, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:15', '2021-09-09 05:04:15'),
(879, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:52', '2021-09-09 07:05:52'),
(880, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:05:52', '2021-09-09 07:05:52'),
(881, 'http://agro.test/admin/tnved-datas', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:25', '2021-09-09 05:04:25'),
(882, 'http://agro.test/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:26', '2021-09-09 05:04:26'),
(883, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:27', '2021-09-09 05:04:27'),
(884, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:00', '2021-09-09 07:06:00'),
(885, 'http://agro.test/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:30', '2021-09-09 05:04:30'),
(886, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:03', '2021-09-09 07:06:03'),
(887, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:31', '2021-09-09 05:04:31'),
(888, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:03', '2021-09-09 07:06:03'),
(889, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:31', '2021-09-09 05:04:31'),
(890, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:31', '2021-09-09 05:04:31'),
(891, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:31', '2021-09-09 05:04:31'),
(892, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:31', '2021-09-09 05:04:31'),
(893, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:05', '2021-09-09 07:06:05'),
(894, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:34', '2021-09-09 05:04:34'),
(895, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:34', '2021-09-09 05:04:34'),
(896, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:34', '2021-09-09 05:04:34'),
(897, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:34', '2021-09-09 05:04:34'),
(898, 'http://agro.test/admin/bread/23', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:38', '2021-09-09 05:04:38'),
(899, 'http://agro.test/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:38', '2021-09-09 05:04:38'),
(900, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:39', '2021-09-09 05:04:39'),
(901, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:39', '2021-09-09 05:04:39'),
(902, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:39', '2021-09-09 05:04:39'),
(903, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:39', '2021-09-09 05:04:39'),
(904, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:14', '2021-09-09 07:06:14'),
(905, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:14', '2021-09-09 07:06:14'),
(906, 'http://agro.test/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:44', '2021-09-09 05:04:44'),
(907, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:45', '2021-09-09 05:04:45'),
(908, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:45', '2021-09-09 05:04:45'),
(909, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:45', '2021-09-09 05:04:45'),
(910, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:45', '2021-09-09 05:04:45'),
(911, 'http://agroregistry.loc/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:47', '2021-09-09 05:04:47'),
(912, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:50', '2021-09-09 05:04:50'),
(913, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:50', '2021-09-09 05:04:50'),
(914, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:50', '2021-09-09 05:04:50'),
(915, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:50', '2021-09-09 05:04:50'),
(916, 'http://agro.test/admin/database/tnved_datas', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:52', '2021-09-09 05:04:52'),
(917, 'http://agro.test/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:52', '2021-09-09 05:04:52'),
(918, 'http://agro.test/admin/agro.test/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:53', '2021-09-09 05:04:53'),
(919, 'http://agro.test/admin/agro.test/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:53', '2021-09-09 05:04:53'),
(920, 'http://agro.test/admin/agro.test/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:53', '2021-09-09 05:04:53'),
(921, 'http://agro.test/admin/agro.test/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 05:04:53', '2021-09-09 05:04:53'),
(922, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:53', '2021-09-09 05:04:53'),
(923, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:53', '2021-09-09 05:04:53'),
(924, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:53', '2021-09-09 05:04:53'),
(925, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:54', '2021-09-09 05:04:54'),
(926, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:28', '2021-09-09 07:06:28'),
(927, 'http://agroregistry.loc/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:56', '2021-09-09 05:04:56'),
(928, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:31', '2021-09-09 07:06:31'),
(929, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:59', '2021-09-09 05:04:59'),
(930, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:59', '2021-09-09 05:04:59'),
(931, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:32', '2021-09-09 07:06:32'),
(932, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:59', '2021-09-09 05:04:59'),
(933, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:04:59', '2021-09-09 05:04:59'),
(934, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:34', '2021-09-09 07:06:34'),
(935, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:49', '2021-09-09 07:06:49'),
(936, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:06:49', '2021-09-09 07:06:49'),
(937, 'http://agroregistry.loc/admin/database', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:19', '2021-09-09 05:05:19'),
(938, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:20', '2021-09-09 05:05:20'),
(939, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:22', '2021-09-09 05:05:22'),
(940, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:22', '2021-09-09 05:05:22'),
(941, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:23', '2021-09-09 05:05:23'),
(942, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:23', '2021-09-09 05:05:23'),
(943, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:00', '2021-09-09 07:07:00'),
(944, 'http://agroregistry.loc/admin/bread/tnved_datas/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:29', '2021-09-09 05:05:29'),
(945, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:31', '2021-09-09 05:05:31'),
(946, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:31', '2021-09-09 05:05:31'),
(947, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:31', '2021-09-09 05:05:31'),
(948, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:32', '2021-09-09 05:05:32'),
(949, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:08', '2021-09-09 07:07:08'),
(950, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:08', '2021-09-09 07:07:08'),
(951, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:10', '2021-09-09 07:07:10'),
(952, 'http://agroregistry.loc/admin/bread', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:38', '2021-09-09 05:05:38'),
(953, 'http://agroregistry.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:39', '2021-09-09 05:05:39'),
(954, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:41', '2021-09-09 05:05:41'),
(955, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:42', '2021-09-09 05:05:42'),
(956, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:42', '2021-09-09 05:05:42'),
(957, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:42', '2021-09-09 05:05:42'),
(958, 'http://agroregistry.loc/admin/tnved-datas', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:45', '2021-09-09 05:05:45'),
(959, 'http://agroregistry.loc/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:46', '2021-09-09 05:05:46'),
(960, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:52', '2021-09-09 05:05:52'),
(961, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:52', '2021-09-09 05:05:52'),
(962, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:53', '2021-09-09 05:05:53'),
(963, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:05:53', '2021-09-09 05:05:53'),
(964, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:02', '2021-09-09 05:06:02'),
(965, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:37', '2021-09-09 07:07:37'),
(966, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:04', '2021-09-09 05:06:04'),
(967, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:04', '2021-09-09 05:06:04'),
(968, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:04', '2021-09-09 05:06:04'),
(969, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:04', '2021-09-09 05:06:04'),
(970, 'http://agroregistry.loc/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:05', '2021-09-09 05:06:05'),
(971, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:39', '2021-09-09 07:07:39'),
(972, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:40', '2021-09-09 07:07:40'),
(973, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:08', '2021-09-09 05:06:08'),
(974, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:08', '2021-09-09 05:06:08'),
(975, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:08', '2021-09-09 05:06:08'),
(976, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:08', '2021-09-09 05:06:08'),
(977, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:41', '2021-09-09 07:07:41'),
(978, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:49', '2021-09-09 07:07:49'),
(979, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:50', '2021-09-09 07:07:50'),
(980, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:56', '2021-09-09 07:07:56'),
(981, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:59', '2021-09-09 07:07:59'),
(982, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:07:59', '2021-09-09 07:07:59'),
(983, 'http://agroregistry.loc/admin/database', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:39', '2021-09-09 05:06:39'),
(984, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:40', '2021-09-09 05:06:40'),
(985, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:43', '2021-09-09 05:06:43'),
(986, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:43', '2021-09-09 05:06:43'),
(987, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:43', '2021-09-09 05:06:43'),
(988, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:43', '2021-09-09 05:06:43'),
(989, 'http://agroregistry.loc/admin/bread/tnveds/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:47', '2021-09-09 05:06:47'),
(990, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:08:21', '2021-09-09 07:08:21');
INSERT INTO `activities` (`id`, `url`, `method`, `ip`, `agent`, `user_id`, `created_at`, `updated_at`) VALUES
(991, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:49', '2021-09-09 05:06:49'),
(992, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:50', '2021-09-09 05:06:50'),
(993, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:50', '2021-09-09 05:06:50'),
(994, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:50', '2021-09-09 05:06:50'),
(995, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:08:23', '2021-09-09 07:08:23'),
(996, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:08:24', '2021-09-09 07:08:24'),
(997, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:08:30', '2021-09-09 07:08:30'),
(998, 'http://agroregistry.loc/admin/bread', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:57', '2021-09-09 05:06:57'),
(999, 'http://agroregistry.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:06:58', '2021-09-09 05:06:58'),
(1000, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:08:33', '2021-09-09 07:08:33'),
(1001, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:08:33', '2021-09-09 07:08:33'),
(1002, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:01', '2021-09-09 05:07:01'),
(1003, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:01', '2021-09-09 05:07:01'),
(1004, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:01', '2021-09-09 05:07:01'),
(1005, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:01', '2021-09-09 05:07:01'),
(1006, 'http://agroregistry.loc/admin/tnveds', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:03', '2021-09-09 05:07:03'),
(1007, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:04', '2021-09-09 05:07:04'),
(1008, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:04', '2021-09-09 05:07:04'),
(1009, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:04', '2021-09-09 05:07:04'),
(1010, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:05', '2021-09-09 05:07:05'),
(1011, 'http://agroregistry.loc/admin/tnved-datas', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:18', '2021-09-09 05:07:18'),
(1012, 'http://agroregistry.loc/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:19', '2021-09-09 05:07:19'),
(1013, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:23', '2021-09-09 05:07:23'),
(1014, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:24', '2021-09-09 05:07:24'),
(1015, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:24', '2021-09-09 05:07:24'),
(1016, 'http://agroregistry.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:07:33', '2021-09-09 05:07:33'),
(1017, 'http://agroregistry.loc/admin/activities', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:10:51', '2021-09-09 05:10:51'),
(1018, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:10:51', '2021-09-09 05:10:51'),
(1019, 'http://agro.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:12:31', '2021-09-09 07:12:31'),
(1020, 'http://agro.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:12:31', '2021-09-09 07:12:31'),
(1021, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:11:04', '2021-09-09 05:11:04'),
(1022, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:11:04', '2021-09-09 05:11:04'),
(1023, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:11:06', '2021-09-09 05:11:06'),
(1024, 'http://agroregistry.loc/admin/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:11:07', '2021-09-09 05:11:07'),
(1025, 'http://agroregistry.loc/admin/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:11:08', '2021-09-09 05:11:08'),
(1026, 'http://agroregistry.loc/admin/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:11:09', '2021-09-09 05:11:09'),
(1027, 'http://agroregistry.loc/admin/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:11:09', '2021-09-09 05:11:09'),
(1028, 'http://agroregistry.loc/admin/tnved-datas', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:11:25', '2021-09-09 05:11:25'),
(1029, 'http://agroregistry.loc/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:11:27', '2021-09-09 05:11:27'),
(1030, 'http://agro.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:03', '2021-09-09 07:13:03'),
(1031, 'http://agro.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:04', '2021-09-09 07:13:04'),
(1032, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:07', '2021-09-09 07:13:07'),
(1033, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:22', '2021-09-09 07:13:22'),
(1034, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:31', '2021-09-09 07:13:31'),
(1035, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:31', '2021-09-09 07:13:31'),
(1036, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:32', '2021-09-09 07:13:32'),
(1037, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:39', '2021-09-09 07:13:39'),
(1038, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:39', '2021-09-09 07:13:39'),
(1039, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:40', '2021-09-09 07:13:40'),
(1040, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:48', '2021-09-09 07:13:48'),
(1041, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:48', '2021-09-09 07:13:48'),
(1042, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:50', '2021-09-09 07:13:50'),
(1043, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:58', '2021-09-09 07:13:58'),
(1044, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:13:58', '2021-09-09 07:13:58'),
(1045, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:00', '2021-09-09 07:14:00'),
(1046, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:10', '2021-09-09 07:14:10'),
(1047, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:10', '2021-09-09 07:14:10'),
(1048, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:16', '2021-09-09 07:14:16'),
(1049, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:19', '2021-09-09 07:14:19'),
(1050, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:19', '2021-09-09 07:14:19'),
(1051, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:26', '2021-09-09 07:14:26'),
(1052, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:29', '2021-09-09 07:14:29'),
(1053, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:29', '2021-09-09 07:14:29'),
(1054, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:31', '2021-09-09 07:14:31'),
(1055, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:39', '2021-09-09 07:14:39'),
(1056, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:39', '2021-09-09 07:14:39'),
(1057, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:41', '2021-09-09 07:14:41'),
(1058, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:49', '2021-09-09 07:14:49'),
(1059, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:50', '2021-09-09 07:14:50'),
(1060, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:14:52', '2021-09-09 07:14:52'),
(1061, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:00', '2021-09-09 07:15:00'),
(1062, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:00', '2021-09-09 07:15:00'),
(1063, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:02', '2021-09-09 07:15:02'),
(1064, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:10', '2021-09-09 07:15:10'),
(1065, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:10', '2021-09-09 07:15:10'),
(1066, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:11', '2021-09-09 07:15:11'),
(1067, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:19', '2021-09-09 07:15:19'),
(1068, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:19', '2021-09-09 07:15:19'),
(1069, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:29', '2021-09-09 07:15:29'),
(1070, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:32', '2021-09-09 07:15:32'),
(1071, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:33', '2021-09-09 07:15:33'),
(1072, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:38', '2021-09-09 07:15:38'),
(1073, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:41', '2021-09-09 07:15:41'),
(1074, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:41', '2021-09-09 07:15:41'),
(1075, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:49', '2021-09-09 07:15:49'),
(1076, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:52', '2021-09-09 07:15:52'),
(1077, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:52', '2021-09-09 07:15:52'),
(1078, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:15:54', '2021-09-09 07:15:54'),
(1079, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:02', '2021-09-09 07:16:02'),
(1080, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:02', '2021-09-09 07:16:02'),
(1081, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:03', '2021-09-09 07:16:03'),
(1082, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:10', '2021-09-09 07:16:10'),
(1083, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:11', '2021-09-09 07:16:11'),
(1084, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:12', '2021-09-09 07:16:12'),
(1085, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:19', '2021-09-09 07:16:19'),
(1086, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:19', '2021-09-09 07:16:19'),
(1087, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:21', '2021-09-09 07:16:21'),
(1088, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:28', '2021-09-09 07:16:28'),
(1089, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:28', '2021-09-09 07:16:28'),
(1090, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:30', '2021-09-09 07:16:30'),
(1091, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:37', '2021-09-09 07:16:37'),
(1092, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:37', '2021-09-09 07:16:37'),
(1093, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:38', '2021-09-09 07:16:38'),
(1094, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:45', '2021-09-09 07:16:45'),
(1095, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:45', '2021-09-09 07:16:45'),
(1096, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:46', '2021-09-09 07:16:46'),
(1097, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:53', '2021-09-09 07:16:53'),
(1098, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:53', '2021-09-09 07:16:53'),
(1099, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:16:55', '2021-09-09 07:16:55'),
(1100, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:02', '2021-09-09 07:17:02'),
(1101, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:02', '2021-09-09 07:17:02'),
(1102, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:04', '2021-09-09 07:17:04'),
(1103, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:11', '2021-09-09 07:17:11'),
(1104, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:11', '2021-09-09 07:17:11'),
(1105, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:16', '2021-09-09 07:17:16'),
(1106, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:19', '2021-09-09 07:17:19'),
(1107, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:19', '2021-09-09 07:17:19'),
(1108, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:24', '2021-09-09 07:17:24'),
(1109, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:27', '2021-09-09 07:17:27'),
(1110, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:27', '2021-09-09 07:17:27'),
(1111, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:29', '2021-09-09 07:17:29'),
(1112, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:35', '2021-09-09 07:17:35'),
(1113, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:36', '2021-09-09 07:17:36'),
(1114, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:37', '2021-09-09 07:17:37'),
(1115, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:43', '2021-09-09 07:17:43'),
(1116, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:44', '2021-09-09 07:17:44'),
(1117, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:45', '2021-09-09 07:17:45'),
(1118, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:56', '2021-09-09 07:17:56'),
(1119, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:17:56', '2021-09-09 07:17:56'),
(1120, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:04', '2021-09-09 07:18:04'),
(1121, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:13', '2021-09-09 07:18:13'),
(1122, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:14', '2021-09-09 07:18:14'),
(1123, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:15', '2021-09-09 07:18:15'),
(1124, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:21', '2021-09-09 07:18:21'),
(1125, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:22', '2021-09-09 07:18:22'),
(1126, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:23', '2021-09-09 07:18:23'),
(1127, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:30', '2021-09-09 07:18:30'),
(1128, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:30', '2021-09-09 07:18:30'),
(1129, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:32', '2021-09-09 07:18:32'),
(1130, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:38', '2021-09-09 07:18:38'),
(1131, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:38', '2021-09-09 07:18:38'),
(1132, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:39', '2021-09-09 07:18:39'),
(1133, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:45', '2021-09-09 07:18:45'),
(1134, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:46', '2021-09-09 07:18:46'),
(1135, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:47', '2021-09-09 07:18:47'),
(1136, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:53', '2021-09-09 07:18:53'),
(1137, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:54', '2021-09-09 07:18:54'),
(1138, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:18:55', '2021-09-09 07:18:55'),
(1139, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:19:01', '2021-09-09 07:19:01'),
(1140, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:19:02', '2021-09-09 07:19:02'),
(1141, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:19:03', '2021-09-09 07:19:03'),
(1142, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:19:21', '2021-09-09 07:19:21'),
(1143, 'http://agro.loc/admin/database/applying_for_testings/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:19:24', '2021-09-09 07:19:24'),
(1144, 'http://agro.loc/admin/database/applying_for_testings', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:19:41', '2021-09-09 07:19:41'),
(1145, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:19:42', '2021-09-09 07:19:42'),
(1146, 'http://agro.loc/admin/bread/applying_for_testings/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:19:44', '2021-09-09 07:19:44'),
(1147, 'http://agro.loc/admin/bread', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:21:21', '2021-09-09 07:21:21'),
(1148, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:21:22', '2021-09-09 07:21:22'),
(1149, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:20:04', '2021-09-09 05:20:04'),
(1150, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:21:40', '2021-09-09 07:21:40'),
(1151, 'http://agro.loc/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:21:42', '2021-09-09 07:21:42'),
(1152, 'http://agro.loc/admin/database', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:23:20', '2021-09-09 07:23:20'),
(1153, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:23:20', '2021-09-09 07:23:20'),
(1154, 'http://agro.loc/admin/bread/procedures/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:23:28', '2021-09-09 07:23:28'),
(1155, 'http://agro.loc/admin/bread', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:23:45', '2021-09-09 07:23:45'),
(1156, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:23:46', '2021-09-09 07:23:46'),
(1157, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:23:52', '2021-09-09 07:23:52'),
(1158, 'http://agro.loc/admin/applying-for-testings', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:24:08', '2021-09-09 07:24:08'),
(1159, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:24:18', '2021-09-09 07:24:18'),
(1160, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:24:24', '2021-09-09 07:24:24'),
(1161, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:27:35', '2021-09-09 07:27:35'),
(1162, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Symfony', NULL, '2021-09-09 05:26:14', '2021-09-09 05:26:14'),
(1163, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Symfony', NULL, '2021-09-09 05:26:16', '2021-09-09 05:26:16'),
(1164, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:21', '2021-09-09 05:26:21'),
(1165, 'http://agroregistry.loc/admin/login', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:21', '2021-09-09 05:26:21'),
(1166, 'http://agroregistry.loc/css/custom.css', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:23', '2021-09-09 05:26:23'),
(1167, 'http://agroregistry.loc/js/auth.js', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:23', '2021-09-09 05:26:23'),
(1168, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:24', '2021-09-09 05:26:24'),
(1169, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:24', '2021-09-09 05:26:24'),
(1170, 'http://agroregistry.loc/admin/login', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:31', '2021-09-09 05:26:31'),
(1171, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:31', '2021-09-09 05:26:31'),
(1172, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:34', '2021-09-09 05:26:34'),
(1173, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:34', '2021-09-09 05:26:34'),
(1174, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:34', '2021-09-09 05:26:34'),
(1175, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:35', '2021-09-09 05:26:35'),
(1176, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:26:36', '2021-09-09 05:26:36'),
(1177, 'http://agroregistry.loc/admin/settings', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:07', '2021-09-09 05:27:07'),
(1178, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:09', '2021-09-09 05:27:09'),
(1179, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:09', '2021-09-09 05:27:09'),
(1180, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:09', '2021-09-09 05:27:09'),
(1181, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:11', '2021-09-09 05:27:11'),
(1182, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:11', '2021-09-09 05:27:11'),
(1183, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:13', '2021-09-09 05:27:13'),
(1184, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:14', '2021-09-09 05:27:14'),
(1185, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:14', '2021-09-09 05:27:14'),
(1186, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:15', '2021-09-09 05:27:15'),
(1187, 'http://agroregistry.loc/admin/settings', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:16', '2021-09-09 05:27:16'),
(1188, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:18', '2021-09-09 05:27:18'),
(1189, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:18', '2021-09-09 05:27:18');
INSERT INTO `activities` (`id`, `url`, `method`, `ip`, `agent`, `user_id`, `created_at`, `updated_at`) VALUES
(1190, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:18', '2021-09-09 05:27:18'),
(1191, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:19', '2021-09-09 05:27:19'),
(1192, 'http://agroregistry.loc/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:27:43', '2021-09-09 05:27:43'),
(1193, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:28:10', '2021-09-09 05:28:10'),
(1194, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:28:15', '2021-09-09 05:28:15'),
(1195, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:28:15', '2021-09-09 05:28:15'),
(1196, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:28:15', '2021-09-09 05:28:15'),
(1197, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:28:16', '2021-09-09 05:28:16'),
(1198, 'http://agro.loc/admin/applying-for-testings', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:30:10', '2021-09-09 07:30:10'),
(1199, 'http://agroregistry.loc/admin/bread/parameters/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:28:58', '2021-09-09 05:28:58'),
(1200, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:29:00', '2021-09-09 05:29:00'),
(1201, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:29:00', '2021-09-09 05:29:00'),
(1202, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:29:01', '2021-09-09 05:29:01'),
(1203, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:29:01', '2021-09-09 05:29:01'),
(1204, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:30:34', '2021-09-09 07:30:34'),
(1205, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:29:06', '2021-09-09 05:29:06'),
(1206, 'http://agro.loc/admin/navbar-menus', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:31:41', '2021-09-09 07:31:41'),
(1207, 'http://agro.loc/admin/products', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:31:47', '2021-09-09 07:31:47'),
(1208, 'http://agro.loc/admin/vehicles', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:31:51', '2021-09-09 07:31:51'),
(1209, 'http://agro.loc/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:31:52', '2021-09-09 07:31:52'),
(1210, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Symfony', NULL, '2021-09-09 05:36:56', '2021-09-09 05:36:56'),
(1211, 'http://127.0.0.1:8000', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:37:00', '2021-09-09 05:37:00'),
(1212, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:37:09', '2021-09-09 05:37:09'),
(1213, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:37:13', '2021-09-09 05:37:13'),
(1214, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:37:14', '2021-09-09 05:37:14'),
(1215, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:37:14', '2021-09-09 05:37:14'),
(1216, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:37:15', '2021-09-09 05:37:15'),
(1217, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:37:15', '2021-09-09 05:37:15'),
(1218, 'http://agroregistry.loc/assets/icons/address.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:37:18', '2021-09-09 05:37:18'),
(1219, 'http://agroregistry.loc/img/icons/search.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:37:19', '2021-09-09 05:37:19'),
(1220, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:40:41', '2021-09-09 05:40:41'),
(1221, 'http://agroregistry.loc/img/icons/search.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:40:43', '2021-09-09 05:40:43'),
(1222, 'http://agroregistry.loc/assets/icons/address.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:40:43', '2021-09-09 05:40:43'),
(1223, 'http://127.0.0.1:8000', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:41:25', '2021-09-09 05:41:25'),
(1224, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:41:34', '2021-09-09 05:41:34'),
(1225, 'http://agroregistry.loc/assets/icons/address.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:41:35', '2021-09-09 05:41:35'),
(1226, 'http://agroregistry.loc/img/icons/search.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:41:36', '2021-09-09 05:41:36'),
(1227, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:17', '2021-09-09 05:43:17'),
(1228, 'http://agroregistry.loc/assets/icons/address.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:19', '2021-09-09 05:43:19'),
(1229, 'http://agroregistry.loc/assets/icons/phone.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:20', '2021-09-09 05:43:20'),
(1230, 'http://agroregistry.loc/img/icons/search.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:20', '2021-09-09 05:43:20'),
(1231, 'http://agroregistry.loc/assets/icons/email.svg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:20', '2021-09-09 05:43:20'),
(1232, 'http://agroregistry.loc/images/layout/banner.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:21', '2021-09-09 05:43:21'),
(1233, 'http://agroregistry.loc/assets/icons/youtube.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:22', '2021-09-09 05:43:22'),
(1234, 'http://agroregistry.loc/assets/icons/instagram.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:23', '2021-09-09 05:43:23'),
(1235, 'http://agroregistry.loc/assets/icons/facebook.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:23', '2021-09-09 05:43:23'),
(1236, 'http://agroregistry.loc/assets/slider/slide1.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:23', '2021-09-09 05:43:23'),
(1237, 'http://agroregistry.loc/assets/icons/telegram.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:24', '2021-09-09 05:43:24'),
(1238, 'http://agroregistry.loc/assets/icons/services_layer/1.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:24', '2021-09-09 05:43:24'),
(1239, 'http://agroregistry.loc/assets/icons/services_layer/2.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:24', '2021-09-09 05:43:24'),
(1240, 'http://agroregistry.loc/assets/icons/services_layer/x.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:25', '2021-09-09 05:43:25'),
(1241, 'http://agroregistry.loc/assets/icons/services_layer/3.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:25', '2021-09-09 05:43:25'),
(1242, 'http://agroregistry.loc/assets/icons/services_layer/4.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:26', '2021-09-09 05:43:26'),
(1243, 'http://agroregistry.loc/assets/icons/services_layer/5.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:26', '2021-09-09 05:43:26'),
(1244, 'http://agroregistry.loc/assets/slider/1.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:26', '2021-09-09 05:43:26'),
(1245, 'http://agroregistry.loc/assets/slider/2.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:27', '2021-09-09 05:43:27'),
(1246, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:35', '2021-09-09 05:43:35'),
(1247, 'http://agroregistry.loc/assets/icons/address.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:36', '2021-09-09 05:43:36'),
(1248, 'http://agroregistry.loc/img/icons/search.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:37', '2021-09-09 05:43:37'),
(1249, 'http://agroregistry.loc/assets/icons/email.svg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:37', '2021-09-09 05:43:37'),
(1250, 'http://agroregistry.loc/assets/icons/phone.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:37', '2021-09-09 05:43:37'),
(1251, 'http://agroregistry.loc/assets/icons/instagram.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:37', '2021-09-09 05:43:37'),
(1252, 'http://agroregistry.loc/images/layout/banner.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:38', '2021-09-09 05:43:38'),
(1253, 'http://agroregistry.loc/assets/icons/youtube.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:38', '2021-09-09 05:43:38'),
(1254, 'http://agroregistry.loc/assets/icons/facebook.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:38', '2021-09-09 05:43:38'),
(1255, 'http://agroregistry.loc/assets/slider/slide1.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:38', '2021-09-09 05:43:38'),
(1256, 'http://agroregistry.loc/assets/icons/telegram.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:39', '2021-09-09 05:43:39'),
(1257, 'http://agroregistry.loc/assets/icons/services_layer/1.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:39', '2021-09-09 05:43:39'),
(1258, 'http://agroregistry.loc/assets/icons/services_layer/2.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:40', '2021-09-09 05:43:40'),
(1259, 'http://agroregistry.loc/assets/icons/services_layer/x.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:40', '2021-09-09 05:43:40'),
(1260, 'http://agroregistry.loc/assets/icons/services_layer/3.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:41', '2021-09-09 05:43:41'),
(1261, 'http://agroregistry.loc/assets/icons/services_layer/4.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:42', '2021-09-09 05:43:42'),
(1262, 'http://agroregistry.loc/assets/icons/services_layer/5.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:42', '2021-09-09 05:43:42'),
(1263, 'http://agroregistry.loc/assets/slider/1.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:43', '2021-09-09 05:43:43'),
(1264, 'http://agroregistry.loc/assets/slider/2.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:43:43', '2021-09-09 05:43:43'),
(1265, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:01', '2021-09-09 05:44:01'),
(1266, 'http://agroregistry.loc/assets/icons/address.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:03', '2021-09-09 05:44:03'),
(1267, 'http://agroregistry.loc/assets/icons/email.svg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:03', '2021-09-09 05:44:03'),
(1268, 'http://agroregistry.loc/assets/icons/phone.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:03', '2021-09-09 05:44:03'),
(1269, 'http://agroregistry.loc/img/icons/search.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:04', '2021-09-09 05:44:04'),
(1270, 'http://agroregistry.loc/images/layout/banner.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:05', '2021-09-09 05:44:05'),
(1271, 'http://agroregistry.loc/assets/icons/youtube.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:05', '2021-09-09 05:44:05'),
(1272, 'http://agroregistry.loc/assets/icons/instagram.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:05', '2021-09-09 05:44:05'),
(1273, 'http://agroregistry.loc/assets/icons/facebook.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:06', '2021-09-09 05:44:06'),
(1274, 'http://agroregistry.loc/assets/icons/telegram.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:06', '2021-09-09 05:44:06'),
(1275, 'http://agroregistry.loc/assets/slider/slide1.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:07', '2021-09-09 05:44:07'),
(1276, 'http://agroregistry.loc/assets/icons/services_layer/1.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:07', '2021-09-09 05:44:07'),
(1277, 'http://agroregistry.loc/assets/icons/services_layer/2.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:07', '2021-09-09 05:44:07'),
(1278, 'http://agroregistry.loc/assets/icons/services_layer/x.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:08', '2021-09-09 05:44:08'),
(1279, 'http://agroregistry.loc/assets/icons/services_layer/3.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:08', '2021-09-09 05:44:08'),
(1280, 'http://agroregistry.loc/assets/icons/services_layer/4.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:08', '2021-09-09 05:44:08'),
(1281, 'http://agroregistry.loc/assets/icons/services_layer/5.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:08', '2021-09-09 05:44:08'),
(1282, 'http://agroregistry.loc/assets/slider/1.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:09', '2021-09-09 05:44:09'),
(1283, 'http://agroregistry.loc/assets/slider/2.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:09', '2021-09-09 05:44:09'),
(1284, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:12', '2021-09-09 05:44:12'),
(1285, 'http://agroregistry.loc/assets/icons/phone.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:14', '2021-09-09 05:44:14'),
(1286, 'http://agroregistry.loc/assets/icons/address.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:14', '2021-09-09 05:44:14'),
(1287, 'http://agroregistry.loc/img/icons/search.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:14', '2021-09-09 05:44:14'),
(1288, 'http://agroregistry.loc/assets/icons/email.svg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:15', '2021-09-09 05:44:15'),
(1289, 'http://agroregistry.loc/assets/icons/instagram.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:16', '2021-09-09 05:44:16'),
(1290, 'http://agroregistry.loc/assets/icons/youtube.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:16', '2021-09-09 05:44:16'),
(1291, 'http://agroregistry.loc/images/layout/banner.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:17', '2021-09-09 05:44:17'),
(1292, 'http://agroregistry.loc/assets/icons/facebook.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:18', '2021-09-09 05:44:18'),
(1293, 'http://agroregistry.loc/assets/icons/telegram.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:18', '2021-09-09 05:44:18'),
(1294, 'http://agroregistry.loc/assets/icons/services_layer/x.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:18', '2021-09-09 05:44:18'),
(1295, 'http://agroregistry.loc/assets/icons/services_layer/1.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:19', '2021-09-09 05:44:19'),
(1296, 'http://agroregistry.loc/assets/slider/1.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:20', '2021-09-09 05:44:20'),
(1297, 'http://agroregistry.loc/assets/icons/services_layer/2.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:20', '2021-09-09 05:44:20'),
(1298, 'http://agroregistry.loc/assets/icons/services_layer/4.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:21', '2021-09-09 05:44:21'),
(1299, 'http://agroregistry.loc/assets/slider/slide1.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:21', '2021-09-09 05:44:21'),
(1300, 'http://agroregistry.loc/assets/icons/services_layer/5.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:21', '2021-09-09 05:44:21'),
(1301, 'http://agroregistry.loc/assets/slider/2.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:44:22', '2021-09-09 05:44:22'),
(1302, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Symfony', NULL, '2021-09-09 05:45:17', '2021-09-09 05:45:17'),
(1303, 'http://127.0.0.1:8000', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:45:22', '2021-09-09 05:45:22'),
(1304, 'http://127.0.0.1:8000', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:46:21', '2021-09-09 05:46:21'),
(1305, 'http://agro.loc/admin/procedures', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:48:36', '2021-09-09 07:48:36'),
(1306, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:48:10', '2021-09-09 05:48:10'),
(1307, 'http://agroregistry.loc/img/icons/search.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:48:11', '2021-09-09 05:48:11'),
(1308, 'http://agroregistry.loc/assets/icons/address.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:48:12', '2021-09-09 05:48:12'),
(1309, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:19', '2021-09-09 05:52:19'),
(1310, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:21', '2021-09-09 05:52:21'),
(1311, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:21', '2021-09-09 05:52:21'),
(1312, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:22', '2021-09-09 05:52:22'),
(1313, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:22', '2021-09-09 05:52:22'),
(1314, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:23', '2021-09-09 05:52:23'),
(1315, 'http://agroregistry.loc/admin/compass', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:42', '2021-09-09 05:52:42'),
(1316, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:44', '2021-09-09 05:52:44'),
(1317, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:45', '2021-09-09 05:52:45'),
(1318, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:45', '2021-09-09 05:52:45'),
(1319, 'http://agroregistry.loc/admin/voyager-assets?path=images%2Fcompass%2Fvoyager-home.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:46', '2021-09-09 05:52:46'),
(1320, 'http://agroregistry.loc/admin/voyager-assets?path=images%2Fcompass%2Fdocumentation.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:47', '2021-09-09 05:52:47'),
(1321, 'http://agroregistry.loc/admin/voyager-assets?path=images%2Fbg.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:47', '2021-09-09 05:52:47'),
(1322, 'http://agroregistry.loc/admin/voyager-assets?path=images%2Fcompass%2Fhooks.jpg', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:48', '2021-09-09 05:52:48'),
(1323, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:52', '2021-09-09 05:52:52'),
(1324, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:54', '2021-09-09 05:52:54'),
(1325, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:54', '2021-09-09 05:52:54'),
(1326, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:54', '2021-09-09 05:52:54'),
(1327, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:52:54', '2021-09-09 05:52:54'),
(1328, 'http://agroregistry.loc/admin/database/tnveds', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:54:25', '2021-09-09 05:54:25'),
(1329, 'http://agroregistry.loc/admin/database/tnveds', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:54:27', '2021-09-09 05:54:27'),
(1330, 'http://agroregistry.loc/admin/tnveds', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:54:28', '2021-09-09 05:54:28'),
(1331, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:54:32', '2021-09-09 05:54:32'),
(1332, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:54:32', '2021-09-09 05:54:32'),
(1333, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:54:32', '2021-09-09 05:54:32'),
(1334, 'http://agroregistry.loc/admin/database/tnved_datas', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:55:03', '2021-09-09 05:55:03'),
(1335, 'http://agroregistry.loc/admin/database/tnved_datas/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:55:05', '2021-09-09 05:55:05'),
(1336, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:55:07', '2021-09-09 05:55:07'),
(1337, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:55:07', '2021-09-09 05:55:07'),
(1338, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:55:08', '2021-09-09 05:55:08'),
(1339, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:55:08', '2021-09-09 05:55:08'),
(1340, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:56:44', '2021-09-09 07:56:44'),
(1341, 'http://agro.loc/admin/database/procedures/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 07:56:51', '2021-09-09 07:56:51'),
(1342, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:56:23', '2021-09-09 05:56:23'),
(1343, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:56:24', '2021-09-09 05:56:24'),
(1344, 'http://agroregistry.loc/admin/database/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:56:34', '2021-09-09 05:56:34'),
(1345, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:56:36', '2021-09-09 05:56:36'),
(1346, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:56:36', '2021-09-09 05:56:36'),
(1347, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:56:36', '2021-09-09 05:56:36'),
(1348, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:56:37', '2021-09-09 05:56:37'),
(1349, 'http://agroregistry.loc/admin/database', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:05', '2021-09-09 05:57:05'),
(1350, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:05', '2021-09-09 05:57:05'),
(1351, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:07', '2021-09-09 05:57:07'),
(1352, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:08', '2021-09-09 05:57:08'),
(1353, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:08', '2021-09-09 05:57:08'),
(1354, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:08', '2021-09-09 05:57:08'),
(1355, 'http://agroregistry.loc/admin/bread/tnved_codes/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:18', '2021-09-09 05:57:18'),
(1356, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:20', '2021-09-09 05:57:20'),
(1357, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:21', '2021-09-09 05:57:21'),
(1358, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:21', '2021-09-09 05:57:21'),
(1359, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:21', '2021-09-09 05:57:21'),
(1360, 'http://agroregistry.loc/admin/bread', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:31', '2021-09-09 05:57:31'),
(1361, 'http://agroregistry.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:32', '2021-09-09 05:57:32'),
(1362, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:34', '2021-09-09 05:57:34'),
(1363, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:34', '2021-09-09 05:57:34'),
(1364, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:34', '2021-09-09 05:57:34'),
(1365, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:35', '2021-09-09 05:57:35'),
(1366, 'http://agroregistry.loc/admin/tnved-codes', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:39', '2021-09-09 05:57:39'),
(1367, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:41', '2021-09-09 05:57:41'),
(1368, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:41', '2021-09-09 05:57:41'),
(1369, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:41', '2021-09-09 05:57:41'),
(1370, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:41', '2021-09-09 05:57:41'),
(1371, 'http://agroregistry.loc/admin/tnved-codes/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:43', '2021-09-09 05:57:43'),
(1372, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:45', '2021-09-09 05:57:45'),
(1373, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:45', '2021-09-09 05:57:45'),
(1374, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:46', '2021-09-09 05:57:46'),
(1375, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:47', '2021-09-09 05:57:47'),
(1376, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:47', '2021-09-09 05:57:47'),
(1377, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:47', '2021-09-09 05:57:47'),
(1378, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:48', '2021-09-09 05:57:48'),
(1379, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:49', '2021-09-09 05:57:49'),
(1380, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:57:58', '2021-09-09 05:57:58'),
(1381, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:00', '2021-09-09 05:58:00'),
(1382, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:00', '2021-09-09 05:58:00'),
(1383, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:01', '2021-09-09 05:58:01'),
(1384, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:01', '2021-09-09 05:58:01'),
(1385, 'http://agroregistry.loc/admin/bread/26', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:28', '2021-09-09 05:58:28'),
(1386, 'http://agroregistry.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:28', '2021-09-09 05:58:28');
INSERT INTO `activities` (`id`, `url`, `method`, `ip`, `agent`, `user_id`, `created_at`, `updated_at`) VALUES
(1387, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:30', '2021-09-09 05:58:30'),
(1388, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:30', '2021-09-09 05:58:30'),
(1389, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:30', '2021-09-09 05:58:30'),
(1390, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:31', '2021-09-09 05:58:31'),
(1391, 'http://agroregistry.loc/admin/bread/27', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:45', '2021-09-09 05:58:45'),
(1392, 'http://agroregistry.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:46', '2021-09-09 05:58:46'),
(1393, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:48', '2021-09-09 05:58:48'),
(1394, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:48', '2021-09-09 05:58:48'),
(1395, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:48', '2021-09-09 05:58:48'),
(1396, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:58:48', '2021-09-09 05:58:48'),
(1397, 'http://agro.loc/admin/database/procedures', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:00:35', '2021-09-09 08:00:35'),
(1398, 'http://agro.loc/admin/database/procedures/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:00:36', '2021-09-09 08:00:36'),
(1399, 'http://agro.loc/admin/procedures', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:00:45', '2021-09-09 08:00:45'),
(1400, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:13', '2021-09-09 05:59:13'),
(1401, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:16', '2021-09-09 05:59:16'),
(1402, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:16', '2021-09-09 05:59:16'),
(1403, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:16', '2021-09-09 05:59:16'),
(1404, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:17', '2021-09-09 05:59:17'),
(1405, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:00:50', '2021-09-09 08:00:50'),
(1406, 'http://agroregistry.loc/admin/database/tnved_datas', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:23', '2021-09-09 05:59:23'),
(1407, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:24', '2021-09-09 05:59:24'),
(1408, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:25', '2021-09-09 05:59:25'),
(1409, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:26', '2021-09-09 05:59:26'),
(1410, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:26', '2021-09-09 05:59:26'),
(1411, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:26', '2021-09-09 05:59:26'),
(1412, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:01:05', '2021-09-09 08:01:05'),
(1413, 'http://agroregistry.loc/admin/database/tnveds', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:40', '2021-09-09 05:59:40'),
(1414, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:41', '2021-09-09 05:59:41'),
(1415, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:43', '2021-09-09 05:59:43'),
(1416, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:43', '2021-09-09 05:59:43'),
(1417, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:43', '2021-09-09 05:59:43'),
(1418, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:43', '2021-09-09 05:59:43'),
(1419, 'http://agro.loc/admin/bread/29', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:01:17', '2021-09-09 08:01:17'),
(1420, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:01:18', '2021-09-09 08:01:18'),
(1421, 'http://agroregistry.loc/admin/tnved-codes', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:48', '2021-09-09 05:59:48'),
(1422, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:50', '2021-09-09 05:59:50'),
(1423, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:50', '2021-09-09 05:59:50'),
(1424, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:51', '2021-09-09 05:59:51'),
(1425, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 05:59:51', '2021-09-09 05:59:51'),
(1426, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:01:26', '2021-09-09 08:01:26'),
(1427, 'http://agro.loc/admin/database/procedures/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:01:32', '2021-09-09 08:01:32'),
(1428, 'http://agro.loc/admin/database/procedures', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:02:42', '2021-09-09 08:02:42'),
(1429, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:02:42', '2021-09-09 08:02:42'),
(1430, 'http://agro.loc/admin/bread/procedures/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:02:48', '2021-09-09 08:02:48'),
(1431, 'http://agroregistry.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:01:24', '2021-09-09 06:01:24'),
(1432, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:01:30', '2021-09-09 06:01:30'),
(1433, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:01:30', '2021-09-09 06:01:30'),
(1434, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:01:30', '2021-09-09 06:01:30'),
(1435, 'http://agro.loc/admin/bread', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:03:05', '2021-09-09 08:03:05'),
(1436, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:03:05', '2021-09-09 08:03:05'),
(1437, 'http://agro.loc/admin/procedures', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:03:09', '2021-09-09 08:03:09'),
(1438, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:03:21', '2021-09-09 08:03:21'),
(1439, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:03:27', '2021-09-09 08:03:27'),
(1440, 'http://agro.loc/admin/bread/31', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:03:34', '2021-09-09 08:03:34'),
(1441, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:03:34', '2021-09-09 08:03:34'),
(1442, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:03:42', '2021-09-09 08:03:42'),
(1443, 'http://agro.loc/admin/database/procedures/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:03:46', '2021-09-09 08:03:46'),
(1444, 'http://agro.loc/admin/database/procedures', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:04:12', '2021-09-09 08:04:12'),
(1445, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:04:12', '2021-09-09 08:04:12'),
(1446, 'http://agro.loc/admin/bread/procedures/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:04:17', '2021-09-09 08:04:17'),
(1447, 'http://agroregistry.loc', 'GET', '127.0.0.1', 'Symfony', NULL, '2021-09-09 06:02:55', '2021-09-09 06:02:55'),
(1448, 'http://agro.loc/admin/bread', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:04:34', '2021-09-09 08:04:34'),
(1449, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:04:35', '2021-09-09 08:04:35'),
(1450, 'http://agroregistry.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:08', '2021-09-09 06:03:08'),
(1451, 'http://agro.loc/admin/vehicles', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:04:45', '2021-09-09 08:04:45'),
(1452, 'http://agro.loc/_ignition/health-check', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:04:46', '2021-09-09 08:04:46'),
(1453, 'http://agro.loc/admin/products', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:04:50', '2021-09-09 08:04:50'),
(1454, 'http://agroregistry.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:25', '2021-09-09 06:03:25'),
(1455, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:04:59', '2021-09-09 08:04:59'),
(1456, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:05:09', '2021-09-09 08:05:09'),
(1457, 'http://agroregistry.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:41', '2021-09-09 06:03:41'),
(1458, 'http://agro.loc/admin/database/gosts/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:05:16', '2021-09-09 08:05:16'),
(1459, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:45', '2021-09-09 06:03:45'),
(1460, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:45', '2021-09-09 06:03:45'),
(1461, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:45', '2021-09-09 06:03:45'),
(1462, 'http://agroregistry.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:46', '2021-09-09 06:03:46'),
(1463, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:48', '2021-09-09 06:03:48'),
(1464, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:48', '2021-09-09 06:03:48'),
(1465, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:48', '2021-09-09 06:03:48'),
(1466, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:49', '2021-09-09 06:03:49'),
(1467, 'http://agroregistry.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:03:52', '2021-09-09 06:03:52'),
(1468, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:05:30', '2021-09-09 08:05:30'),
(1469, 'http://agroregistry.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:04:13', '2021-09-09 06:04:13'),
(1470, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:05:48', '2021-09-09 08:05:48'),
(1471, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:04:16', '2021-09-09 06:04:16'),
(1472, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:04:16', '2021-09-09 06:04:16'),
(1473, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:04:16', '2021-09-09 06:04:16'),
(1474, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:04:17', '2021-09-09 06:04:17'),
(1475, 'http://agro.loc/admin/procedures', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:05:55', '2021-09-09 08:05:55'),
(1476, 'http://agro.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:06:13', '2021-09-09 08:06:13'),
(1477, 'http://agro.loc/admin/bread/procedures/edit', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:06:21', '2021-09-09 08:06:21'),
(1478, 'http://agroregistry.loc/admin/settings', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:04:55', '2021-09-09 06:04:55'),
(1479, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:04:58', '2021-09-09 06:04:58'),
(1480, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:04:58', '2021-09-09 06:04:58'),
(1481, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:04:58', '2021-09-09 06:04:58'),
(1482, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:04:58', '2021-09-09 06:04:58'),
(1483, 'http://agro.loc/admin/bread/32', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:06:33', '2021-09-09 08:06:33'),
(1484, 'http://agro.loc/admin/bread', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:06:33', '2021-09-09 08:06:33'),
(1485, 'http://agroregistry.loc/admin/database', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:05:03', '2021-09-09 06:05:03'),
(1486, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:05:05', '2021-09-09 06:05:05'),
(1487, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:05:05', '2021-09-09 06:05:05'),
(1488, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:05:06', '2021-09-09 06:05:06'),
(1489, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:05:06', '2021-09-09 06:05:06'),
(1490, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:06:43', '2021-09-09 08:06:43'),
(1491, 'http://agro.loc/admin/procedures', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:06:50', '2021-09-09 08:06:50'),
(1492, 'http://agroregistry.loc/admin/bread/parameters/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:05:18', '2021-09-09 06:05:18'),
(1493, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:05:20', '2021-09-09 06:05:20'),
(1494, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:05:20', '2021-09-09 06:05:20'),
(1495, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:05:20', '2021-09-09 06:05:20'),
(1496, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:05:21', '2021-09-09 06:05:21'),
(1497, 'http://agroregistry.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:06:40', '2021-09-09 06:06:40'),
(1498, 'http://agroregistry.loc/storage/settings/August2021/XoJJahDi2cuuwtexWzKW.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:06:43', '2021-09-09 06:06:43'),
(1499, 'http://agroregistry.loc/storage/settings/August2021/bmRGGaLT6PQOrWyYRJJ2.gif', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:06:43', '2021-09-09 06:06:43'),
(1500, 'http://agroregistry.loc/storage/users/default.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:06:43', '2021-09-09 06:06:43'),
(1501, 'http://agroregistry.loc/storage/settings/August2021/7SovzNAV0gHIzIfc647z.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', NULL, '2021-09-09 06:06:44', '2021-09-09 06:06:44'),
(1502, 'http://agro.test/ad', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 06:07:05', '2021-09-09 06:07:05'),
(1503, 'http://agro.test', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 06:07:09', '2021-09-09 06:07:09'),
(1504, 'http://agro.test/assets/icons/address.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 06:07:10', '2021-09-09 06:07:10'),
(1505, 'http://agro.test/img/icons/search.png', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', NULL, '2021-09-09 06:07:10', '2021-09-09 06:07:10'),
(1506, 'http://agro.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:18:35', '2021-09-09 08:18:35'),
(1507, 'http://agro.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:18:39', '2021-09-09 08:18:39'),
(1508, 'http://agro.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:18:40', '2021-09-09 08:18:40'),
(1509, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:18:45', '2021-09-09 08:18:45'),
(1510, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:00', '2021-09-09 08:20:00'),
(1511, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:08', '2021-09-09 08:20:08'),
(1512, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:08', '2021-09-09 08:20:08'),
(1513, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:10', '2021-09-09 08:20:10'),
(1514, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:15', '2021-09-09 08:20:15'),
(1515, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:15', '2021-09-09 08:20:15'),
(1516, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:20', '2021-09-09 08:20:20'),
(1517, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:23', '2021-09-09 08:20:23'),
(1518, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:23', '2021-09-09 08:20:23'),
(1519, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:29', '2021-09-09 08:20:29'),
(1520, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:33', '2021-09-09 08:20:33'),
(1521, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:33', '2021-09-09 08:20:33'),
(1522, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:36', '2021-09-09 08:20:36'),
(1523, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:47', '2021-09-09 08:20:47'),
(1524, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:47', '2021-09-09 08:20:47'),
(1525, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:51', '2021-09-09 08:20:51'),
(1526, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:54', '2021-09-09 08:20:54'),
(1527, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:54', '2021-09-09 08:20:54'),
(1528, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:20:59', '2021-09-09 08:20:59'),
(1529, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:02', '2021-09-09 08:21:02'),
(1530, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:02', '2021-09-09 08:21:02'),
(1531, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:04', '2021-09-09 08:21:04'),
(1532, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:09', '2021-09-09 08:21:09'),
(1533, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:09', '2021-09-09 08:21:09'),
(1534, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:11', '2021-09-09 08:21:11'),
(1535, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:16', '2021-09-09 08:21:16'),
(1536, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:16', '2021-09-09 08:21:16'),
(1537, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:18', '2021-09-09 08:21:18'),
(1538, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:24', '2021-09-09 08:21:24'),
(1539, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:24', '2021-09-09 08:21:24'),
(1540, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:26', '2021-09-09 08:21:26'),
(1541, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:31', '2021-09-09 08:21:31'),
(1542, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:31', '2021-09-09 08:21:31'),
(1543, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:33', '2021-09-09 08:21:33'),
(1544, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:38', '2021-09-09 08:21:38'),
(1545, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:38', '2021-09-09 08:21:38'),
(1546, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:40', '2021-09-09 08:21:40'),
(1547, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:45', '2021-09-09 08:21:45'),
(1548, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:46', '2021-09-09 08:21:46'),
(1549, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:21:47', '2021-09-09 08:21:47'),
(1550, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:22:39', '2021-09-09 08:22:39'),
(1551, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:22:39', '2021-09-09 08:22:39'),
(1552, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:22:41', '2021-09-09 08:22:41'),
(1553, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:22:47', '2021-09-09 08:22:47'),
(1554, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:22:47', '2021-09-09 08:22:47'),
(1555, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:22:49', '2021-09-09 08:22:49'),
(1556, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:22:54', '2021-09-09 08:22:54'),
(1557, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:22:54', '2021-09-09 08:22:54'),
(1558, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:22:56', '2021-09-09 08:22:56'),
(1559, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:01', '2021-09-09 08:23:01'),
(1560, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:01', '2021-09-09 08:23:01'),
(1561, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:03', '2021-09-09 08:23:03'),
(1562, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:09', '2021-09-09 08:23:09'),
(1563, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:09', '2021-09-09 08:23:09'),
(1564, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:10', '2021-09-09 08:23:10'),
(1565, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:17', '2021-09-09 08:23:17'),
(1566, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:17', '2021-09-09 08:23:17'),
(1567, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:19', '2021-09-09 08:23:19'),
(1568, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:24', '2021-09-09 08:23:24'),
(1569, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:24', '2021-09-09 08:23:24'),
(1570, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:26', '2021-09-09 08:23:26'),
(1571, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:32', '2021-09-09 08:23:32'),
(1572, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:32', '2021-09-09 08:23:32'),
(1573, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:34', '2021-09-09 08:23:34'),
(1574, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:40', '2021-09-09 08:23:40'),
(1575, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:40', '2021-09-09 08:23:40'),
(1576, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:41', '2021-09-09 08:23:41'),
(1577, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:47', '2021-09-09 08:23:47'),
(1578, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:47', '2021-09-09 08:23:47'),
(1579, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:49', '2021-09-09 08:23:49'),
(1580, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:54', '2021-09-09 08:23:54'),
(1581, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:55', '2021-09-09 08:23:55'),
(1582, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:23:56', '2021-09-09 08:23:56'),
(1583, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:02', '2021-09-09 08:24:02');
INSERT INTO `activities` (`id`, `url`, `method`, `ip`, `agent`, `user_id`, `created_at`, `updated_at`) VALUES
(1584, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:02', '2021-09-09 08:24:02'),
(1585, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:04', '2021-09-09 08:24:04'),
(1586, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:10', '2021-09-09 08:24:10'),
(1587, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:10', '2021-09-09 08:24:10'),
(1588, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:12', '2021-09-09 08:24:12'),
(1589, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:18', '2021-09-09 08:24:18'),
(1590, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:18', '2021-09-09 08:24:18'),
(1591, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:19', '2021-09-09 08:24:19'),
(1592, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:25', '2021-09-09 08:24:25'),
(1593, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:26', '2021-09-09 08:24:26'),
(1594, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:27', '2021-09-09 08:24:27'),
(1595, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:33', '2021-09-09 08:24:33'),
(1596, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:33', '2021-09-09 08:24:33'),
(1597, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:35', '2021-09-09 08:24:35'),
(1598, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:40', '2021-09-09 08:24:40'),
(1599, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:40', '2021-09-09 08:24:40'),
(1600, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:42', '2021-09-09 08:24:42'),
(1601, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:47', '2021-09-09 08:24:47'),
(1602, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:48', '2021-09-09 08:24:48'),
(1603, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:49', '2021-09-09 08:24:49'),
(1604, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:55', '2021-09-09 08:24:55'),
(1605, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:55', '2021-09-09 08:24:55'),
(1606, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:24:57', '2021-09-09 08:24:57'),
(1607, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:03', '2021-09-09 08:25:03'),
(1608, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:03', '2021-09-09 08:25:03'),
(1609, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:05', '2021-09-09 08:25:05'),
(1610, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:10', '2021-09-09 08:25:10'),
(1611, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:10', '2021-09-09 08:25:10'),
(1612, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:12', '2021-09-09 08:25:12'),
(1613, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:17', '2021-09-09 08:25:17'),
(1614, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:18', '2021-09-09 08:25:18'),
(1615, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:20', '2021-09-09 08:25:20'),
(1616, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:24', '2021-09-09 08:25:24'),
(1617, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:25', '2021-09-09 08:25:25'),
(1618, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:27', '2021-09-09 08:25:27'),
(1619, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:34', '2021-09-09 08:25:34'),
(1620, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:34', '2021-09-09 08:25:34'),
(1621, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:36', '2021-09-09 08:25:36'),
(1622, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:42', '2021-09-09 08:25:42'),
(1623, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:43', '2021-09-09 08:25:43'),
(1624, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:45', '2021-09-09 08:25:45'),
(1625, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:51', '2021-09-09 08:25:51'),
(1626, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:52', '2021-09-09 08:25:52'),
(1627, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:54', '2021-09-09 08:25:54'),
(1628, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:58', '2021-09-09 08:25:58'),
(1629, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:25:59', '2021-09-09 08:25:59'),
(1630, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:00', '2021-09-09 08:26:00'),
(1631, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:06', '2021-09-09 08:26:06'),
(1632, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:07', '2021-09-09 08:26:07'),
(1633, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:08', '2021-09-09 08:26:08'),
(1634, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:14', '2021-09-09 08:26:14'),
(1635, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:14', '2021-09-09 08:26:14'),
(1636, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:16', '2021-09-09 08:26:16'),
(1637, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:21', '2021-09-09 08:26:21'),
(1638, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:21', '2021-09-09 08:26:21'),
(1639, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:23', '2021-09-09 08:26:23'),
(1640, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:28', '2021-09-09 08:26:28'),
(1641, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:28', '2021-09-09 08:26:28'),
(1642, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:30', '2021-09-09 08:26:30'),
(1643, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:36', '2021-09-09 08:26:36'),
(1644, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:36', '2021-09-09 08:26:36'),
(1645, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:37', '2021-09-09 08:26:37'),
(1646, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:58', '2021-09-09 08:26:58'),
(1647, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:26:59', '2021-09-09 08:26:59'),
(1648, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:04', '2021-09-09 08:27:04'),
(1649, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:09', '2021-09-09 08:27:09'),
(1650, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:09', '2021-09-09 08:27:09'),
(1651, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:22', '2021-09-09 08:27:22'),
(1652, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:25', '2021-09-09 08:27:25'),
(1653, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:25', '2021-09-09 08:27:25'),
(1654, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:26', '2021-09-09 08:27:26'),
(1655, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:32', '2021-09-09 08:27:32'),
(1656, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:32', '2021-09-09 08:27:32'),
(1657, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:36', '2021-09-09 08:27:36'),
(1658, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:39', '2021-09-09 08:27:39'),
(1659, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:40', '2021-09-09 08:27:40'),
(1660, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:41', '2021-09-09 08:27:41'),
(1661, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:48', '2021-09-09 08:27:48'),
(1662, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:48', '2021-09-09 08:27:48'),
(1663, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:53', '2021-09-09 08:27:53'),
(1664, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:56', '2021-09-09 08:27:56'),
(1665, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:57', '2021-09-09 08:27:57'),
(1666, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:27:59', '2021-09-09 08:27:59'),
(1667, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:04', '2021-09-09 08:28:04'),
(1668, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:04', '2021-09-09 08:28:04'),
(1669, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:12', '2021-09-09 08:28:12'),
(1670, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:16', '2021-09-09 08:28:16'),
(1671, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:17', '2021-09-09 08:28:17'),
(1672, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:22', '2021-09-09 08:28:22'),
(1673, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:27', '2021-09-09 08:28:27'),
(1674, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:27', '2021-09-09 08:28:27'),
(1675, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:33', '2021-09-09 08:28:33'),
(1676, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:42', '2021-09-09 08:28:42'),
(1677, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:42', '2021-09-09 08:28:42'),
(1678, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:47', '2021-09-09 08:28:47'),
(1679, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:50', '2021-09-09 08:28:50'),
(1680, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:50', '2021-09-09 08:28:50'),
(1681, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:52', '2021-09-09 08:28:52'),
(1682, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:58', '2021-09-09 08:28:58'),
(1683, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:28:58', '2021-09-09 08:28:58'),
(1684, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:29:00', '2021-09-09 08:29:00'),
(1685, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:29:07', '2021-09-09 08:29:07'),
(1686, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:29:07', '2021-09-09 08:29:07'),
(1687, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:29:12', '2021-09-09 08:29:12'),
(1688, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:29:15', '2021-09-09 08:29:15'),
(1689, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:29:15', '2021-09-09 08:29:15'),
(1690, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:29:20', '2021-09-09 08:29:20'),
(1691, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:30:34', '2021-09-09 08:30:34'),
(1692, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:30:34', '2021-09-09 08:30:34'),
(1693, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:30:36', '2021-09-09 08:30:36'),
(1694, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:30:42', '2021-09-09 08:30:42'),
(1695, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:30:42', '2021-09-09 08:30:42'),
(1696, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:30:44', '2021-09-09 08:30:44'),
(1697, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:30:50', '2021-09-09 08:30:50'),
(1698, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:30:50', '2021-09-09 08:30:50'),
(1699, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:30:52', '2021-09-09 08:30:52'),
(1700, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:30:58', '2021-09-09 08:30:58'),
(1701, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:30:59', '2021-09-09 08:30:59'),
(1702, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:01', '2021-09-09 08:31:01'),
(1703, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:06', '2021-09-09 08:31:06'),
(1704, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:06', '2021-09-09 08:31:06'),
(1705, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:08', '2021-09-09 08:31:08'),
(1706, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:13', '2021-09-09 08:31:13'),
(1707, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:14', '2021-09-09 08:31:14'),
(1708, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:15', '2021-09-09 08:31:15'),
(1709, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:22', '2021-09-09 08:31:22'),
(1710, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:22', '2021-09-09 08:31:22'),
(1711, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:24', '2021-09-09 08:31:24'),
(1712, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:30', '2021-09-09 08:31:30'),
(1713, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:30', '2021-09-09 08:31:30'),
(1714, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:32', '2021-09-09 08:31:32'),
(1715, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:37', '2021-09-09 08:31:37'),
(1716, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:37', '2021-09-09 08:31:37'),
(1717, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:39', '2021-09-09 08:31:39'),
(1718, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:44', '2021-09-09 08:31:44'),
(1719, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:44', '2021-09-09 08:31:44'),
(1720, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:46', '2021-09-09 08:31:46'),
(1721, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:51', '2021-09-09 08:31:51'),
(1722, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:52', '2021-09-09 08:31:52'),
(1723, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:54', '2021-09-09 08:31:54'),
(1724, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:31:59', '2021-09-09 08:31:59'),
(1725, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:00', '2021-09-09 08:32:00'),
(1726, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:01', '2021-09-09 08:32:01'),
(1727, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:06', '2021-09-09 08:32:06'),
(1728, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:07', '2021-09-09 08:32:07'),
(1729, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:09', '2021-09-09 08:32:09'),
(1730, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:14', '2021-09-09 08:32:14'),
(1731, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:14', '2021-09-09 08:32:14'),
(1732, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:16', '2021-09-09 08:32:16'),
(1733, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:22', '2021-09-09 08:32:22'),
(1734, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:22', '2021-09-09 08:32:22'),
(1735, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:24', '2021-09-09 08:32:24'),
(1736, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:32', '2021-09-09 08:32:32'),
(1737, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:32', '2021-09-09 08:32:32'),
(1738, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:34', '2021-09-09 08:32:34'),
(1739, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:42', '2021-09-09 08:32:42'),
(1740, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:43', '2021-09-09 08:32:43'),
(1741, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:44', '2021-09-09 08:32:44'),
(1742, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:51', '2021-09-09 08:32:51'),
(1743, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:51', '2021-09-09 08:32:51'),
(1744, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:53', '2021-09-09 08:32:53'),
(1745, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:58', '2021-09-09 08:32:58'),
(1746, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:32:59', '2021-09-09 08:32:59'),
(1747, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:00', '2021-09-09 08:33:00'),
(1748, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:05', '2021-09-09 08:33:05'),
(1749, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:05', '2021-09-09 08:33:05'),
(1750, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:07', '2021-09-09 08:33:07'),
(1751, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:12', '2021-09-09 08:33:12'),
(1752, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:12', '2021-09-09 08:33:12'),
(1753, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:14', '2021-09-09 08:33:14'),
(1754, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:19', '2021-09-09 08:33:19'),
(1755, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:19', '2021-09-09 08:33:19'),
(1756, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:21', '2021-09-09 08:33:21'),
(1757, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:26', '2021-09-09 08:33:26'),
(1758, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:26', '2021-09-09 08:33:26'),
(1759, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:28', '2021-09-09 08:33:28'),
(1760, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:34', '2021-09-09 08:33:34'),
(1761, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:34', '2021-09-09 08:33:34'),
(1762, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:36', '2021-09-09 08:33:36'),
(1763, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:41', '2021-09-09 08:33:41'),
(1764, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:41', '2021-09-09 08:33:41'),
(1765, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:43', '2021-09-09 08:33:43'),
(1766, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:50', '2021-09-09 08:33:50'),
(1767, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:50', '2021-09-09 08:33:50'),
(1768, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:52', '2021-09-09 08:33:52'),
(1769, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:57', '2021-09-09 08:33:57'),
(1770, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:58', '2021-09-09 08:33:58'),
(1771, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:33:59', '2021-09-09 08:33:59'),
(1772, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:34:09', '2021-09-09 08:34:09'),
(1773, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:34:09', '2021-09-09 08:34:09'),
(1774, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:34:12', '2021-09-09 08:34:12'),
(1775, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:34:23', '2021-09-09 08:34:23'),
(1776, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:34:24', '2021-09-09 08:34:24'),
(1777, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:34:26', '2021-09-09 08:34:26'),
(1778, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:34:34', '2021-09-09 08:34:34'),
(1779, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:34:34', '2021-09-09 08:34:34'),
(1780, 'http://agro.loc', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:35:59', '2021-09-09 08:35:59'),
(1781, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:39:35', '2021-09-09 08:39:35'),
(1782, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:39:51', '2021-09-09 08:39:51'),
(1783, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:39:54', '2021-09-09 08:39:54');
INSERT INTO `activities` (`id`, `url`, `method`, `ip`, `agent`, `user_id`, `created_at`, `updated_at`) VALUES
(1784, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:39:55', '2021-09-09 08:39:55'),
(1785, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:39:56', '2021-09-09 08:39:56'),
(1786, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:02', '2021-09-09 08:40:02'),
(1787, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:02', '2021-09-09 08:40:02'),
(1788, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:04', '2021-09-09 08:40:04'),
(1789, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:12', '2021-09-09 08:40:12'),
(1790, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:12', '2021-09-09 08:40:12'),
(1791, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:14', '2021-09-09 08:40:14'),
(1792, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:20', '2021-09-09 08:40:20'),
(1793, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:20', '2021-09-09 08:40:20'),
(1794, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:22', '2021-09-09 08:40:22'),
(1795, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:27', '2021-09-09 08:40:27'),
(1796, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:28', '2021-09-09 08:40:28'),
(1797, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:30', '2021-09-09 08:40:30'),
(1798, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:36', '2021-09-09 08:40:36'),
(1799, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:36', '2021-09-09 08:40:36'),
(1800, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:38', '2021-09-09 08:40:38'),
(1801, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:43', '2021-09-09 08:40:43'),
(1802, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:43', '2021-09-09 08:40:43'),
(1803, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:45', '2021-09-09 08:40:45'),
(1804, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:51', '2021-09-09 08:40:51'),
(1805, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:51', '2021-09-09 08:40:51'),
(1806, 'http://agro.loc/admin/gosts/create', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:53', '2021-09-09 08:40:53'),
(1807, 'http://agro.loc/admin/gosts', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:58', '2021-09-09 08:40:58'),
(1808, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:40:58', '2021-09-09 08:40:58'),
(1809, 'http://agro.loc/admin/gosts', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:41:05', '2021-09-09 08:41:05'),
(1810, 'http://agro.loc/admin', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36 OPR/73.0.3856.344', NULL, '2021-09-09 08:44:18', '2021-09-09 08:44:18'),
(1811, 'http://reestr.agro.uz', 'GET', '5.255.253.157', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', NULL, '2021-09-09 12:22:22', '2021-09-09 12:22:22');

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `id` int(10) UNSIGNED NOT NULL,
  `Raqami` int(11) DEFAULT NULL,
  `Sanasi` datetime DEFAULT NULL,
  `Statusi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FISH` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Manzili` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Telefon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `e_mail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `STIR` int(11) DEFAULT NULL,
  `Tashkilot_nomi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Tovar_turi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Texnika_nomi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TN_VED_kodi` int(11) DEFAULT NULL,
  `Kategoriyasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Turi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Rasmi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Invoys_raqami` int(11) DEFAULT NULL,
  `Invoys_sanasi` datetime DEFAULT NULL,
  `Fayl1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fayl2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fayl3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fayl4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `applying_for_testings`
--

CREATE TABLE `applying_for_testings` (
  `application_number` int(10) UNSIGNED NOT NULL,
  `date_of_issue` date DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `applicant_full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `STIR` int(11) DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `which_product_testing` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tech_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_tnved` int(11) DEFAULT NULL,
  `tech_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tech_category_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tech_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoys_number` int(11) DEFAULT NULL,
  `invoys_date` date DEFAULT NULL,
  `send_file_` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_file_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_file_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_file_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `apps`
--

CREATE TABLE `apps` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2021-08-21 00:56:38', '2021-08-21 00:56:38'),
(2, NULL, 1, 'Category 2', 'category-2', '2021-08-21 00:56:38', '2021-08-21 00:56:38'),
(3, 1, 1, 'sdafasdf', 'sdafasdf', '2021-08-30 04:18:19', '2021-08-30 04:18:19');

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE `contracts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `inn` int(11) DEFAULT NULL,
  `pinfl` int(11) DEFAULT NULL,
  `location` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contract_no` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expire_date` timestamp NULL DEFAULT NULL,
  `contract_price` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(59, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(60, 9, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(61, 9, 'price', 'text', 'Price', 0, 1, 1, 1, 1, 1, '{}', 3),
(62, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(63, 14, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(64, 14, 'inn', 'text', 'Inn', 0, 1, 1, 1, 1, 1, '{}', 3),
(65, 14, 'pinfl', 'text', 'Pinfl', 0, 1, 1, 1, 1, 1, '{}', 4),
(66, 14, 'location', 'text', 'Location', 0, 1, 1, 1, 1, 1, '{}', 5),
(67, 14, 'phone', 'text', 'Phone', 0, 1, 1, 1, 1, 1, '{}', 6),
(68, 14, 'contract_no', 'text', 'Contract No', 0, 1, 1, 1, 1, 1, '{}', 7),
(69, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(70, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(71, 14, 'expire_date', 'timestamp', 'Expire Date', 0, 1, 1, 1, 1, 1, '{}', 10),
(72, 14, 'contract_price', 'text', 'Contract Price', 0, 1, 1, 1, 1, 1, '{}', 11),
(73, 14, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 12),
(74, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(75, 18, 'model', 'text', 'Model', 1, 1, 1, 1, 1, 1, '{}', 2),
(76, 18, 'manufacture_date', 'timestamp', 'Manufacture Date', 1, 1, 1, 1, 1, 1, '{}', 3),
(77, 18, 'type', 'text', 'Type', 1, 1, 1, 1, 1, 1, '{}', 4),
(78, 18, 'power_class', 'text', 'Power Class', 1, 1, 1, 1, 1, 1, '{}', 5),
(79, 18, 'power', 'text', 'Power', 1, 1, 1, 1, 1, 1, '{}', 6),
(80, 18, 'engine_model', 'text', 'Engine Model', 1, 1, 1, 1, 1, 1, '{}', 7),
(81, 18, 'turns_min', 'text', 'Turns Min', 1, 1, 1, 1, 1, 1, '{}', 8),
(82, 18, 'vom_turns_min', 'text', 'Vom Turns Min', 1, 1, 1, 1, 1, 1, '{}', 9),
(83, 18, 'suspended_load_capacity', 'text', 'Suspended Load Capacity', 1, 1, 1, 1, 1, 1, '{}', 10),
(84, 18, 'walking_section', 'text', 'Walking Section', 1, 1, 1, 1, 1, 1, '{}', 11),
(85, 18, 'speed', 'text', 'Speed', 1, 1, 1, 1, 1, 1, '{}', 12),
(86, 18, 'start_activation', 'text', 'Start Activation', 1, 1, 1, 1, 1, 1, '{}', 13),
(87, 18, 'const_work_width', 'text', 'Const Work Width', 1, 1, 1, 1, 1, 1, '{}', 14),
(88, 18, 'dimensions', 'text', 'Dimensions', 1, 1, 1, 1, 1, 1, '{}', 15),
(89, 18, 'ground_clearance', 'text', 'Ground Clearance', 0, 1, 1, 1, 1, 1, '{}', 16),
(90, 18, 'usage_load', 'text', 'Usage Load', 0, 1, 1, 1, 1, 1, '{}', 17),
(91, 18, 'wheel_width', 'text', 'Wheel Width', 0, 1, 1, 1, 1, 1, '{}', 18),
(92, 18, 'bunker_capacity', 'text', 'Bunker Capacity', 0, 1, 1, 1, 1, 1, '{}', 19),
(93, 18, 'resource_capacity', 'text', 'Resource Capacity', 0, 1, 1, 1, 1, 1, '{}', 20),
(94, 18, 'processing_depth', 'text', 'Processing Depth', 0, 1, 1, 1, 1, 1, '{}', 21),
(95, 18, 'row_spacing', 'text', 'Row Spacing', 0, 1, 1, 1, 1, 1, '{}', 22),
(96, 18, 'processing_type', 'text', 'Processing Type', 0, 1, 1, 1, 1, 1, '{}', 23),
(97, 18, 'processing_movement', 'text', 'Processing Movement', 0, 1, 1, 1, 1, 1, '{}', 24),
(98, 18, 'processing_width', 'text', 'Processing Width', 0, 1, 1, 1, 1, 1, '{}', 25),
(99, 18, 'processing_number_bodies', 'text', 'Processing Number Bodies', 0, 1, 1, 1, 1, 1, '{}', 26),
(100, 18, 'grinding_drum', 'text', 'Grinding Drum', 0, 1, 1, 1, 1, 1, '{}', 27),
(101, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(102, 19, 'url', 'text', 'Url', 0, 1, 1, 1, 1, 1, '{}', 2),
(103, 19, 'method', 'text', 'Method', 0, 1, 1, 1, 1, 1, '{}', 3),
(104, 19, 'ip', 'text', 'Ip', 0, 1, 1, 1, 1, 1, '{}', 4),
(105, 19, 'agent', 'text', 'Agent', 0, 1, 1, 1, 1, 1, '{}', 5),
(106, 19, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(107, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(108, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(109, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(110, 20, 'Raqami', 'number', 'Raqami', 0, 1, 1, 1, 1, 1, '{}', 2),
(111, 20, 'Sanasi', 'date', 'Sanasi', 0, 1, 1, 1, 1, 1, '{}', 3),
(112, 20, 'Statusi', 'checkbox', 'Statusi', 0, 1, 1, 1, 1, 1, '{}', 4),
(113, 20, 'FISH', 'text', 'FISH', 0, 1, 1, 1, 1, 1, '{}', 5),
(114, 20, 'Manzili', 'text_area', 'Manzili', 0, 1, 1, 1, 1, 1, '{}', 6),
(115, 20, 'Telefon', 'text', 'Telefon', 0, 1, 1, 1, 1, 1, '{}', 7),
(116, 20, 'e_mail', 'text', 'E Mail', 0, 1, 1, 1, 1, 1, '{}', 8),
(117, 20, 'STIR', 'number', 'STIR', 0, 1, 1, 1, 1, 1, '{}', 9),
(118, 20, 'Tashkilot_nomi', 'text', 'Tashkilot Nomi', 0, 1, 1, 1, 1, 1, '{}', 10),
(119, 20, 'Tovar_turi', 'text', 'Tovar Turi', 0, 1, 1, 1, 1, 1, '{}', 11),
(120, 20, 'Texnika_nomi', 'text', 'Texnika Nomi', 0, 1, 1, 1, 1, 1, '{}', 12),
(121, 20, 'TN_VED_kodi', 'number', 'TN VED Kodi', 0, 1, 1, 1, 1, 1, '{}', 13),
(122, 20, 'Kategoriyasi', 'select_dropdown', 'Kategoriyasi', 0, 1, 1, 1, 1, 1, '{}', 14),
(123, 20, 'Turi', 'select_dropdown', 'Turi', 0, 1, 1, 1, 1, 1, '{}', 15),
(124, 20, 'Rasmi', 'image', 'Rasmi', 0, 1, 1, 1, 1, 1, '{}', 16),
(125, 20, 'Invoys_raqami', 'number', 'Invoys Raqami', 0, 1, 1, 1, 1, 1, '{}', 17),
(126, 20, 'Invoys_sanasi', 'date', 'Invoys Sanasi', 0, 1, 1, 1, 1, 1, '{}', 18),
(127, 20, 'Fayl1', 'file', 'Fayl1', 0, 1, 1, 1, 1, 1, '{}', 19),
(128, 20, 'Fayl2', 'file', 'Fayl2', 0, 1, 1, 1, 1, 1, '{}', 20),
(129, 20, 'Fayl3', 'file', 'Fayl3', 0, 1, 1, 1, 1, 1, '{}', 21),
(130, 20, 'Fayl4', 'file', 'Fayl4', 0, 1, 1, 1, 1, 1, '{}', 22),
(131, 20, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 23),
(132, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 24),
(137, 22, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(138, 22, 'title_uz', 'text', 'Title Uz', 0, 1, 1, 1, 1, 1, '{}', 2),
(139, 22, 'title_en', 'text', 'Title En', 0, 1, 1, 1, 1, 1, '{}', 3),
(140, 22, 'title_ru', 'text', 'Title Ru', 0, 1, 1, 1, 1, 1, '{}', 4),
(141, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(142, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(143, 24, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(144, 24, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(145, 24, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(146, 24, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(147, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(152, 28, 'application_number', 'number', 'Application Number', 1, 0, 0, 0, 0, 0, '{}', 1),
(153, 28, 'date_of_issue', 'date', 'Date Of Issue', 0, 1, 1, 1, 1, 1, '{}', 2),
(154, 28, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 3),
(155, 28, 'applicant_full_name', 'text', 'Applicant Full Name', 0, 1, 1, 1, 1, 1, '{}', 4),
(156, 28, 'address', 'text', 'Address', 0, 1, 1, 1, 1, 1, '{}', 5),
(157, 28, 'phone_number', 'number', 'Phone Number', 0, 1, 1, 1, 1, 1, '{}', 6),
(158, 28, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 7),
(159, 28, 'STIR', 'number', 'STIR', 0, 1, 1, 1, 1, 1, '{}', 8),
(160, 28, 'company_name', 'text', 'Company Name', 0, 1, 1, 1, 1, 1, '{}', 9),
(161, 28, 'which_product_testing', 'text', 'Which Product Testing', 0, 1, 1, 1, 1, 1, '{}', 10),
(162, 28, 'tech_name', 'text', 'Tech Name', 0, 1, 1, 1, 1, 1, '{}', 11),
(163, 28, 'code_tnved', 'number', 'Code Tnved', 0, 1, 1, 1, 1, 1, '{}', 12),
(164, 28, 'tech_category', 'text', 'Tech Category', 0, 1, 1, 1, 1, 1, '{}', 13),
(165, 28, 'tech_category_type', 'text', 'Tech Category Type', 0, 1, 1, 1, 1, 1, '{}', 14),
(166, 28, 'tech_photo', 'image', 'Tech Photo', 0, 1, 1, 1, 1, 1, '{}', 15),
(167, 28, 'invoys_number', 'number', 'Invoys Number', 0, 1, 1, 1, 1, 1, '{}', 16),
(168, 28, 'invoys_date', 'date', 'Invoys Date', 0, 1, 1, 1, 1, 1, '{}', 17),
(169, 28, 'send_file_', 'text', 'Send File', 0, 1, 1, 1, 1, 1, '{}', 18),
(170, 28, 'send_file_2', 'text', 'Send File 2', 0, 1, 1, 1, 1, 1, '{}', 19),
(171, 28, 'send_file_3', 'text', 'Send File 3', 0, 1, 1, 1, 1, 1, '{}', 20),
(172, 28, 'send_file_4', 'text', 'Send File 4', 0, 1, 1, 1, 1, 1, '{}', 21),
(173, 28, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 22),
(174, 28, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 23),
(180, 30, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(181, 30, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(182, 30, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(183, 30, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(190, 32, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(191, 32, 'tnved_id', 'number', 'Tnved Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(192, 32, 'parametr_id', 'number', 'Parametr Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(193, 32, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(194, 32, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(195, 32, 'labarotory_id', 'number', 'Labarotory Id', 0, 1, 1, 1, 1, 1, '{}', 6);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2021-08-21 00:56:05', '2021-08-21 00:56:05'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-08-21 00:56:05', '2021-08-21 00:56:05'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-08-21 00:56:05', '2021-08-21 00:56:05'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2021-08-21 00:56:38', '2021-08-21 00:56:38'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2021-08-21 00:56:38', '2021-08-21 00:56:38'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(9, 'products', 'products', 'Product', 'Products', NULL, 'App\\Models\\Products', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-08-23 08:02:31', '2021-08-23 08:02:31'),
(14, 'contracts', 'contracts', 'Contract', 'Contracts', NULL, 'App\\Models\\Contract', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-08-30 04:46:01', '2021-08-30 04:46:01'),
(18, 'vehicles', 'vehicles', 'Vehicle', 'Vehicles', NULL, 'App\\Models\\Vehicle', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-08-30 05:11:02', '2021-08-30 05:11:02'),
(19, 'activities', 'activities', 'Activity', 'Activities', NULL, 'App\\Models\\Activity', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-08-30 05:31:22', '2021-08-30 05:31:22'),
(20, 'applications', 'applications', 'Application', 'Applications', NULL, 'App\\Models\\Application', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-09-07 02:55:37', '2021-09-07 02:56:47'),
(22, 'navbar_menus', 'navbar-menus', 'Navbar Menu', 'Navbar Menus', NULL, 'App\\Models\\Navbarmenu', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-09-07 05:36:21', '2021-09-07 05:36:21'),
(24, 'gosts', 'gosts', 'Gost', 'Gosts', NULL, 'App\\Models\\Gost', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-09-09 07:01:21', '2021-09-09 07:04:10'),
(28, 'applying_for_testings', 'applying-for-testings', 'Applying For Testing', 'Applying For Testings', NULL, 'App\\Models\\ApplyingForTesting', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-09-09 07:21:21', '2021-09-09 07:21:21'),
(30, 'tnved_codes', 'tnved-codes', 'Tnved Code', 'Tnved Codes', NULL, 'App\\Models\\TnvedCode', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-09-09 05:57:31', '2021-09-09 05:57:31'),
(32, 'procedures', 'procedures', 'Procedure', 'Procedures', NULL, 'App\\Models\\Procedure', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-09-09 08:04:34', '2021-09-09 08:06:33');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `gosts`
--

CREATE TABLE `gosts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gosts`
--

INSERT INTO `gosts` (`id`, `name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'ГОСТ 12.2.002-91', NULL, '2021-09-09 07:04:26', '2021-09-09 07:04:26'),
(2, 'ГОСТ 12.2.002.3-91', NULL, '2021-09-09 07:04:38', '2021-09-09 07:04:38'),
(3, 'ГОСТ 12.2.102-2013', NULL, '2021-09-09 07:04:51', '2021-09-09 07:04:51'),
(4, 'ГОСТ 12.4-095-80', NULL, '2021-09-09 07:05:02', '2021-09-09 07:05:02'),
(5, 'ГОСТ 17.2.2.02-98', NULL, '2021-09-09 07:05:12', '2021-09-09 07:05:12'),
(6, 'ГОСТ ISO 520-2014', NULL, '2021-09-09 07:05:23', '2021-09-09 07:05:23'),
(7, 'O\'z DSt  592:2008', NULL, '2021-09-09 07:05:33', '2021-09-09 07:05:33'),
(8, 'ГОСТ ИСО 5353-2003 (ISO 5353:1995, IDT)', NULL, '2021-09-09 07:05:43', '2021-09-09 07:05:43'),
(9, 'ГОСТ ISO 6016-2014', NULL, '2021-09-09 07:05:52', '2021-09-09 07:05:52'),
(10, 'ГОСТ 7057-2001', NULL, '2021-09-09 07:06:03', '2021-09-09 07:06:03'),
(11, 'ГОСТ ИСО 8909-3-2004', NULL, '2021-09-09 07:06:14', '2021-09-09 07:06:14'),
(12, 'ГОСТ 10842-89', NULL, '2021-09-09 07:06:32', '2021-09-09 07:06:32'),
(13, 'ГОСТ ИСО 11112-2000', NULL, '2021-09-09 07:06:49', '2021-09-09 07:06:49'),
(14, 'ГОСТ 12042-80', NULL, '2021-09-09 07:07:08', '2021-09-09 07:07:08'),
(15, 'ГОСТ 14107-75', NULL, '2021-09-09 07:07:39', '2021-09-09 07:07:39'),
(16, 'ГОСТ ИСО 14269.5-2003', NULL, '2021-09-09 07:07:49', '2021-09-09 07:07:49'),
(17, 'ГОСТ 17257-87', NULL, '2021-09-09 07:07:59', '2021-09-09 07:07:59'),
(18, 'ГОСТ 18509-88', NULL, '2021-09-09 07:08:23', '2021-09-09 07:08:23'),
(19, 'ГОСТ 20915-2011', NULL, '2021-09-09 07:08:33', '2021-09-09 07:08:33'),
(20, 'ГОСТ 21820.3-76', NULL, '2021-09-09 07:13:31', '2021-09-09 07:13:31'),
(21, 'ГОСТ 24055-2016', NULL, '2021-09-09 07:13:39', '2021-09-09 07:13:39'),
(22, 'ГОСТ 24059-2017', NULL, '2021-09-09 07:13:48', '2021-09-09 07:13:48'),
(23, 'ГОСТ 26025-83', NULL, '2021-09-09 07:13:58', '2021-09-09 07:13:58'),
(24, 'ГОСТ 26953-86', NULL, '2021-09-09 07:14:10', '2021-09-09 07:14:10'),
(25, 'ГОСТ 26954-86', NULL, '2021-09-09 07:14:19', '2021-09-09 07:14:19'),
(26, 'ГОСТ 27256-87 (ИСО 7128-83)', NULL, '2021-09-09 07:14:29', '2021-09-09 07:14:29'),
(27, 'ГОСТ 27257-87 (ИСО 7457-83)', NULL, '2021-09-09 07:14:39', '2021-09-09 07:14:39'),
(28, 'ГОСТ 27927-88', NULL, '2021-09-09 07:14:50', '2021-09-09 07:14:50'),
(29, 'ГОСТ 28287-89', NULL, '2021-09-09 07:15:00', '2021-09-09 07:15:00'),
(30, 'ГОСТ 28301-2015', NULL, '2021-09-09 07:15:10', '2021-09-09 07:15:10'),
(31, 'ГОСТ 28306-2018', NULL, '2021-09-09 07:15:19', '2021-09-09 07:15:19'),
(32, 'ГОСТ 28307-2013', NULL, '2021-09-09 07:15:32', '2021-09-09 07:15:32'),
(33, 'ГОСТ 28713-2018', NULL, '2021-09-09 07:15:41', '2021-09-09 07:15:41'),
(34, 'ГОСТ 28714-2007', NULL, '2021-09-09 07:15:52', '2021-09-09 07:15:52'),
(35, 'ГОСТ 28718-2016', NULL, '2021-09-09 07:16:02', '2021-09-09 07:16:02'),
(36, 'ГОСТ 29295-92 (ИСО 9246-88)', NULL, '2021-09-09 07:16:11', '2021-09-09 07:16:11'),
(37, 'ГОСТ 30483-97', NULL, '2021-09-09 07:16:19', '2021-09-09 07:16:19'),
(38, 'ГОСТ 30746-2001 ( ИСО 789-2:1993)', NULL, '2021-09-09 07:16:28', '2021-09-09 07:16:28'),
(39, 'ГОСТ 30747-2001 ( ИСО 789-1:1990)', NULL, '2021-09-09 07:16:37', '2021-09-09 07:16:37'),
(40, 'ГОСТ 30752-2001', NULL, '2021-09-09 07:16:45', '2021-09-09 07:16:45'),
(41, 'ГОСТ 31345-2017', NULL, '2021-09-09 07:16:53', '2021-09-09 07:16:53'),
(42, 'O’z DSt 3225:2017', NULL, '2021-09-09 07:17:02', '2021-09-09 07:17:02'),
(43, 'O’zDSt 35.13-2010 (Правила ЕЭК ООН №13)', NULL, '2021-09-09 07:17:11', '2021-09-09 07:17:11'),
(44, 'O’z DSt 3090:2016', NULL, '2021-09-09 07:17:19', '2021-09-09 07:17:19'),
(45, 'O’z DSt 3111-2016', NULL, '2021-09-09 07:17:27', '2021-09-09 07:17:27'),
(46, 'O’z DSt 3193:2017', NULL, '2021-09-09 07:17:35', '2021-09-09 07:17:35'),
(47, 'O’z DSt 3201:2017', NULL, '2021-09-09 07:17:43', '2021-09-09 07:17:43'),
(48, 'O’z DSt 3202:2017', NULL, '2021-09-09 07:17:56', '2021-09-09 07:17:56'),
(49, 'O’z DSt 3203:2017', NULL, '2021-09-09 07:18:13', '2021-09-09 07:18:13'),
(50, 'O’z DSt 3204:2017', NULL, '2021-09-09 07:18:21', '2021-09-09 07:18:21'),
(51, 'O’z DSt 3206:2017', NULL, '2021-09-09 07:18:30', '2021-09-09 07:18:30'),
(52, 'O’z DSt  3211:2017', NULL, '2021-09-09 07:18:38', '2021-09-09 07:18:38'),
(53, 'O’z DSt 3225:2017', NULL, '2021-09-09 07:18:45', '2021-09-09 07:18:45'),
(54, 'O’z DSt 3355:2018', NULL, '2021-09-09 07:18:54', '2021-09-09 07:18:54'),
(55, 'O’z DSt 3412:2019', NULL, '2021-09-09 07:19:01', '2021-09-09 07:19:01'),
(56, '4254-1-2013', NULL, '2021-09-09 08:20:08', '2021-09-09 08:20:08'),
(57, 'O’zDSt 35.13-2010', NULL, '2021-09-09 08:20:15', '2021-09-09 08:20:15'),
(58, 'ГОСТ 10000-75', NULL, '2021-09-09 08:20:23', '2021-09-09 08:20:23'),
(59, 'ГОСТ 1114-84', NULL, '2021-09-09 08:20:33', '2021-09-09 08:20:33'),
(60, 'ГОСТ 12.1.003-2014', NULL, '2021-09-09 08:20:47', '2021-09-09 08:20:47'),
(61, 'ГОСТ 12.1.003-91', NULL, '2021-09-09 08:20:54', '2021-09-09 08:20:54'),
(62, 'ГОСТ 12.1.012-2001', NULL, '2021-09-09 08:21:02', '2021-09-09 08:21:02'),
(63, 'ГОСТ 12.1.012-2004', NULL, '2021-09-09 08:21:09', '2021-09-09 08:21:09'),
(64, 'ГОСТ 12.2.003-91', NULL, '2021-09-09 08:21:16', '2021-09-09 08:21:16'),
(65, 'ГОСТ 12.2.003-91', NULL, '2021-09-09 08:21:24', '2021-09-09 08:21:24'),
(66, 'ГОСТ 12.2.011-2012', NULL, '2021-09-09 08:21:31', '2021-09-09 08:21:31'),
(67, 'ГОСТ 12.2.019-2015', NULL, '2021-09-09 08:21:38', '2021-09-09 08:21:38'),
(68, 'ГОСТ 12.2.042-2013', NULL, '2021-09-09 08:21:45', '2021-09-09 08:21:45'),
(69, 'ГОСТ 12.2.042-91', NULL, '2021-09-09 08:22:39', '2021-09-09 08:22:39'),
(70, 'ГОСТ 12.2.062-81', NULL, '2021-09-09 08:22:47', '2021-09-09 08:22:47'),
(71, 'ГОСТ 12.2.111-85', NULL, '2021-09-09 08:22:54', '2021-09-09 08:22:54'),
(72, 'ГОСТ 12.2.120-2015', NULL, '2021-09-09 08:23:01', '2021-09-09 08:23:01'),
(73, 'ГОСТ 12.2.121-2013', NULL, '2021-09-09 08:23:09', '2021-09-09 08:23:09'),
(74, 'ГОСТ 12.2.140-2004', NULL, '2021-09-09 08:23:17', '2021-09-09 08:23:17'),
(75, 'ГОСТ 12.4.026-2015', NULL, '2021-09-09 08:23:24', '2021-09-09 08:23:24'),
(76, 'ГОСТ 14192-96', NULL, '2021-09-09 08:23:32', '2021-09-09 08:23:32'),
(77, 'ГОСТ 17.2.2.02-98', NULL, '2021-09-09 08:23:40', '2021-09-09 08:23:40'),
(78, 'ГОСТ 17.2.2.05-97', NULL, '2021-09-09 08:23:47', '2021-09-09 08:23:47'),
(79, 'ГОСТ 19677-87', NULL, '2021-09-09 08:23:55', '2021-09-09 08:23:55'),
(80, 'ГОСТ 20000-88', NULL, '2021-09-09 08:24:02', '2021-09-09 08:24:02'),
(81, 'ГОСТ 20062-96', NULL, '2021-09-09 08:24:10', '2021-09-09 08:24:10'),
(82, 'ГОСТ 21753-76', NULL, '2021-09-09 08:24:18', '2021-09-09 08:24:18'),
(83, 'ГОСТ 22587-91', NULL, '2021-09-09 08:24:25', '2021-09-09 08:24:25'),
(84, 'ГОСТ 23389-78', NULL, '2021-09-09 08:24:33', '2021-09-09 08:24:33'),
(85, 'ГОСТ 23982-85', NULL, '2021-09-09 08:24:40', '2021-09-09 08:24:40'),
(86, 'ГОСТ 26336-97', NULL, '2021-09-09 08:24:47', '2021-09-09 08:24:47'),
(87, 'ГОСТ 26738-91', NULL, '2021-09-09 08:24:55', '2021-09-09 08:24:55'),
(88, 'ГОСТ 27258-87', NULL, '2021-09-09 08:25:03', '2021-09-09 08:25:03'),
(89, 'ГОСТ 27310-87', NULL, '2021-09-09 08:25:10', '2021-09-09 08:25:10'),
(90, 'ГОСТ 27434-87', NULL, '2021-09-09 08:25:17', '2021-09-09 08:25:17'),
(91, 'ГОСТ 2867-2002', NULL, '2021-09-09 08:25:25', '2021-09-09 08:25:25'),
(92, 'ГОСТ 28708-2001', NULL, '2021-09-09 08:25:34', '2021-09-09 08:25:34'),
(93, 'ГОСТ 4254-1-2013', NULL, '2021-09-09 08:25:42', '2021-09-09 08:25:42'),
(94, 'ГОСТ 6964-72', NULL, '2021-09-09 08:25:51', '2021-09-09 08:25:51'),
(95, 'ГОСТ 7496-93', NULL, '2021-09-09 08:25:58', '2021-09-09 08:25:58'),
(96, 'ГОСТ 8769-75', NULL, '2021-09-09 08:26:06', '2021-09-09 08:26:06'),
(97, 'ГОСТ ЕН 632-2003', NULL, '2021-09-09 08:26:14', '2021-09-09 08:26:14'),
(98, 'ГОСТ ИСО 26322-1-2012', NULL, '2021-09-09 08:26:21', '2021-09-09 08:26:21'),
(99, 'ГОСТ ИСО 26322-2-2012', NULL, '2021-09-09 08:26:28', '2021-09-09 08:26:28'),
(100, 'ГОСТ ИСО 2867-2002', NULL, '2021-09-09 08:26:36', '2021-09-09 08:26:36'),
(101, 'ГОСТ ИСО 4252-2005', NULL, '2021-09-09 08:26:58', '2021-09-09 08:26:58'),
(102, 'ГОСТ ИСО 4252-2015', NULL, '2021-09-09 08:27:09', '2021-09-09 08:27:09'),
(103, 'ГОСТ ИСО 4254-1-2013', NULL, '2021-09-09 08:27:25', '2021-09-09 08:27:25'),
(104, 'ГОСТ ИСО 4254-6-2012.', NULL, '2021-09-09 08:27:32', '2021-09-09 08:27:32'),
(105, 'ГОСТ ИСО 4254-9-2012', NULL, '2021-09-09 08:27:39', '2021-09-09 08:27:39'),
(106, 'ГОСТ ИСО 5353-2003', NULL, '2021-09-09 08:27:48', '2021-09-09 08:27:48'),
(107, 'ГОСТ-10000-75', NULL, '2021-09-09 08:27:57', '2021-09-09 08:27:57'),
(108, 'ГОСТ12.2.003-91', NULL, '2021-09-09 08:28:04', '2021-09-09 08:28:04'),
(109, 'ГОСТ-12.4.026', NULL, '2021-09-09 08:28:16', '2021-09-09 08:28:16'),
(110, 'ГСОТ 12.1.003-2014', NULL, '2021-09-09 08:28:27', '2021-09-09 08:28:27'),
(111, 'O’z DSt 35.71-2013 (Правила ЕЭК ООН №71)', NULL, '2021-09-09 08:28:42', '2021-09-09 08:28:42'),
(112, 'O’z DSt 35.86-2011 (Правила ЕЭК ООН № 86)', NULL, '2021-09-09 08:28:50', '2021-09-09 08:28:50'),
(113, 'O’z DSt 35.86-211 (Правила ЕЭК ООН № 86)', NULL, '2021-09-09 08:28:58', '2021-09-09 08:28:58'),
(114, 'UzTR.80-006:2016', NULL, '2021-09-09 08:29:07', '2021-09-09 08:29:07'),
(115, 'ГОСТ 10000-75', NULL, '2021-09-09 08:29:15', '2021-09-09 08:29:15'),
(116, 'ГОСТ 12.1.003-2014', NULL, '2021-09-09 08:30:34', '2021-09-09 08:30:34'),
(117, 'ГОСТ 12.1.012-2004', NULL, '2021-09-09 08:30:42', '2021-09-09 08:30:42'),
(118, 'ГОСТ 12.2.003-91', NULL, '2021-09-09 08:30:50', '2021-09-09 08:30:50'),
(119, 'ГОСТ 12.2.003-91 ГОСТ 12.4.026-2015', NULL, '2021-09-09 08:30:59', '2021-09-09 08:30:59'),
(120, 'ГОСТ 12.2.011-2012', NULL, '2021-09-09 08:31:06', '2021-09-09 08:31:06'),
(121, 'ГОСТ 12.2.019-2015', NULL, '2021-09-09 08:31:13', '2021-09-09 08:31:13'),
(122, 'ГОСТ 12.2.042-2013', NULL, '2021-09-09 08:31:22', '2021-09-09 08:31:22'),
(123, 'ГОСТ 12.2.042-91', NULL, '2021-09-09 08:31:30', '2021-09-09 08:31:30'),
(124, 'ГОСТ 12.2.062-81', NULL, '2021-09-09 08:31:37', '2021-09-09 08:31:37'),
(125, 'ГОСТ 12.2.111-85', NULL, '2021-09-09 08:31:44', '2021-09-09 08:31:44'),
(126, 'ГОСТ 12.2.111-85 (Правила ЕЭК ООН №13)', NULL, '2021-09-09 08:31:51', '2021-09-09 08:31:51'),
(127, 'ГОСТ 12.2.120-2015', NULL, '2021-09-09 08:31:59', '2021-09-09 08:31:59'),
(128, 'ГОСТ 12.2.120-2015)', NULL, '2021-09-09 08:32:07', '2021-09-09 08:32:07'),
(129, 'ГОСТ 12.2.121-2013', NULL, '2021-09-09 08:32:14', '2021-09-09 08:32:14'),
(130, 'ГОСТ 12.4.026-2015', NULL, '2021-09-09 08:32:22', '2021-09-09 08:32:22'),
(131, 'ГОСТ 14192-96', NULL, '2021-09-09 08:32:32', '2021-09-09 08:32:32'),
(132, 'ГОСТ 17.2.2.01-84', NULL, '2021-09-09 08:32:42', '2021-09-09 08:32:42'),
(133, 'ГОСТ 17.2.2.02-98', NULL, '2021-09-09 08:32:51', '2021-09-09 08:32:51'),
(134, 'ГОСТ 17.2.2.05-97', NULL, '2021-09-09 08:32:58', '2021-09-09 08:32:58'),
(135, 'ГОСТ 19677-87', NULL, '2021-09-09 08:33:05', '2021-09-09 08:33:05'),
(136, 'ГОСТ 20000-88', NULL, '2021-09-09 08:33:12', '2021-09-09 08:33:12'),
(137, 'ГОСТ 21753-76', NULL, '2021-09-09 08:33:19', '2021-09-09 08:33:19'),
(138, 'ГОСТ 22999-93', NULL, '2021-09-09 08:33:26', '2021-09-09 08:33:26'),
(139, 'ГОСТ 23074-85', NULL, '2021-09-09 08:33:34', '2021-09-09 08:33:34'),
(140, 'ГОСТ 26336-97', NULL, '2021-09-09 08:33:41', '2021-09-09 08:33:41'),
(141, 'ГОСТ 26711-89', NULL, '2021-09-09 08:33:50', '2021-09-09 08:33:50'),
(142, 'ГОСТ 27258-87', NULL, '2021-09-09 08:33:57', '2021-09-09 08:33:57'),
(143, 'ГОСТ 28516-90', NULL, '2021-09-09 08:34:09', '2021-09-09 08:34:09'),
(144, 'ГОСТ 30067-93', NULL, '2021-09-09 08:34:23', '2021-09-09 08:34:23'),
(145, 'ГОСТ 6964-72', NULL, '2021-09-09 08:34:34', '2021-09-09 08:34:34'),
(146, 'ГОСТ 8769-75', NULL, '2021-09-09 08:39:54', '2021-09-09 08:39:54'),
(147, 'ГОСТ ИСО 26322-1-2012', NULL, '2021-09-09 08:40:02', '2021-09-09 08:40:02'),
(148, 'ГОСТ ИСО 26322-2-2012', NULL, '2021-09-09 08:40:12', '2021-09-09 08:40:12'),
(149, 'ГОСТ ИСО 2867-2002', NULL, '2021-09-09 08:40:20', '2021-09-09 08:40:20'),
(150, 'ГОСТ ИСО 3776-1-2012', NULL, '2021-09-09 08:40:27', '2021-09-09 08:40:27'),
(151, 'ГОСТ ИСО 4252-7-2005', NULL, '2021-09-09 08:40:36', '2021-09-09 08:40:36'),
(152, 'ГОСТ ИСО 4253-2005', NULL, '2021-09-09 08:40:43', '2021-09-09 08:40:43'),
(153, 'ГОСТ ИСО 4254-1-2013', NULL, '2021-09-09 08:40:51', '2021-09-09 08:40:51'),
(154, 'ГОСТ ИСО 5353-2003', NULL, '2021-09-09 08:40:58', '2021-09-09 08:40:58');

-- --------------------------------------------------------

--
-- Table structure for table `labaratories`
--

CREATE TABLE `labaratories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `inn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-08-21 00:56:06', '2021-08-21 00:56:06'),
(2, 'site_uz', '2021-09-07 03:56:04', '2021-09-07 05:59:59'),
(3, 'site_ru', '2021-09-07 06:03:39', '2021-09-07 06:03:39'),
(4, 'site_en', '2021-09-07 06:06:19', '2021-09-07 06:06:19');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-08-21 00:56:06', '2021-08-21 00:56:06', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2021-08-21 00:56:06', '2021-08-29 13:18:25', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2021-08-21 00:56:06', '2021-08-21 00:56:06', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2021-08-21 00:56:06', '2021-08-21 00:56:06', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2021-08-21 00:56:06', '2021-08-30 05:33:12', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2021-08-21 00:56:06', '2021-08-29 13:18:36', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2021-08-21 00:56:07', '2021-08-29 13:18:36', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2021-08-21 00:56:07', '2021-08-29 13:18:36', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2021-08-21 00:56:07', '2021-08-29 13:18:36', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 10, '2021-08-21 00:56:07', '2021-08-30 05:33:12', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2021-08-21 00:56:08', '2021-08-30 05:33:12', 'voyager.hooks', NULL),
(12, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 7, '2021-08-21 00:56:38', '2021-08-29 13:18:25', 'voyager.categories.index', NULL),
(13, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 5, '2021-08-21 00:56:38', '2021-08-29 13:18:25', 'voyager.posts.index', NULL),
(14, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 6, '2021-08-21 00:56:39', '2021-08-29 13:18:25', 'voyager.pages.index', NULL),
(16, 1, 'Products', '', '_self', NULL, NULL, NULL, 11, '2021-08-23 08:02:31', '2021-08-30 05:33:12', 'voyager.products.index', NULL),
(17, 1, 'Contracts', '', '_self', NULL, NULL, NULL, 12, '2021-08-30 04:46:01', '2021-08-30 05:33:12', 'voyager.contracts.index', NULL),
(18, 1, 'Vehicles', '', '_self', NULL, NULL, NULL, 13, '2021-08-30 05:11:02', '2021-08-30 05:33:12', 'voyager.vehicles.index', NULL),
(19, 1, 'Activities', '', '_self', 'voyager-window-list', '#000000', NULL, 8, '2021-08-30 05:31:22', '2021-08-30 05:34:24', 'voyager.activities.index', 'null'),
(20, 1, 'Applications', '', '_self', NULL, NULL, NULL, 14, '2021-09-07 02:55:37', '2021-09-07 02:55:37', 'voyager.applications.index', NULL),
(22, 1, 'Navbar Menus', '', '_self', NULL, NULL, NULL, 15, '2021-09-07 05:36:21', '2021-09-07 05:36:21', 'voyager.navbar-menus.index', NULL),
(23, 2, 'BOG’LANISH', '', '_self', NULL, '#000000', NULL, 7, '2021-09-07 06:00:28', '2021-09-07 06:03:37', NULL, ''),
(24, 2, 'KO’P UCHRAYDIGAN SAVOLLAR', '', '_self', NULL, '#000000', NULL, 6, '2021-09-07 06:00:43', '2021-09-07 06:03:37', NULL, ''),
(25, 2, 'BOSH SAHIFA', '', '_self', NULL, '#000000', NULL, 1, '2021-09-07 06:01:07', '2021-09-07 06:03:30', NULL, ''),
(26, 2, 'SINOVLAR', '', '_self', NULL, '#000000', NULL, 2, '2021-09-07 06:01:22', '2021-09-07 06:03:37', NULL, ''),
(27, 2, 'SERTIFIKATLASHTIRISH', '', '_self', NULL, '#000000', NULL, 3, '2021-09-07 06:01:33', '2021-09-07 06:03:37', NULL, ''),
(28, 2, 'REESTR', '', '_self', NULL, '#000000', NULL, 4, '2021-09-07 06:01:44', '2021-09-07 06:03:37', NULL, ''),
(29, 2, 'NORMATIV HUQUQIY BAZA', '', '_self', NULL, '#000000', NULL, 5, '2021-09-07 06:02:05', '2021-09-07 06:03:37', NULL, ''),
(30, 3, 'ГЛАВНАЯ', '', '_self', NULL, '#000000', NULL, 16, '2021-09-07 06:06:04', '2021-09-07 06:06:04', NULL, ''),
(31, 3, 'ИСПЫТАНИЯ', '', '_self', NULL, '#000000', NULL, 17, '2021-09-07 06:06:26', '2021-09-07 06:06:26', NULL, ''),
(32, 3, 'СЕРТИФИКАЦИЯ', '', '_self', NULL, '#000000', NULL, 18, '2021-09-07 06:06:55', '2021-09-07 06:06:55', NULL, ''),
(33, 3, 'РЕЕСТР', '', '_self', NULL, '#000000', NULL, 19, '2021-09-07 06:07:09', '2021-09-07 06:07:09', NULL, ''),
(34, 3, 'НОРМАТИВНАЯ БАЗА', '', '_self', NULL, '#000000', NULL, 20, '2021-09-07 06:07:27', '2021-09-07 06:07:27', NULL, ''),
(35, 3, 'ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ', '', '_self', NULL, '#000000', NULL, 21, '2021-09-07 06:07:41', '2021-09-07 06:07:41', NULL, ''),
(36, 3, 'КОНТАКТЫ', '', '_self', NULL, '#000000', NULL, 22, '2021-09-07 06:07:52', '2021-09-07 06:07:52', NULL, ''),
(37, 4, 'HOME', '', '_self', NULL, '#000000', NULL, 1, '2021-09-07 06:08:19', '2021-09-07 06:11:01', NULL, ''),
(38, 4, 'TESTS', '', '_self', NULL, '#000000', NULL, 2, '2021-09-07 06:08:53', '2021-09-07 06:11:01', NULL, ''),
(39, 4, 'CERTIFICATION', '', '_self', NULL, '#000000', NULL, 3, '2021-09-07 06:09:15', '2021-09-07 06:11:01', NULL, ''),
(40, 4, 'REGISTRY', '', '_self', NULL, '#000000', NULL, 4, '2021-09-07 06:09:37', '2021-09-07 06:11:01', NULL, ''),
(41, 4, 'NORMATIVE BASE', '', '_self', NULL, '#000000', NULL, 5, '2021-09-07 06:09:59', '2021-09-07 06:11:04', NULL, ''),
(42, 4, 'FAQ', '', '_self', NULL, '#000000', NULL, 6, '2021-09-07 06:10:24', '2021-09-07 06:11:04', NULL, ''),
(43, 4, 'CONTACTS', '', '_self', NULL, '#000000', NULL, 7, '2021-09-07 06:10:43', '2021-09-07 06:11:04', NULL, ''),
(45, 1, 'Gosts', '', '_self', NULL, NULL, NULL, 24, '2021-09-09 07:01:21', '2021-09-09 07:01:21', 'voyager.gosts.index', NULL),
(48, 1, 'Applying For Testings', '', '_self', NULL, NULL, NULL, 27, '2021-09-09 07:21:21', '2021-09-09 07:21:21', 'voyager.applying-for-testings.index', NULL),
(50, 1, 'Tnved Codes', '', '_self', NULL, NULL, NULL, 29, '2021-09-09 05:57:31', '2021-09-09 05:57:31', 'voyager.tnved-codes.index', NULL),
(52, 1, 'Procedures', '', '_self', NULL, NULL, NULL, 30, '2021-09-09 08:04:34', '2021-09-09 08:04:34', 'voyager.procedures.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(25, '2016_01_01_000000_create_pages_table', 2),
(26, '2016_01_01_000000_create_posts_table', 2),
(27, '2016_02_15_204651_create_categories_table', 2),
(28, '2017_04_11_000000_alter_post_nullable_fields_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `navbar_menus`
--

CREATE TABLE `navbar_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_uz` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2021-08-21 00:56:39', '2021-08-21 00:56:39');

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

CREATE TABLE `parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gost_id` int(11) DEFAULT NULL,
  `section` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skp_code` int(11) DEFAULT NULL,
  `labaratory_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(2, 'browse_bread', NULL, '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(3, 'browse_database', NULL, '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(4, 'browse_media', NULL, '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(5, 'browse_compass', NULL, '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(6, 'browse_menus', 'menus', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(7, 'read_menus', 'menus', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(8, 'edit_menus', 'menus', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(9, 'add_menus', 'menus', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(10, 'delete_menus', 'menus', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(11, 'browse_roles', 'roles', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(12, 'read_roles', 'roles', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(13, 'edit_roles', 'roles', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(14, 'add_roles', 'roles', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(15, 'delete_roles', 'roles', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(16, 'browse_users', 'users', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(17, 'read_users', 'users', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(18, 'edit_users', 'users', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(19, 'add_users', 'users', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(20, 'delete_users', 'users', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(21, 'browse_settings', 'settings', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(22, 'read_settings', 'settings', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(23, 'edit_settings', 'settings', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(24, 'add_settings', 'settings', '2021-08-21 00:56:08', '2021-08-21 00:56:08'),
(25, 'delete_settings', 'settings', '2021-08-21 00:56:08', '2021-08-21 00:56:08'),
(26, 'browse_hooks', NULL, '2021-08-21 00:56:08', '2021-08-21 00:56:08'),
(27, 'browse_categories', 'categories', '2021-08-21 00:56:38', '2021-08-21 00:56:38'),
(28, 'read_categories', 'categories', '2021-08-21 00:56:38', '2021-08-21 00:56:38'),
(29, 'edit_categories', 'categories', '2021-08-21 00:56:38', '2021-08-21 00:56:38'),
(30, 'add_categories', 'categories', '2021-08-21 00:56:38', '2021-08-21 00:56:38'),
(31, 'delete_categories', 'categories', '2021-08-21 00:56:38', '2021-08-21 00:56:38'),
(32, 'browse_posts', 'posts', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(33, 'read_posts', 'posts', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(34, 'edit_posts', 'posts', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(35, 'add_posts', 'posts', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(36, 'delete_posts', 'posts', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(37, 'browse_pages', 'pages', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(38, 'read_pages', 'pages', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(39, 'edit_pages', 'pages', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(40, 'add_pages', 'pages', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(41, 'delete_pages', 'pages', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(47, 'browse_products', 'products', '2021-08-23 08:02:31', '2021-08-23 08:02:31'),
(48, 'read_products', 'products', '2021-08-23 08:02:31', '2021-08-23 08:02:31'),
(49, 'edit_products', 'products', '2021-08-23 08:02:31', '2021-08-23 08:02:31'),
(50, 'add_products', 'products', '2021-08-23 08:02:31', '2021-08-23 08:02:31'),
(51, 'delete_products', 'products', '2021-08-23 08:02:31', '2021-08-23 08:02:31'),
(52, 'browse_contracts', 'contracts', '2021-08-30 04:46:01', '2021-08-30 04:46:01'),
(53, 'read_contracts', 'contracts', '2021-08-30 04:46:01', '2021-08-30 04:46:01'),
(54, 'edit_contracts', 'contracts', '2021-08-30 04:46:01', '2021-08-30 04:46:01'),
(55, 'add_contracts', 'contracts', '2021-08-30 04:46:01', '2021-08-30 04:46:01'),
(56, 'delete_contracts', 'contracts', '2021-08-30 04:46:01', '2021-08-30 04:46:01'),
(57, 'browse_vehicles', 'vehicles', '2021-08-30 05:11:02', '2021-08-30 05:11:02'),
(58, 'read_vehicles', 'vehicles', '2021-08-30 05:11:02', '2021-08-30 05:11:02'),
(59, 'edit_vehicles', 'vehicles', '2021-08-30 05:11:02', '2021-08-30 05:11:02'),
(60, 'add_vehicles', 'vehicles', '2021-08-30 05:11:02', '2021-08-30 05:11:02'),
(61, 'delete_vehicles', 'vehicles', '2021-08-30 05:11:02', '2021-08-30 05:11:02'),
(62, 'browse_activities', 'activities', '2021-08-30 05:31:22', '2021-08-30 05:31:22'),
(63, 'read_activities', 'activities', '2021-08-30 05:31:22', '2021-08-30 05:31:22'),
(64, 'edit_activities', 'activities', '2021-08-30 05:31:22', '2021-08-30 05:31:22'),
(65, 'add_activities', 'activities', '2021-08-30 05:31:22', '2021-08-30 05:31:22'),
(66, 'delete_activities', 'activities', '2021-08-30 05:31:22', '2021-08-30 05:31:22'),
(67, 'browse_applications', 'applications', '2021-09-07 02:55:37', '2021-09-07 02:55:37'),
(68, 'read_applications', 'applications', '2021-09-07 02:55:37', '2021-09-07 02:55:37'),
(69, 'edit_applications', 'applications', '2021-09-07 02:55:37', '2021-09-07 02:55:37'),
(70, 'add_applications', 'applications', '2021-09-07 02:55:37', '2021-09-07 02:55:37'),
(71, 'delete_applications', 'applications', '2021-09-07 02:55:37', '2021-09-07 02:55:37'),
(77, 'browse_navbar_menus', 'navbar_menus', '2021-09-07 05:36:21', '2021-09-07 05:36:21'),
(78, 'read_navbar_menus', 'navbar_menus', '2021-09-07 05:36:21', '2021-09-07 05:36:21'),
(79, 'edit_navbar_menus', 'navbar_menus', '2021-09-07 05:36:21', '2021-09-07 05:36:21'),
(80, 'add_navbar_menus', 'navbar_menus', '2021-09-07 05:36:21', '2021-09-07 05:36:21'),
(81, 'delete_navbar_menus', 'navbar_menus', '2021-09-07 05:36:21', '2021-09-07 05:36:21'),
(87, 'browse_gosts', 'gosts', '2021-09-09 07:01:21', '2021-09-09 07:01:21'),
(88, 'read_gosts', 'gosts', '2021-09-09 07:01:21', '2021-09-09 07:01:21'),
(89, 'edit_gosts', 'gosts', '2021-09-09 07:01:21', '2021-09-09 07:01:21'),
(90, 'add_gosts', 'gosts', '2021-09-09 07:01:21', '2021-09-09 07:01:21'),
(91, 'delete_gosts', 'gosts', '2021-09-09 07:01:21', '2021-09-09 07:01:21'),
(102, 'browse_applying_for_testings', 'applying_for_testings', '2021-09-09 07:21:21', '2021-09-09 07:21:21'),
(103, 'read_applying_for_testings', 'applying_for_testings', '2021-09-09 07:21:21', '2021-09-09 07:21:21'),
(104, 'edit_applying_for_testings', 'applying_for_testings', '2021-09-09 07:21:21', '2021-09-09 07:21:21'),
(105, 'add_applying_for_testings', 'applying_for_testings', '2021-09-09 07:21:21', '2021-09-09 07:21:21'),
(106, 'delete_applying_for_testings', 'applying_for_testings', '2021-09-09 07:21:21', '2021-09-09 07:21:21'),
(112, 'browse_tnved_codes', 'tnved_codes', '2021-09-09 05:57:31', '2021-09-09 05:57:31'),
(113, 'read_tnved_codes', 'tnved_codes', '2021-09-09 05:57:31', '2021-09-09 05:57:31'),
(114, 'edit_tnved_codes', 'tnved_codes', '2021-09-09 05:57:31', '2021-09-09 05:57:31'),
(115, 'add_tnved_codes', 'tnved_codes', '2021-09-09 05:57:31', '2021-09-09 05:57:31'),
(116, 'delete_tnved_codes', 'tnved_codes', '2021-09-09 05:57:31', '2021-09-09 05:57:31'),
(122, 'browse_procedures', 'procedures', '2021-09-09 08:04:34', '2021-09-09 08:04:34'),
(123, 'read_procedures', 'procedures', '2021-09-09 08:04:34', '2021-09-09 08:04:34'),
(124, 'edit_procedures', 'procedures', '2021-09-09 08:04:34', '2021-09-09 08:04:34'),
(125, 'add_procedures', 'procedures', '2021-09-09 08:04:34', '2021-09-09 08:04:34'),
(126, 'delete_procedures', 'procedures', '2021-09-09 08:04:34', '2021-09-09 08:04:34');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(6, 2),
(7, 1),
(7, 2),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(16, 3),
(17, 1),
(17, 3),
(18, 1),
(18, 3),
(19, 1),
(19, 3),
(20, 1),
(20, 3),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(32, 2),
(33, 1),
(33, 2),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(37, 2),
(38, 1),
(38, 2),
(39, 1),
(40, 1),
(41, 1),
(47, 1),
(47, 2),
(48, 1),
(48, 2),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\r\n                <h2>We can use all kinds of format!</h2>\r\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-08-21 00:56:39', '2021-08-21 00:56:39');

-- --------------------------------------------------------

--
-- Table structure for table `procedures`
--

CREATE TABLE `procedures` (
  `id` int(10) UNSIGNED NOT NULL,
  `tnved_id` int(11) DEFAULT NULL,
  `parametr_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `labarotory_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Whoopi Matthews', 170, '2021-08-23 08:03:36', '2021-08-23 08:03:36');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(2, 'user', 'Normal User', '2021-08-21 00:56:07', '2021-08-21 00:56:07'),
(3, 'tester', 'Laboratory assistant', '2021-08-21 04:59:52', '2021-08-21 04:59:52');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\August2021\\7SovzNAV0gHIzIfc647z.png', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Agro Reestr', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Agro Reestr admin profil', '', 'text', 1, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\August2021\\XoJJahDi2cuuwtexWzKW.gif', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\August2021\\bmRGGaLT6PQOrWyYRJJ2.gif', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 2, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `tnved_codes`
--

CREATE TABLE `tnved_codes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2021-08-21 00:56:39', '2021-08-21 00:56:39'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(22, 'menu_items', 'title', 13, 'pt', 'Publicações', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(24, 'menu_items', 'title', 12, 'pt', 'Categorias', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(25, 'menu_items', 'title', 14, 'pt', 'Páginas', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2021-08-21 00:56:40', '2021-08-21 00:56:40'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2021-08-21 00:56:40', '2021-08-21 00:56:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jshshir` int(11) DEFAULT NULL,
  `location` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `pinfl` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inn` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_expire_date` date DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `extra_info` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `region_id` bigint(20) DEFAULT NULL,
  `district_id` bigint(20) DEFAULT NULL,
  `midname` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `jshshir`, `location`, `pinfl`, `inn`, `passport`, `passport_expire_date`, `phone`, `address`, `last_login`, `extra_info`, `auth_type`, `department`, `region_id`, `district_id`, `midname`, `lastname`, `firstname`, `fullname`, `username`, `deleted_at`, `status`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$RvYdS9Qz6Sa6lhjOrLsHc.tpilh5gBKQjak4fD./drRdO9ff0l5bi', 'btl92O9Gl6c6MRoKpEPcsBTLXDu3EfqoqjBdVMYjpvEI8u7WU4l8I3QXLk99', NULL, '2021-08-21 00:56:38', '2021-08-21 00:56:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 2, 'asadbek_fayzulloev', 'fayzulloevasadbek@gmail.com', 'users/default.png', NULL, '$2y$10$8pkVMNfOzJffEI2hc6PIDegceiwXW.pMOBrPqh6zLV1VTYD7I3mAe', NULL, NULL, '2021-09-09 06:05:17', '2021-09-09 06:05:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(10) UNSIGNED NOT NULL,
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `manufacture_date` timestamp NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `power_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `power` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `engine_model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `turns_min` float NOT NULL,
  `vom_turns_min` float NOT NULL,
  `suspended_load_capacity` float NOT NULL,
  `walking_section` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `speed` float NOT NULL,
  `start_activation` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `const_work_width` int(11) NOT NULL,
  `dimensions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `ground_clearance` float DEFAULT NULL,
  `usage_load` float DEFAULT NULL,
  `wheel_width` float DEFAULT NULL,
  `bunker_capacity` float DEFAULT NULL,
  `resource_capacity` float DEFAULT NULL,
  `processing_depth` float DEFAULT NULL,
  `row_spacing` float DEFAULT NULL,
  `processing_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `processing_movement` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `processing_width` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `processing_number_bodies` int(11) DEFAULT NULL,
  `grinding_drum` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applying_for_testings`
--
ALTER TABLE `applying_for_testings`
  ADD PRIMARY KEY (`application_number`);

--
-- Indexes for table `apps`
--
ALTER TABLE `apps`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `categories_slug_unique` (`slug`) USING BTREE,
  ADD KEY `categories_parent_id_foreign` (`parent_id`) USING BTREE;

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`) USING BTREE;

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `data_types_name_unique` (`name`) USING BTREE,
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`) USING BTREE;

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`) USING BTREE;

--
-- Indexes for table `gosts`
--
ALTER TABLE `gosts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labaratories`
--
ALTER TABLE `labaratories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `menus_name_unique` (`name`) USING BTREE;

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`) USING BTREE;

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `navbar_menus`
--
ALTER TABLE `navbar_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `pages_slug_unique` (`slug`) USING BTREE;

--
-- Indexes for table `parameters`
--
ALTER TABLE `parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`) USING BTREE;

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `permissions_key_index` (`key`) USING BTREE;

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`) USING BTREE,
  ADD KEY `permission_role_permission_id_index` (`permission_id`) USING BTREE,
  ADD KEY `permission_role_role_id_index` (`role_id`) USING BTREE;

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`) USING BTREE,
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`) USING BTREE;

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `posts_slug_unique` (`slug`) USING BTREE;

--
-- Indexes for table `procedures`
--
ALTER TABLE `procedures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `roles_name_unique` (`name`) USING BTREE;

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `settings_key_unique` (`key`) USING BTREE;

--
-- Indexes for table `tnved_codes`
--
ALTER TABLE `tnved_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE,
  ADD KEY `users_role_id_foreign` (`role_id`) USING BTREE;

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`) USING BTREE,
  ADD KEY `user_roles_user_id_index` (`user_id`) USING BTREE,
  ADD KEY `user_roles_role_id_index` (`role_id`) USING BTREE;

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1812;

--
-- AUTO_INCREMENT for table `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `applying_for_testings`
--
ALTER TABLE `applying_for_testings`
  MODIFY `application_number` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `apps`
--
ALTER TABLE `apps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gosts`
--
ALTER TABLE `gosts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `labaratories`
--
ALTER TABLE `labaratories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `navbar_menus`
--
ALTER TABLE `navbar_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `parameters`
--
ALTER TABLE `parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `procedures`
--
ALTER TABLE `procedures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tnved_codes`
--
ALTER TABLE `tnved_codes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
