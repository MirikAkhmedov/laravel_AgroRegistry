<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Activity extends Model
{
    use HasFactory,Searchable;
    protected $fillable = [
        'url', 'method', 'ip', 'agent', 'user_id'
    ];
}
