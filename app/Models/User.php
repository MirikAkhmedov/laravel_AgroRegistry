<?php

namespace App\Models;

use App\App;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Scout\Searchable;
use TCG\Voyager\Traits\Translatable;

class User extends \TCG\Voyager\Models\User
{
    use Translatable;
    protected $translatable = ['title', 'body'];

    use HasApiTokens, HasFactory, Notifiable, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'fullname',
        'firstname',
        'lastname',
        'midname',
        'pinfl',
        'inn',
        'passport',
        'phone',
        'address',
        'auth_type',
        'status',
        'passport_expire_date',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function companies()
    {
        return $this->belongsToMany(Company::class, 'user_companies');
    }

    public function Gtk()
    {
        return $this->hasOne(Gtk::class);
    }

    public function applications(){
        return $this->hasMany(Application::class);
    }

}
