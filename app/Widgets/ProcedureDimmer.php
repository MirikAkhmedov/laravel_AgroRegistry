<?php

namespace App\Widgets;

use App\Models\Procedure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class ProcedureDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Procedure::count();
        $string = trans_choice('dimmer.procedure1', $count);

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-documentation',
            'title'  => "{$count} {$string}",
            'text'   => trans('dimmer.Comapplication_text', ['count' => $count, 'string' => Str::lower($string)]),
            // 'text' => 'Ariza',
            'button' => [
                'text' => trans('dimmer.procedure'),
                // 'text' => trans('voyager::dimmer.post_link_text'),
                'link' =>route('voyager.procedures.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/02.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
}
