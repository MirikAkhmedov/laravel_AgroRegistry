<?php
namespace App\Actions;


use TCG\Voyager\Actions\AbstractAction;
use Illuminate\Support\Facades\Auth;
class  wordAction extends AbstractAction {
    public function getTitle(){
        return "download";
    }
    public function getIcon(){
        return 'voyager-download';
    }
    public function getPolicy(){
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull right',
        ];
    }

    public function getDefaultRoute()
    {
        // , compact('appeal_id')
        return route('contracts.download', ['word' => $this->data->id]);
    }

    public function shouldActionDisplayOnDataType()
    {
//        if(Auth::user()->hasRole('user')){
            return $this->dataType->slug == 'applications';
//        }
    }

}
