<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Content;
use App\Models\Parameter;
use App\Models\Procedure;
use Illuminate\Http\Request;

class LabaratoryController extends Controller
{
    public function test($application){
        $app = Application::where('id', $application)->first();
        $parametres_array = Procedure::where('tnved_id', $app->tnved_code)->get()->pluck('parametr_id');
        $parametres = Parameter::whereIn('id', $parametres_array)->get();
        $content = Content::query()->where('application_id', $app->id);

        return view('labaratories.test', compact('parametres', 'app','content'));
    }

    public function store(Request $request){
        dd($request);
    }

    public function test_apply(Request $request){
        $input = $request->all();

        foreach ($input as $key => $value){
            $split =  explode("_", $key);

            if ($split[0] == "param"){


                $parametr = Parameter::find($split[1]);
                $content = explode(" ",$parametr->content);
                $i = 0;
                $replacement = "";
                $json = [];
                foreach ($content as $word){
                    if (substr($word, 0, 2) == '${')
                    {
                        $param_in = "param_".$parametr->id."_".$i;

                        echo $request->$param_in;
                        $json[$param_in] = $request->$param_in;
                        $replacement .= $request->$param_in." ";
                        $i++;
                    }else{
                        $replacement .= $word." ";
                    }
                }

                $new_content = Content::query()->where('application_id', $request->application)->where('parametr_id', $parametr->id)->first()?
                    Content::query()->where('application_id', $request->application)->where('parametr_id', $parametr->id)->first(): new Content();

                $new_content->text = $replacement;
                $new_content->application_id = $request->application;
                $new_content->parametr_id = $parametr->id;
                $new_content->inputs = json_encode($json);

                $new_content->save();
            }
        }
        return redirect()->route("voyager.applications.index");
    }
}
