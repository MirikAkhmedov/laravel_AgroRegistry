<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Services\SmsService\SmsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Models\Application;
use App\Models\UserCompany;
use App\Models\TnvedCode;
use App\Models\Tovar;
use App\Models\Gtk;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\User;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Models\DataRow;

class ApplicationController extends Controller
{
    public function activity() {
        $x = Activity::where("ip", FacadesRequest::ip())->get();
        return view("active", ['nmadr'=>$x]);
    }

    public function index()
    {

        return view('application.add');
    }
    public function __construct()
    {
        $this->middleware("auth");
    }
    public function run()
    {
        $user = Auth::user();
        //        dd($user->status);

        return view("application.add", ["user" => $user]);
    }

    public function gen(Request $req)
    {
        $user = auth()->user();
        $user->email = $req->email;
        $user->address = $req->address;
        $user->phone = preg_replace('/[^0-9]/', '', $req->phone);
        $user->save();
        $textsms = "Address: https://reestr.agro.uz/admin ";
        $smsSender = new SmsService();
        $smsSender->send($req->phone, $textsms);
        return redirect()->route('voyager.applications.user_next');
    }
    // Teyganga ko'tiga yara chiqsin
    public function user_next()
    {
        return view("application.filter");
    }

    public function filter(Request $request)
    {
        $tovar_type = $request->tovar_type;
        $tnved_code = TnvedCode::query()->where('code', $request->tnved_code)->first()->id;
        session()->put('tovar_type', $tovar_type);
        session()->put('tnved_code', $tnved_code);
        session()->put('status', "data_uz");
        $i = 0;
        if ($tovar_type == "Import") {

            $g7_a = $request->G7_A;
            $g7_b = $request->G7_B;
            $g7_c = $request->G7_C;

            $gtk = Gtk::query()->where("G7_A", $g7_a)->where("G7_B", $g7_b)->where("G7_C", $g7_c)->first();
            if ($gtk) {
                foreach ($gtk->tovars as $tovar) {
                    if ($tovar->TNVED == TnvedCode::find($tnved_code)->code) {
                        session()->put('tovar', $tovar->id);
                        session()->put('gtk', $gtk->id);
                        session()->put('status', "data");
                        $i++;
                    }
                }
            }
            if (!$i) {
                $gtk = null;
                $tovar = null;
                $status = "no_data";
                return view("application.data", compact('tnved_code', 'tovar_type', 'gtk', 'tovar', 'status'));
            }
        }



        return redirect()->route('voyager.application.data');
    }

    public function data()
    {


        $tovar_type = session('tovar_type');
        $tnved_code = TnvedCode::find(session('tnved_code'));
        $gtk = null;
        $tovar = null;
        if (session()->has('gtk') && session()->has('tovar')) {
            $tovar = Tovar::find(session('tovar'));
            $gtk = Gtk::find(session('gtk'));
        } elseif (!session()->has('tovar_type') && !session()->has('tnved_code')) {
            return back();
        }

        $status = session('status');

        return view("application.data", compact('tnved_code', 'tovar_type', 'gtk', 'tovar', 'status'));
    }
    public function check(Request $request)
    {
        $vehicle_name = $request->vehicle_name;
        $quantity = $request->quantity;
        $dirname = 'uploads/' . Auth::user()->id;
        $files = $request->file('images');
        $file1 = $request->file('file1');
        $file2 = $request->file('file2');
        $file3 = $request->file('file3');
        $file4 = $request->file('file4');
        $file_json = array();
        $file1_json = array();
        $file2_json = array();
        $file3_json = array();
        $file4_json = array();
        $gtk = $request->gtk;
        $tovar = $request->tovar;

        if ($gtk && $tovar) {
            $tovar = Tovar::find($tovar);

            $vehicle_name = $tovar->G31NAME;
            $quantity = $tovar->QUANTITY;
        }


        if ($request->hasFile('images')) {
            foreach ($files as $file) {
                $file->storeAs($dirname, $file->getClientOriginalName());
                $file_json[] = $dirname . "/" . $file->getClientOriginalName();
            }
        }
        if ($request->hasFile('file1')) {
            foreach ($file1 as $file) {
                $file->storeAs($dirname, $file->getClientOriginalName());
                $file1_json[] = $dirname . "/" . $file->getClientOriginalName();
            }
            session()->put('file1', $file1_json);
        }
        if ($request->hasFile('file2')) {
            foreach ($file2 as $file) {
                $file->storeAs($dirname, $file->getClientOriginalName());
                $file2_json[] = $dirname . "/" . $file->getClientOriginalName();
            }
            session()->put('file2', $file1_json);
        }
        if ($request->hasFile('file3')) {
            foreach ($file3 as $file) {
                $file->storeAs($dirname, $file->getClientOriginalName());
                $file3_json[] = $dirname . "/" . $file->getClientOriginalName();
            }
            session()->put('file3', $file1_json);
        }
        if ($request->hasFile('file4')) {
            foreach ($file4 as $file) {
                $file->storeAs($dirname, $file->getClientOriginalName());
                $file4_json[] = $dirname . "/" . $file->getClientOriginalName();
            }
            session()->put('file4', $file1_json);
        }

        session()->put('vehicle_name', $vehicle_name);
        session()->put('quantity', $quantity);
        session()->put('images', $file_json);

        return redirect()->route('voyager.application.double_check');
    }
    public function double_check()
    {

        $tovar_type = session('tovar_type');
        $tnved_code = TnvedCode::find(session('tnved_code'));
        $images = session('images');

        $quantity = session('quantity');
        $vehicle_name = session('vehicle_name');

        $gtk = Gtk::find(session('gtk'));
        $tovar = Tovar::find(session('tovar'));

        if (!session()->has('vehicle_name')) {
            return back();
        }

        return view('application.check', compact('vehicle_name', 'tovar_type', 'tnved_code', 'images', 'quantity', 'gtk', 'tovar'));
    }
    public function store(Request $req)
    {

        $user_companies = UserCompany::where('user_id', Auth::user()->id)->get();
        foreach ($user_companies as $value) {
            $companies[] = $value->company_id;
        }
        $app = new Application();
        $x = Application::all();
        $app->user_id = Auth::user()->id;

        $gtk = Gtk::find(session('gtk'));
        $tnved_code = TnvedCode::find(session('tnved_code'));

        if ($gtk) {
            $app->import_country = $gtk->G17;
            $app->manafactured_country = $gtk->G15;
            $app->sender_company = $gtk->G2_NAME;
        }

        $app->vehicle_category = $tnved_code->name;
        $app->tnved_code = $tnved_code->id;

        $app->vehicle_name = session('vehicle_name');
        $app->quantity = session('quantity');
        $app->tovar_type = session('tovar_type');

        $app->image = $req->images;

        $app->file1 = json_encode(session('file1'));
        $app->file2 = json_encode(session('file2'));
        $app->file3 = json_encode(session('file3'));
        $app->file4 = json_encode(session('file4'));
        $app->save();

        session()->forget('tnved_code');
        session()->forget('gtk');
        session()->forget('tovar');
        session()->forget('tovar_type');
        session()->forget('vehicle_name');
        session()->forget('quantity');
        session()->forget('images');
        session()->forget('status');



        return redirect()->route('voyager.applications.index');
    }



    public function pay($id)
    {

        return view("application.payAction");
    }
    public function update($id)
    {
        $x = Application::where('id', $id)->first();
        $x->update([
            'status' => 1,
        ]);
        // Application::where('id', $id)->update([
        //     'status'=>
        // ]);

        return back();
    }
    public function show()
    {
        $x = Application::all();
        return view("application.applicatin", ["applications" => $x]);
    }
    public function view(Application $id)
    {

        $application = $id;
        $tnved_code = TnvedCode::find($application->tnved_code);
        $status = $application->status;
        $status = DataRow::query()->where('data_type_id', 20)->where('field', 'status')->first()->details->options->$status;
        return view("application.show", compact('application', 'tnved_code', 'status'));
    }
    public function delete(Request $req, $id)
    {
        Application::where("id", $id)->delete();
        return back();
    }
    public function edit(Application $id)
    {
        session()->put('edit', $id->id);
        return redirect()->route("voyager.applications.create");
    }
}


//0 => "name"
//    1 => "email"
//    2 => "password"
//    3 => "fullname"
//    4 => "firstname"
//    5 => "lastname"
//    6 => "midname"
//    7 => "pinfl"
//    8 => "inn"
//    9 => "passport"
//    10 => "phone"
//    11 => "address"
//    12 => "auth_type"
//    13 => "status"
//    14 => "passport_expire_date"
