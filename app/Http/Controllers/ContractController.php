<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Models\Application;
use App\Models\UserCompany;
use App\Models\Contract;
use App\Models\User;


use Illuminate\Support\Facades\Storage;


class ContractController extends Controller{
    public function run () {
        return view("contract.contract");
    }
    public function download (Request $req, $word) {
        $contract = User::where("id",Auth::user()->id)->get()->first();
        $templateProcessor = new TemplateProcessor("temlpate/dogovor.docx");
        $templateProcessor->setValue('fullName', $contract->fullname);
        $templateProcessor->setValue('entity', $contract->user_type);
        $templateProcessor->setValue("customer", $contract->fullname);
        $templateProcessor->setValue("address", $contract->address);
        $templateProcessor->setValue("phone_number", $contract->phone);
        $templateProcessor->setValue("phone_number", $contract->phone);
        $templateProcessor->setValue("inn", $contract->inn);
        $templateProcessor->saveAs("salom.docx");
        return response()->download("salom.docx")->deleteFileAfterSend(true);
    }
}
