<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class SendEmailController extends Controller
{
    function index()
    {
     return view('send_email');
    }

    function send(Request $request)
    {
     $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required',
      'message' =>  'required'
     ]);

        $details = array(
            'name'      =>  $request->name,
            'message'   =>   $request->message,
            'email' => $request->email
        );
        $to = $request->email;
     Mail::to($to)->send(new SendMail($details));
     return back()->with('success', 'Thanks for contacting us!');

    }
}

?>