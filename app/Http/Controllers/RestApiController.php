<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gtk;
use App\Models\Tovar;

class RestApiController extends Controller
{
    public function json_write(Request $request){
        return json_decode(['Vse'=>'OK']);
        $json_array = json_decode($request->input('json'));
        $gtk = new GTK();        
        $gtk_id;
        $g31prod;
        $g31engi;
        $g31det;

    foreach($json_array as $json_ar1)
    {        
        $gtk->G15 = $json_ar1->DeclarationData->G15;
        $gtk->G1_B = $json_ar1->DeclarationData->G1_B;
        $gtk->G17 = $json_ar1->DeclarationData->G17;
        $gtk->G1_A = $json_ar1->DeclarationData->G1_A;
        $gtk->G2_CODE2 = $json_ar1->DeclarationData->G2_CODE2;
        $gtk->G2_ADDRESS = $json_ar1->DeclarationData->G2_ADDRESS;
        $gtk->G8_ADDRESS = $json_ar1->DeclarationData->G8_ADDRESS;
        $gtk->G8_NAME = $json_ar1->DeclarationData->G8_NAME;
        $gtk->G7_B = $json_ar1->DeclarationData->G7_B;
        $gtk->STATUS = $json_ar1->DeclarationData->STATUS;
        $gtk->G7_A = $json_ar1->DeclarationData->G7_A;
        $gtk->G54_DATE = $json_ar1->DeclarationData->G54_DATE;
        $gtk->G2_NAME = $json_ar1->DeclarationData->G2_NAME;
        $gtk->G7_C = $json_ar1->DeclarationData->G7_C;
        $gtk->G8_CODE2 = $json_ar1->DeclarationData->G8_CODE2;
        $gtk->ID = $json_ar1->DeclarationData->ID;
        $gtk->Information_Date = $json_ar1->Information_Date;
        $gtk_id  = $json_ar1->DeclarationData->ID;
        $gtk2 = GTK::find($gtk_id);        
        if($gtk2 == null)
        {$gtk->save();
    }
        
foreach($json_ar1->DeclarationData->Tovar as $tovar2)
    {
    foreach($tovar2->G31DETAILS as $G31)
    {
        $g31prod = $G31->PRODUCTIONYEAR;
        $g31engi = $G31->ENGINENO;
        $g31det = "[{\"ENGINENO\": \".$g31engi.\", \"PRODUCTIONYEAR\": \".$g31prod.\"}]";
    }     
    
    $tovar = new Tovar();
    $tovar->G31DETAILS = $g31det;
    $tovar->G41 = $tovar2->G41;
    $tovar->QUANTITY = $tovar2->QUANTITY;
    $tovar->G32 = $tovar2->G32;
    $tovar->G31NAME = $tovar2->G31NAME;    
    $tovar->G46 = $tovar2->G46;
    $tovar->G34 = $tovar2->G34;
    $tovar->G38 = $tovar2->G38;
    $tovar->TNVED = $tovar2->TNVED;    
    {$tovar->save();}   
    }
    } 
    
    return json_decode(['Vse'=>'OK']);

    }

}
