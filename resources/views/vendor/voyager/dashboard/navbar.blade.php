<nav class="navbar navbar-default navbar-fixed-top navbar-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="hamburger btn-link">
                <span class="hamburger-inner"></span>
            </button>
            
        </div>
        <ul class="nav navbar-nav @if (trans('generic.is_rtl') == 'true') navbar-left @else navbar-right @endif">
            {{-- lang selector --}}
            {{-- <li style="margin-top: 20px;">
                <select class="selectLang" data-width="fit" style="border: none; padding: 5px 2px;border-radius: 2px">
                    <option>English</option>
                    <option>Russian</option>
                    <option>Uzbek</option>
                </select>
            </li> --}}
            <li>
                <form action="{{route('user.lang')}}" id="form-id" method="post">
                @csrf
                    <button  class="btn " name="lang" value="ru" type="submit">Русский</button>
                </form>
            </li>

            <li>
                <form action="{{route('user.lang')}}" id="form-id" method="post">
                @csrf
                    <button  class="btn" name="lang" value="uz"  type="submit">O`zbek</button>
                </form>
            </li>

            <li>
                <form action="{{route('user.lang')}}" id="form-id" method="post">
                @csrf
                    <button  class="btn" name="lang" value="en"  type="submit">English</button>
                </form>
            </li>


            {{-- /lang selector --}}

            <li class="dropdown profile">
                <a href="#" class="text-right dropdown-toggle" data-toggle="dropdown" role="button"
                   aria-expanded="false"><img src="{{ $user_avatar }}" class="profile-img"> <span
                            class="caret"></span></a>
                <ul class="dropdown-menu dropdown-menu-animated">

                    <li class="profile-img">
                        <img src="{{ $user_avatar }}" class="profile-img">
                        <div class="profile-body">
                            <h5>{{ Auth::user()->name }}</h5>
                            <h6>{{ Auth::user()->email }}</h6>
                        </div>
                    </li>
                    <li class="divider"></li>
                    <?php $nav_items = config('voyager.dashboard.navbar_items'); ?>
                    @if(is_array($nav_items) && !empty($nav_items))
                    @foreach($nav_items as $name => $item)
                    <li {!! isset($item['classes']) && !empty($item['classes']) ? 'class="'.$item['classes'].'"' : '' !!}>
                        @if(isset($item['route']) && $item['route'] == 'voyager.logout')
                        <form action="{{ route('voyager.logout') }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-block">
                                @if(isset($item['icon_class']) && !empty($item['icon_class']))
                                <i class="{!! $item['icon_class'] !!}"></i>
                                @endif
                                {{__($name)}}
                            </button>
                        </form>
                        @else
                        <a href="{{ isset($item['route']) && Route::has($item['route']) ? route($item['route']) : (isset($item['route']) ? $item['route'] : '#') }}" {!! isset($item['target_blank']) && $item['target_blank'] ? 'target="_blank"' : '' !!}>
                            @if(isset($item['icon_class']) && !empty($item['icon_class']))
                            <i class="{!! $item['icon_class'] !!}"></i>
                            @endif
                            {{__($name)}}
                        </a>
                        @endif
                    </li>
                    @endforeach
                    @endif
                </ul>
            </li>
        </ul>
    </div>
</nav>
