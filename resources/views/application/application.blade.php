@extends('voyager::master')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@section('content')

<div class="side-body padding-top">
    <div class="container-fluid">
        <h1 class="page-title">
            <i class=""></i> Arizalar
        </h1>
        <a href="http://reestr.agro.uz/ru/admin/applications/create" class="btn btn-success btn-add-new">
            <i class="voyager-plus"></i> <span>Qo`shish </span>
        </a>
        <a class="btn btn-danger" id="bulk_delete_btn"><i class="voyager-trash"></i> <span>Tanlanganlarni o`chirish </span></a>


        <!-- /.modal -->


    </div>
    <div id="voyager-notifications"></div>
    <div class="page-content browse container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row"><div class="col-sm-12">
                                        <table id="dataTable" class="table table-bordered table-hover dataTable no-footer" role="grid" aria-describedby="dataTable_info">
                                            <thead>
                                                <tr>
                                                    <th>Ariza raqami</th>
                                                    <th>TNVED Raqami</th>
                                                    <th>Kategoriya nomi</th>
                                                    <th>Texnika name</th>
                                                    <th>Status</th>
                                                    <th>Ariza beruvchi</th>
                                                    <th>Texnika soni</th>
                                                    <th>Countries</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse (auth()->user()->applications as $application)
                                                <tr>
                                                    <td>{{ $application->id }}</td>
                                                    <td>{{ $application->tnved_code }}</td>
                                                    <td>{{ App\Models\TnvedCode::where("id",$application->tnved_code)->first()->name }}</td>
                                                    <td>{{ $application->vehicle_name }}</td>
                                                    <td>  <button class="btn {{$application->status == "1" ? "btn-primary":"btn-danger"  }}">status</button>  </td>
                                                    <td> vgdplfkgopkg</td>
                                                    <td>{{ $application->quantity }}</td>
                                                    <td>{{ App\Models\Countries::where("id", $application->id)->first()->name }}</td>
                                                    <td class="no-sort no-click bread-actions">
                                                        <a href="javascript:;" title="Удалить" class="btn btn-sm btn-danger pull-right delete" data-toggle="modal" data-target="#exampleModal">
                                                            <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Удалить</span>
                                                        </a>
                                                        <a href="http://reestr.agro.uz/ru/admin/applications/{{$application->id }}/edit" title="Изменить" class="btn btn-sm btn-primary pull-right edit">
                                                            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Изменить</span>
                                                        </a>
                                                        <a href="http://reestr.agro.uz/ru/admin/applications/38" title="Просмотр" class="btn btn-sm btn-warning pull-right view">
                                                            <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">Просмотр</span>
                                                        </a>
                                                    </td>
                                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <h1 aria-hidden="true">&times;</h1>
                                                                    </button>
                                                                </div>
                                                                <h1 class="modal-body" class="h1">
                                                                    Вы действительно хотите удалить это?
                                                                </h1>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <form action="{{ route('application.delete',[ 'id'=> $application->id ]) }}" method="POST">
                                                                        @csrf
                                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </tr>
                                                @empty
                                                @endforelse

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{--  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>--}}
{{--<script src="https://code.jquery.com/jquery-3.5.1.js"></script>--}}
{{--<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap.min.js"></script>--}}

<script>
    $(document).ready(function() {
        let lang = $('#lang').text().trim();
        $('#example').DataTable({
            "lengthMenu": [
            [5,10, 25, 50, -1],
            [5,10, 25, 50, "All"]
            ],
            "order": [[0, 'desc']],
            language: {
                url: `https://cdn.datatables.net/plug-ins/1.11.3/i18n/uz.json`
            }
        });
    });
</script>
@endsection
