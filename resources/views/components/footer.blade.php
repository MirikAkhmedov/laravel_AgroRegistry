<div id="foot" class="footer">
        <div class="container mt-5">
            <div class="row">
                <div class="item col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 ">
                    <div class="dsf mb-4 d-flex">
                        <img src="img/image 16.png" alt="error">
                        <p>@lang('site.footer')</p>
                    </div>
                    <p>@lang('site.footer1')
                    </p>
                    <div class="dsf">

                    </div>
                </div>

                <div class="item col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                    <div class="dtf mb-4 ">
                        <p>@lang('site.footer2')</p>
                    </div>
                    <p>
                    @lang('site.footer3')
                    </p>
                    <ul>
                        <li>@lang('site.footer4') </li>
                        <li>+998(70)601-11-48 </li>
                        <li>Email: </li>
                        <li>uzmis1@mail.ru </li>
                    </ul>
                </div>

                <div class="item col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                    <div class="dtf mb-4 ">
                        <p>@lang('site.footer5')</p>
                    </div>
                    <ul>
                        <li><a href="#">@lang('site.footer6') </a></li>
                        <li><a href="#">@lang('site.footer7')</a></li>
                        <li><a href="#">@lang('site.footer8')</a></li>
                        <li><a href="#">@lang('site.footer9') </a></li>
                    </ul>
                </div>
                <div class="item col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                    <div class="dtf mb-4 ">
                        <p>@lang('site.footer10')</p>
                    </div>
                    <ul>
                        <li><a href="#">@lang('site.footer11')</a></li>
                        <li><a href="#">@lang('site.footer12')</a></li>
                        <li><a href="#">@lang('site.footer13')</a></li>
                        <li><a href="#">@lang('site.footer14')</a></li>
                        <li><a href="#">@lang('site.footer15')</a></li>
                    </ul>
                </div>


            </div>

        </div>
    </div>
    <!-- end footer -->
    <div class="container-fluid foter_end">
        <div class="row">
            <div class="col-12">
                <p class="text-center">@lang('site.footer16')</p>
            </div>
        </div>
    </div>
