<style>
    .highlight{
        background-color: yellow;
        font-weight: bold;
    }
</style>
   <nav class="navbar navbar-expand-lg navbar-light bg-white " style="border:1px solid rgb(0,0,0,0.1);">
        <div class="container-fluid d-flex align-items-center justify-content-beetwen">
                <div>
                    <a class="navbar-brand d-flex align-items-center float: top-left" href="#">
                        <img src="{{asset('images/layout/gerb.png')}}" id="gerb">
                        @lang('site.into')
                    </a>
                </div>


                <div class="d-flex align-items-center" id="navbarSupportedContent">
                    <a href="#"><span class="iconify" data-icon="ic:baseline-location-on"></span>
                    @lang('site.adress')
                </a>
                    <div class="nav_line"></div>
                    <a href="#"><span class="iconify" data-icon="ic:round-mail"></span> uzmis1@mail.ru</a>
                    <div class="nav_line"></div>
                    <a href="#"><span class="iconify" data-icon="icomoon-free:phone"></span> (90) 955 55 55</a>
                    <div class="nav_line"></div>
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle d-flex align-items-center" data-toggle="dropdown"><span class="iconify" data-icon="eva:arrow-ios-downward-outline"></span>
                        @lang('site.lang')</a>
                        <div class="dropdown-menu">
                            <a href="/uz" class="dropdown-item">UZ</a>
                            <a href="/ru" class="dropdown-item">RU</a>
                            <a href="/en" class="dropdown-item">EN</a>
                        </div>
                    </div>

                    <a href="/admin/login" class="btn btn-success text-white"  id="headerButton">@lang('site.admin')</a>

                </div>
        </div>
    </nav>


    <nav class="bg-white" id="headNavbar">
        <div class="container-fluid nav d-flex justify-content-center">
            <a class="m-xl-5 m-md-4 link-secondary active" href="#" >@lang('site.header')</a>
            <a class="m-xl-5 m-md-4 link-secondary" href="#round">@lang('site.header1')</a>
            <a class="m-xl-5 m-md-4 link-secondary" href="#gohere">@lang('site.header2')</a>
            <a class="m-xl-5 m-md-4 link-secondary" href="#baza">@lang('site.header4')</a>
            <a class="m-xl-5 m-md-4 link-secondary" href="#vazifa">@lang('site.header5')</a>
            <a class="m-xl-5 m-md-4 link-secondary" href="#foot">@lang('site.header6')</a>

        </div>
    </nav>


   @section('script')

       <script>

           $(document).ready(function (){
               $(".searchButton").click(function (){
                   searchHighlight($(".searchTerm").val())
               })
           })

           function generateResults(matches){

           }

           function searchHighlight(txt) {
               if(txt){
                   $(".highlight").removeClass("highlight")
                   var content =  $("body").html();
                   var searchexp = new RegExp(txt, "ig");
                   var matches = content.match(searchexp);
                   if(matches){
                        $("body").html(content.replace(searchexp, function (match){
                            return "<span class='highlight'>" + match + "</span>"
                        }))
                   }
               }else{
                   $(".highlight").removeClass("highlight")
               }
           }
       </script>

   @endsection
