@extends('voyager::master')
@section('content')
    <h1 class="page-title">
        <i class=""></i>Test
    </h1>
    <link rel="stylesheet" href="{{asset('css/laboratory.css')}}"/>
    <div class="wrapper">
        <!-- Sidebar  -->
        <div id=" voyager-notifications">
            <div class="page-content edit-add container-fluid">
                <div class="row">
                    <form action="{{route('voyager.application.test.store')}}" method="post">
                        @csrf
                        <input type="hidden" name="application" value="{{$app->id}}">
                        <div class="col-auto panel panel-border" id="main" style="margin: 0 20px">
                            <div class="panel-body" >
                                @foreach ($parametres as $parametr)

                                    <div class="check check_{{$parametr->id}}" data-check="check_{{$parametr->id}}">

                                    <h6>{{$parametr->name}}</h6>
                                    <hr>
                                    <p  class="{{$parametr->id}}">

                                        @php
                                            $str = $parametr->content;
                                        $replacement = "";
                                        $json = [];
                                        $len = substr_count($str, '${i}');

                                        //$value = $content->where('parametr_id', $parametr)->first()?$content->where('parametr_id', $parametr)->first():null;

                                        for ($i=0; $i < $len ; $i++) {
                                         $input = '<input type="number" name="param_'.$parametr->id.'_'.$i.'"  value="">';
                                         $str = preg_replace('/\${i}/', $input, $str, 1);
                                        }
                                        $k = $len;
                                        $len = substr_count($str, '${d}');
                                        for ($i=$k; $i < $len + $k ; $i++) {
                                         $input = '<input type="date" name="param_'.$parametr->id.'_'.$i.'"  value="">';
                                         $str = preg_replace('/\${d}/', $input, $str, 1);

                                        }
                                        $k = $len + $k;
                                        $len = substr_count($str, '${s}');
                                        for ($i=$k; $i < $len + $k ; $i++) {
                                         $input = '<input type="text" name="param_'.$parametr->id.'_'.$i.'"  value="">';
                                         $str = preg_replace('/\${s}/', $input, $str, 1);
                                        }
                                        echo $str;

                                        @endphp
                                    </p>
                                </div>

                                    <br>
                                @endforeach
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-success">Saqlash</button>
                            </div>
                        </div>
                    </form>


                    <button class="openbtn" onclick="openNav()">&#9776; Open</button>
                    <div class="col-md-4">
                        <div id="mySidepanel" class="sidepanel">
                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                            <form action="">
                                <ul style="text-decoration: none">
                                @foreach ($parametres as $parametr)

                                    <li>
                                        <label for="check_{{$parametr->id}}">{{$parametr->name}}</label>
                                        <input class="checkbox" name="checkbox" type="checkbox" id="check_{{$parametr->id}}"
                                               data-check="check_{{$parametr->id}}" onclick="panelHidden()" checked>
                                    </li>

                                @endforeach
                                </ul>
                            </form>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')

    <script type="text/javascript" src="{{asset('js/laboratory.js')}}"></script>

@endsection

