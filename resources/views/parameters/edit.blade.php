@extends('voyager::master')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


@section('content')
        <h1 class="page-title">
            <i class=""></i>Add</h1>
            <div id=" voyager-notifications">
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" class="form-edit-add" action="http://reestr.local/admin/parameters" method="POST"
                        enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->

                        <!-- CSRF TOKEN -->
                        <input type="hidden" name="_token" value="hF3AsgcgnVr3yvbRmafXEbQ3KcLUiLsrUQ7jydD3">

                        <div class="panel-body">
                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="parametrName">Parametr nomi</label>
                                <input required="" type="text" class="form-control" name="name" placeholder="Parametr nomi" value="">
                            </div>
                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="parametrContent">Content</label>
                            </div>
                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="name">GOST</label>
                                <select class="form-control" name="gost_id">
                                    @foreach(DB::select('select * from gosts') as $gost)
                                        <option value="{{ $gost->id }}"@if(isset($dataTypeContent->gost_id) && $dataTypeContent->gost_id == $gost->id) selected="selected"@endif>{{ $gost->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="name">Section</label>
                                <input type="text" class="form-control" name="section" placeholder="Section" value="">
                            </div>
                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="name">Skp Code</label>
                                <input type="text" class="form-control" name="skp_code" placeholder="Skp Code" value="">
                            </div>
                            <div class="form-group  col-md-12 " data-select2-id="8">
                                <label class="control-label" for="name">Labaratoriya</label>
                                <select class="form-control" name="labaratory_id">
                                    @foreach(DB::select('select * from labaratories') as $labaratory)
                                        <option value="{{ $labaratory->id }}"@if(isset($dataTypeContent->labaratory_id) && $dataTypeContent->labaratory_id == $labaratory->id) selected="selected"@endif>{{ $labaratory->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">Save</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="http://reestr.local/admin/upload" target="form_target" method="post"
                        enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="applications">
                        <input type="hidden" name="_token" value="hF3AsgcgnVr3yvbRmafXEbQ3KcLUiLsrUQ7jydD3">
                    </form>

                </div>
            </div>
        </div>
    </div>
    <script>
        function usertype(ch){
            company = document.getElementById('company');

            if(ch == 1){
                company.disabled = false;
            } else
            company.disabled = true;

        }
        function selected(s) {


        }

    </script>
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> Are you sure</h4>
                </div>

                <div class="modal-body">
                    <h4>Are you sure you want to delete '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">Yes, Delete it!</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@endsection
