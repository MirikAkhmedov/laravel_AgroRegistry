@extends('voyager::master')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
{{--<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>--}}
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .copied{
        position: absolute;
        height: 30px;
        width: 100%;
        background-color: white;
        border-radius: 5px;
        z-index: 100000;
        bottom: 110%;
        box-shadow: 0 0 2px gray;
        left: 0;
        color: gray;
        align-items: center;
        display: none;
    }
    .fas{
        margin-left: 20px;
    }

    .copy{
        position: relative;
        box-shadow: 0 0 2px gray;

    }
</style>
@section('content')
        <h1 class="page-title">
            <i class=""></i>Add</h1>
            <div id=" voyager-notifications">
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" class="form-edit-add" action="{{route('voyager.parameters.store')}}" method="POST"
                        enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->

                        <!-- CSRF TOKEN -->
                        @csrf
                        <div class="panel-body">
                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="parametrName">Parametr nomi</label>
                                <input required="" type="text" class="form-control" name="name" placeholder="Parametr nomi" value="">
                            </div>
                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="parametrContent">Content</label>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-12" style="margin: 0;">O'zgaruvchi qo'shish uchun buferga olish:</div>
                                        <div class="col-lg-2"><div class="btn btn-light copy " data-copy="${i}"><div class="copied">Copied</div>Number  <div class="fas fa-copy"></div></div></div>
                                        <div class="col-lg-2"><div class="btn btn-light copy" data-copy="${s}"><div class="copied">Copied</div>String  <div class="fas fa-copy"></div></div></div>
                                        <div class="col-lg-2"><div class="btn btn-light copy" data-copy="${d}"><div class="copied">Copied</div>Date  <div class="fas fa-copy"></div></div></div>
                                    </div>

                                </div>

                                <script>
                                    $('.copy').click(function() {

                                        copyToClipboard($(this).attr('data-copy'));
                                        $(this).children('.copied').css("display","block");
                                        $(this).children('.copied').css("opacity","1");
                                        $(this).children('.copied').animate({
                                            opacity: 0
                                        }, {duration:1000, queue: false, complete: function(){
                                                $("#fading_dolar").css({
                                                    "opacity": "1",
                                                });
                                            }});
                                    })
                                    function copyToClipboard(text) {
                                        var sampleTextarea = document.createElement("textarea");
                                        document.body.appendChild(sampleTextarea);
                                        sampleTextarea.value = text; //save main text in it
                                        sampleTextarea.select(); //select textarea contenrs
                                        document.execCommand("copy");
                                        document.body.removeChild(sampleTextarea);
                                    }
                                </script>

                                <textarea name="editor1"></textarea>
                                <script>
                                    CKEDITOR.replace( 'editor1' );
                                </script>
                            </div>
                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="name">GOST</label>
                                <select class="form-control" name="gost_id">
                                    @foreach(DB::select('select * from gosts') as $gost)
                                        <option value="{{ $gost->id }}"@if(isset($dataTypeContent->gost_id) && $dataTypeContent->gost_id == $gost->id) selected="selected"@endif>{{ $gost->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="name">Section</label>
                                <input type="text" class="form-control" name="section" placeholder="Section" value="">
                            </div>
                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="name">Skp Code</label>
                                <input type="text" class="form-control" name="skp_code" placeholder="Skp Code" value="">
                            </div>
                            <div class="form-group  col-md-12 " data-select2-id="8">
                                <label class="control-label" for="name">Labaratoriya</label>
                                <select class="form-control" name="labaratory_id">
                                    @foreach(DB::select('select * from labaratories') as $labaratory)
                                        <option value="{{ $labaratory->id }}"@if(isset($dataTypeContent->labaratory_id) && $dataTypeContent->labaratory_id == $labaratory->id) selected="selected"@endif>{{ $labaratory->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">Save</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="http://reestr.local/admin/upload" target="form_target" method="post"
                        enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="applications">
                        <input type="hidden" name="_token" value="hF3AsgcgnVr3yvbRmafXEbQ3KcLUiLsrUQ7jydD3">
                    </form>

                </div>
            </div>
        </div>
    </div>
    <script>
        function usertype(ch){
            company = document.getElementById('company');

            if(ch == 1){
                company.disabled = false;
            } else
            company.disabled = true;

        }
        function selected(s) {


        }


    </script>
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> Are you sure</h4>
                </div>

                <div class="modal-body">
                    <h4>O'chirmoqchimisiz?<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ortga</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">O'chirish</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@endsection
