<?php

return [
    'name' => 'OneAuth',
    'one_id' => [
        'CLIENT_ID' =>  env('CLIENT_ID', ''),
        'SCOPE' =>  env('SCOPE', ''),
        'CLIENT_SECRET' =>  env('CLIENT_SECRET', ''),
        "REDIRECT_URI" => env('ONEID_REDIRECT_URI', ''),
    ],
];
