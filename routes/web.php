<?php

use App\Http\Controllers\ApplicationController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\admin\auth\VoyagerEriController;
use App\Http\Controllers\OneAuthController;
use App\Http\Controllers\admin\VoyagerSearchController;
use App\Http\Controllers\ContractController;
use App\Services\ActivityServise;
use App\Http\Controllers\SendEmailController;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\LabaratoryController;
use App\Http\Controllers\ParametersController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/sendemail', [SendEmailController::class, 'index']);
Route::post('/sendemail/send', [SendEmailController::class, 'send']);

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {

        Route::any('(.*)', [LanguageController::class, 'setLang'])->name('user.lang');

        Voyager::routes();
        Route::get('/redirect/appeal/{appeal}', [LanguageController::class, 'toExpert'])->name('answer.redirect');
        Route::get('/appeals/chat/{appeal}', [LanguageController::class, 'showChat'])->name("conversation.index");
        Route::get("/appeals", [LanguageController::class, 'showAppeal'])->name('voyager.appeals.index');
        Route::post('/appeals/chat/rate/{appeal}', [LanguageController::class, 'rating'])->name("conversation.rating");
        Route::post('/appeal/chat/close/{appeal}', [LanguageController::class, 'close'])->name("appeal.close");
        Route::post('/appeal/chat/{id}', [LanguageController::class, 'send'])->name("conversation.send");


        Route::get('/', function () {
            return view('site/mainsection');
        });

        Route::get('history', function () {
            return view('site/history');
        });

        Route::group(['prefix' => 'admin'], function () {
            Voyager::routes();
            Route::get('/home', function () {
                return view('welcome');
            })->name('home');
            Route::get('eri/login', [VoyagerEriController::class, 'index'])->name('eri.login');
            Route::post('eri/login', [VoyagerEriController::class, 'auth'])->name('eri.login');
            Route::get("/search", [VoyagerSearchController::class, "indexSearch"]);
            Route::get("/form", [VoyagerSearchController::class, "indexForm"]);
            Route::get("/search/all", [VoyagerSearchController::class, "ShowSearch"]);
            Route::get("/applications/create", [ApplicationController::class, "index"])->name('voyager.applications.create');
            Route::get("/applications/pay/{id}", [ApplicationController::class, "update"])->name('applications.pay');
            Route::get('/labaratory/test/{laboratory}',  [LabaratoryController::class, "test"])->name("labaratory.test");

            Route::post("/applications/{id}/update", [ApplicationController::class, "update"])->name('applications.update');
            Route::get('/applications', [ApplicationController::class, "show"])->name("application");
            Route::get('/applications/{id}/edit', [ApplicationController::class, "edit"])->name("application.edit");
            Route::post('/application/{id}/delete', [ApplicationController::class, "delete"])->name("application.delete");

            Route::get('/labaratory/test/{application}',  [LabaratoryController::class, "test"])->name("labaratory.test");
            Route::post('labaratory/test/', [LabaratoryController::class, "test_apply"])->name("voyager.application.test.store");
            Route::get('/applications/{id}', [ApplicationController::class, "view"])->name("voyager.applications.show");

            Route::get('/parameters/create', [ParametersController::class, 'create'])->name('voyager.parameters.create');

            Route::post('/labaratory/{application}',  [LabaratoryController::class, "store"])->name("labaratory.store");
            Route::post('applicationnnn/create', [ApplicationController::class, "gen"])->name("voyager.applicationnnn.create");
            Route::get("/applications/create/filter", [ApplicationController::class, "user_next"])->name('voyager.applications.user_next');
            Route::post('applicationnnn/create/filter', [ApplicationController::class, "filter"])->name("voyager.application.filter");
            Route::get('applicationnnn/create/data', [ApplicationController::class, "data"])->name("voyager.application.data");
            Route::post('applicationnnn/create/check', [ApplicationController::class, "check"])->name("voyager.application.check");
            Route::get('applicationnnn/create/check', [ApplicationController::class, "double_check"])->name("voyager.application.double_check");
            Route::get('application/protocol/{application}', [ApplicationController::class, "protocol"])->name("voyager.applications.protocol");
            Route::post('applicationnnn/create/store', [ApplicationController::class, "store"])->name("voyager.application.store");
        });
        Route::middleware(['guest'])->prefix('oneauth')->group(function () {
            Route::get('/index', [OneAuthController::class, 'index'])->name('oneauth.index');
            Route::get('/auth', [OneAuthController::class, 'auth']);
        });

//         Route::group(["prefix" => '/'], function () {
//             $activity = new ActivityServise();
//
//             $activity->getActivity();
//         });
        Route::get("users/activitiy/{user}", [ApplicationController::class,"activity"])->name("users.activity");

        Route::view("/site", "site");
        Route::post('/fileUpload', [ApplicationController::class, "fileUpload"]);
        Route::get("/contracts/download/{word}", [ContractController::class, "download"])->name("contracts.download");
    }
);
